#爱玩网后台管理系统


## 启动本地开发环境

```bash
# 安装项目依赖到本地
npm i
# 启动本地开发环境，启动成功后浏览器会自动打开前端页面
npm run dev
```

说明：如果网络情况不佳，下载项目依赖失败，可尝试：
```bash
# 全局安装cnpm
npm i -g cnpm
# 用cnpm代替npm来安装项目依赖
cnpm i
# 然后启动本地开发环境
npm run dev
```

## 数据库账号信息

```java
jdbc.url=jdbc:mysql://59.110.142.208:3306/traveldb?useUnicode=true&amp;characterEncoding=utf-8
jdbc.username=travel
jdbc.password=yff@1234A
```

<h3>后台页面说明 连接地址说明</h3>
后台维护 无<br /> 
  后台系统管理 无<br /> 
    后台用户管理 /sysuser/list <br /> 
    后台角色管理 /role/list <br /> 
    后台角色管理 /nav/list <br /> 
  前台业务管理 无
     产品管理 /product/list <br /> 
     资源管理 /resources/list <br />
     <br />
线上业务 无<br />


<h3>后台接口说明</h3>
  调试地址:
  http://1l9466d256.iok.la/travel.manage.vc
  http://59.110.142.208:8080
  
  <h4>登录页</h4>
  login/login<br /> 
    params.userAccount = userAccount;<br /> 
    params.userPwd = userPwd;
    获取菜单 menu/queryList.do
  <h4>首页</h4>
    无  
  <h4>后台用户管理</h4>
    /role/getRoleAll.do 获取所有角色列表
    /menu/getMenuList 获取菜单列表 里面有所有菜单和已选菜单列表
    /role/updateRoleMenu  更新菜单接口  roleId menuIds
    
    
  <h4>订单</h4>
  1）订单列表：/order/queryList.do<br /> 
  2）新增订单：/order/addOrder.do<br /> 
  3）修改订单：/order/updateOrder.do<br /> 
  4）删除订单：/order/deleteOrder.do<br /> 
  5）订单信息：/order/getOrderById.do<br /> 
  
  <h4>业务用户管理</h4>
  1）业务用户列表：/buser/queryList.do<br /> 
  2）新增业务用户：/buser/addBuser.do<br /> 
  3）修改业务用户：/buser/updateBuser.do<br /> 
  4）删除业务用户：/buser/deleteBuser.do<br /> 
  5）业务用户信息：/buser/getBuserById.do<br /> 
        private Integer buserId;业务用户ID<br /> 
        private String buserName;用户名称<br /> 
        private String buserAccount;用户账号<br /> 
        private String buserPwd;用户密码<br /> 
        private Byte buserSex;用户性别<br /> 
        private String buserMobile;用户手机号<br /> 
        private String buserEmail;用户邮箱<br /> 
        private Byte buserCertType;用户证件类型<br /> 
        private String buserCertNo;用户证件号码<br /> 
        private Byte buserIsMember;是否会员<br /> 
        private Byte buserMemberGrade;会员等级<br /> 
        private Integer buserScore;用户积分<br /> 
        private String buserAddress;用户地址<br /> 
        private String buserHeaderUrl;用户头像地址<br /> 
        private Date buserExpireDate;会员过期时间<br /> 
        private Date buserLastLoginDate;用户最后登录时间<br /> 

  
  
  
  <h4>用户积分收入管理</h4>
  1）用户积分收入列表：/buserInScore/queryList.do<br /> 
  2）新增用户积分收入：/buserInScore/addBuserInScore.do<br /> 
  3）修改用户积分收入：/buserInScore/updateBuserInScore.do<br /> 
  4）删除用户积分收入：/buserInScore/deleteBuserInScore.do<br /> 
  5）用户积分收入信息：/buserInScore/getBuserInScoreById.do<br /> 
     
  
  
  
   <h4>用户积分支出管理</h4>
   1）用户积分支出列表：/buserOutScore/queryList.do
   2）新增用户积分支出：/buserOutScore/addBuserOutScore.do
   3）修改用户积分支出：/buserOutScore/updateBuserOutScore.do
   4）删除用户积分支出：/buserOutScore/deleteBuserOutScore.do
   5）用户积分支出信息：/buserOutScore/getBuserOutScoreById.do
  
  
  
  <h4>数据字典</h4>
  性别	D00000001<br /> 
  证件类型	D00000002<br /> 
  菜单类型	D00000003<br /> 
  产品类型	D00000004<br /> 
  产品发布状态	D00000005<br /> 
  会员等级	D00000006<br /> 
  订单状态	D00000007<br /> 
  积分来源类型	D00000008<br /> 
  积分支出类型	D00000009<br /> 
  资源类型	D00000010<br /> 
  
  
  查询产品接口 product/queryAllProductList.do 
  查询位置接口 dicItem/getDicItemListByDicCode.do ?dicCode=D00000012
  
  
  
  
  #推荐位
  sysBanner/addBanner.do <br /> 
  参数  bannerDetail,productId,type,resourceId,sort<br /> 
  
  
  #目的地
  destination/queryList.do <br /> 
  destination/addDestination.do <br /> 
  
  updateDestinationById.do
  
  private Integer destinationId;<br /> 
  private String destinationName;<br /> 
  private String destinationDetail;<br /> 
  private Integer type<br /> 
  
  deleteDestinationById.do
  destinationId


#产品添加

是否推荐操作 

#温馨提示
 1、添加温馨提示列表  sysPrompt/addPrompt.do   
 
  参数 noticeInfo  
 2、删除温馨提示列表  sysPrompt/deletePromptById.do   
 
  参数 noticeId  
 3、查询温馨提示列表  sysPrompt/getPromptList.do   


  


  
  

  
