import { merge } from './object';
import { load as modalLoad, alert as modalAlert, getLoadStatus } from './modal';

let apiPrefix = '/';
let apiSuffix = '.do';
let countForAjaxWait = 0;

export function setApiPrefix (val) {
  apiPrefix = val;
}

export function setApiSuffix (val) {
  apiSuffix = val;
}

// set的意思在这里是有问题的，这里只是为了get数据，并未修改任何数据，但是因为有人在在这个方法了所以保留，但也只是作为一个getUrl方法的一个别名
export function setUrl (url) {
  return getUrl(url);
}

export function getUrl (url) {
  return location.origin + apiPrefix + url + apiSuffix;
}

export function post (endPoint, requestData, options, config) {
  requestData = requestData || {};
  config = config || { handleErrorAutomatically: true, silent: false };
  const handleErrorAutomatically = config.handleErrorAutomatically || false;
  const silent = config.silent || false;
  if (!silent) {
    countForAjaxWait++;
    if (countForAjaxWait === 1 && getLoadStatus() === false) {
      modalLoad(true);
    }
  }
  return $.ajax(merge({
    type: 'POST',
    accept: 'application/json',
    contentType: requestData instanceof FormData ? false : 'application/json;charset=utf-8',
    url: apiPrefix + (endPoint || '') + apiSuffix,
    dataType: 'json',
    processData: !(requestData instanceof FormData),
    data: (function () {
      if (requestData instanceof FormData) {
        return requestData;
      }
      return requestData;
    })()
  }, options || {})).done(function (data) {
    if (data.code === '10000') {
      //
    } else {
      handleErrorAutomatically && modalAlert(data.errorMsg || '服务器响应异常！');
    }
  }).fail(function () {
    handleErrorAutomatically && modalAlert('网络异常，请稍后再试！');
  }).always(function () {
    if (!silent) {
      countForAjaxWait--;
      if (countForAjaxWait === 0 && getLoadStatus() === true) {
        modalLoad(false);
      }
    }
  });
};
