export function toDouble (num) {
  return num < 10 ? '0' + num : '' + num;
}

export function getString (val) {
  return val === 0 ? '0' : (val ? '' + val : '');
}

export function isNull(val) {
  if (val !== undefined) {
    if (val == null || val.length === 0) {
      return true;
    } else {
      return false;
    }
  } else {
    return false;
  }
}

// 移除文本中的空白
export function trimSpaces (val) {
  return ('' + val).replace(/\s/g, '');
}

// 获取url中查询参数的值
export function getQueryKeyVal (key) {
  const reg = new RegExp('(\\?|&)' + key + '=([^&]*)(&|$)');
  const r = window.location.search.match(reg);
  return r ? window.decodeURI(r[2]) : '';
};

/*
 * 获取URL参数  仅限ace框架
 */
export function getQueryParam (name) {
  var url = window.location.href;
  if (url.indexOf('?') !== -1) {
    var paramString = url.split('?')[1].split('#page')[0];
  } else {
    return null;
  }
  var reg = new RegExp('(^|&)' + name + '=([^&]*)(&|$)', 'i');
  var r = paramString.match(reg);
  if (r != null) return decodeURI(r[2]);
  return null;
}
