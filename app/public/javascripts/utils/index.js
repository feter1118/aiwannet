import * as common from './common';
import * as object from './object';
import * as modal from './modal';
import * as storage from './storage';
import * as string from './string';
import * as array from './array';
import * as date from './date';
import * as number from './number';
import * as device from './device';
import * as hook from './hook';
import * as validate from './validate';
import * as wechat from './wechat';
import * as ajax from './ajax';
import * as config from './config';
import * as jqgrid from './jqgrid';

const output = {
  common,
  object,
  modal,
  storage,
  string,
  array,
  date,
  number,
  device,
  hook,
  validate,
  wechat,
  ajax,
  config,
  jqgrid
};

export default output;

// 挂载到window对象上，页面里可以直接通过访问$utils调用相关方法
window.$utils = output;
