
export function typeOf (input) {
  return ({}).toString.call(input).slice(8, -1).toLowerCase();
};

export function hasValue (val) {
  return val !== '' && val !== null && val !== undefined && !/^\s+$/.test(val);
};

export function getHashParameter (key) {
  var params = getHashParameters();
  return params[key];
}
export function getHashParameters () {
  const arr = (location.hash || '').replace(/^#/, '').split('&');
  const params = {};
  for (var i = 0; i < arr.length; i++) {
    const data = arr[i].split('=');
    if (data.length === 2) {
      params[data[0]] = data[1];
    }
  }
  return params;
}
