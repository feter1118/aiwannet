var scripts = [null, "", null];
var grid_banner = "#grid-table";
var pager_product = "#grid-pager";
var url = $utils.ajax.setUrl("sysBanner/queryList")
$('.page-content-area').ace_ajax('loadScripts', scripts, function () {
  jQuery(function ($) {
    $(window).on('resize.jqGrid', function () {
      $(grid_banner).jqGrid('setGridWidth', $(".page-content").width());
    })
    var parent_column = $(grid_banner).closest('[class*="col-"]');
    $(document).on('settings.ace.jqGrid', function (ev, event_name, collapsed) {
      if (event_name === 'sidebar_collapsed' || event_name === 'main_container_fixed') {
        //setTimeout is for webkit only to give time for DOM changes and then redraw!!!
        setTimeout(function () {
          $(grid_banner).jqGrid('setGridWidth', parent_column.width());
        }, 0);
      }
    })
    var optinons = jqSetting(grid_banner, pager_product, url);
    optinons.url = url;
    optinons.caption = "推荐位管理"
    optinons.colNames = ['操作 ', 'ID','关联产品','位置', '中文标题', '英文标题',  '排序', '更新时间','主图', '推荐主图'];
    optinons.colModel = [
      {
        name: 'operation', index: '', width: 80, fixed: true, sortable: false, resize: false,
        formatter: operation
      },
      {
        display: "BANNER_ID",
        name: 'BANNER_ID',
        index: 'BANNER_ID',
        width: 60,
        sorttype: "int",
        hidden: true,
        editable: true
      },
      {
        display: "关联项名称",
        name: 'PRODUCT_NAME',
        index: 'PRODUCT_NAME',
        width: 90,
        editable: true,
        sorttype: "date",
        hidden: false
      },
      {
        display: "位置",
        name: 'TYPE',
        index: 'TYPE',
        width: 50,
        editable: true,
        sorttype: "date",
        hidden: false,
        formatter: function (v, o, r) {
          if (v == 1) {
            return '首页'
          } else  if (v == 2) {
            return '目的地'
          } else if (v == 3) {
            return '攻略'
          } else if (v == 4) {
            return '积分商城'
          } else{
            return '产品详情'
          }
        }
      },
      {
        display: "中文标题",
        name: 'BANNER_CN_TITLE',
        index: 'BANNER_CN_TITLE',
        width: 90,
        editable: true,
        sorttype: "date",
        hidden: false
      },
      {
        display: "英文标题",
        name: 'BANNER_EN_TITLE',
        index: 'BANNER_EN_TITLE',
        width: 120,
        editable: true,
        sorttype: "date",
        hidden: false
      },
      {
        display: "排序",
        name: 'RESOURCE_SORT',
        index: 'RESOURCE_SORT',
        width: 60,
        editable: true,
        sorttype: "date",
        hidden: false
      },
      {
        display: "创建时间",
        name: 'CREATE_TIME',
        index: 'CREATE_TIME',
        width: 150,
        sortable: false,
        editable: false,
        formatter: pickDate
      },{
        display: "推荐主图",
        name: 'ITEM_NAME',
        index: 'ITEM_NAME',
        width: 80,
        editable: true,
        sorttype: "date",
        hidden: true
      },
      {
        display: "资源URL",
        name: 'BANNER_CN_TITLE',
        index: 'BANNER_CN_TITLE',
        width: 80,
        editable: true,
        sorttype: "date",
        hidden: false,
        formatter: imageUrl
      }
    ];
    jQuery(grid_banner).jqGrid(optinons);
    $(window).triggerHandler('resize.jqGrid');//trigger window resize to make the grid get the correct size

    function imageUrl(v, o, r) {
      var html = "";
      html = '<a  target="_blank" href="' + r.IMAGE_URL + '">'
      html = html +  ' <img  width="100%" style="max-height: 100%" src="' + r.IMAGE_URL + '" >';
      html = html + '</a>';
      return html;
    }

    function operation(v, o, r) {
      var html = ""
      html += '<span onclick="del(' + r.BANNER_ID + ')" class="ace-icon fa fa-trash-o red bigger-150 cursor-p tt-cursor"></span>'
      return html
    }

    $(document).one('ajaxloadstart.page', function (e) {
      $(grid_banner).jqGrid('GridUnload');
      $('.ui-jqdialog').remove();
    });

   /* var urlResourceCategory = $utils.ajax.setUrl("dicItem/getDicItemListByDicCode");
    var params = {}
    params.dicCode = 'D00000012'
    axios.post(urlResourceCategory, params)
      .then(function (response) {
        console.info(response)
        var array = response.body;
        for (var i = 0; i < array.length; i++) {
          $("#recommendPosition").append("<option value='" + array[i].ITEM_CODE + "'>" + array[i].ITEM_NAME + "</option>");
        }
      })
      .catch(function (error) {
      });*/

  });
});

function search() {
  var params = {}
  params.type = $("#recommendPosition").val();
  params.bannerDetail = "";
  params.productName = $("#productName").val();
  console.info(params);
  $(grid_banner).jqGrid('setGridParam', {
    url: url,
    contentType: 'application/json;charset=utf-8',
    page: 1,
    postData: params
    // 发送数据
  }).trigger("reloadGrid"); // 重新载入
}

function goEdit(id) {
  window.location.href = "#page/product/edit?id=" + id;
}

function del(id) {
  bootbox.confirm("确定删除吗", function (result) {
    if (result) {
      var url = $utils.ajax.setUrl("sysBanner/deleteBannerById");
      var params = {}
      params.bannerId = id;
      axios.post(url, params)
        .then(function (response) {
          $(grid_banner).trigger("reloadGrid");
        })
        .catch(function (error) {
        });
    }
  });
}
