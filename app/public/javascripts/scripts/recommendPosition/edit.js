var scripts = [null, '../assets/js/bootstrap-multiselect.js', "../assets/js/jquery.validate.js", "../assets/js/jquery.validate_msgzh.js", null]
$('.page-content-area').ace_ajax('loadScripts', scripts, function () {
  $('.multiselect').multiselect({
    enableFiltering: true,
    buttonClass: 'btn btn-white btn-primary',
    templates: {
      button: '<button type="button" class="multiselect dropdown-toggle" data-toggle="dropdown"></button>',
      ul: '<ul class="multiselect-container dropdown-menu"></ul>',
      filter: '<li class="multiselect-item filter"><div class="input-group"><span class="input-group-addon"><i class="fa fa-search"></i></span><input class="form-control multiselect-search" type="text"></div></li>',
      filterClearBtn: '<span class="input-group-btn"><button class="btn btn-default btn-white btn-grey multiselect-clear-filter" type="button"><i class="fa fa-times-circle red2"></i></button></span>',
      li: '<li><a href="javascript:void(0);"><label></label></a></li>',
      divider: '<li class="multiselect-item divider"></li>',
      liGroup: '<li class="multiselect-item group"><label class="multiselect-group"></label></li>'
    }
  });


  $("#webuploader-pick").click(function () {
    resourceModel(1,1,"",function (data) {
      console.log(data)
      $(".mainImg").attr("src",data[0].src);
      $(".mainImg").attr("data-id",data[0].id);
    })
  })

  queryAllProductList()

  function queryAllProductList() {
    var url = $utils.ajax.setUrl("product/queryAllProductList");
    var params = {}
    axios.post(url, params)
      .then(function (response) {
        if (response.body.length > 0) {
          var h = '<option value="">不选</option>'
          $.each(response.body, function (key, val) {
            h += '<option value="' + val.PRODUCT_ID + '">' + val.PRODUCT_NAME + '</option>'
          })
          $("#productId").html(h)
          $('#productId').multiselect("rebuild")
        }
      })
  }


});
