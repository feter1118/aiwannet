var scripts = [null, '../assets/js/bootstrap-multiselect.js', "../assets/js/jquery.validate.js", "../assets/js/jquery.validate_msgzh.js", null]
$('.page-content-area').ace_ajax('loadScripts', scripts, function () {
  $('.multiselect').multiselect({
    enableFiltering: true,
    buttonClass: 'btn btn-white btn-primary',
    maxHeight: 200,
    templates: {
      button: '<button type="button" class="multiselect dropdown-toggle" data-toggle="dropdown"></button>',
      ul: '<ul class="multiselect-container dropdown-menu"></ul>',
      filter: '<li class="multiselect-item filter"><div class="input-group"><span class="input-group-addon"><i class="fa fa-search"></i></span><input class="form-control multiselect-search" type="text"></div></li>',
      filterClearBtn: '<span class="input-group-btn"><button class="btn btn-default btn-white btn-grey multiselect-clear-filter" type="button"><i class="fa fa-times-circle red2"></i></button></span>',
      li: '<li><a href="javascript:void(0);"><label></label></a></li>',
      divider: '<li class="multiselect-item divider"></li>',
      liGroup: '<li class="multiselect-item group"><label class="multiselect-group"></label></li>'
    }
  });

  var urlResourceCategory=$utils.ajax.setUrl("dicItem/getDicItemListByDicCode");
  var params={}
  params.dicCode ='D00000012'
  axios.post(urlResourceCategory, params)
    .then(function (response) {
      console.info(response)
      var array = response.body;
      for(var i=0;i<array.length;i++){
        $("#resourceCategory").append("<option value='"+array[i].ITEM_CODE+"'>"+array[i].ITEM_NAME+"</option>");
      }
    })
    .catch(function (error) {
    });

  $("#webuploader-pick").click(function () {
    resourceModel(1, 1,1, function (data) {
      console.log(data)
      $(".mainImg").attr("src", data[0].src);
      $(".mainImg").attr("data-id", data[0].id);
    })
  })
  
  queryAllStrategyList()
  function queryAllStrategyList(){
    var url = $utils.ajax.setUrl("sysStrategy/queryAllStrategyList");
    var params = {}
    axios.post(url, params)
      .then(function (response) {
        if (response.body.length > 0) {
          var h = '<option value="">不选</option>'
          $.each(response.body, function (key, val) {
            h += '<option value="' + val.STRATEGY_ID + '">' + val.STRATEGY_NAME + '</option>'
          })
          $("#strategyId").html(h)
          $('#strategyId').multiselect("rebuild")
        }
      })
      .catch(function (error) {
      });
  }
  
  queryAllGoodsList()
  function queryAllGoodsList(){
    var url = $utils.ajax.setUrl("store/goods/queryAllGoodsList");
    var params = {}
    axios.post(url, params)
      .then(function (response) {
        if (response.body.length > 0) {
          var h = '<option value="">不选</option>'
          $.each(response.body, function (key, val) {
            h += '<option value="' + val.GOODS_ID + '">' + val.GOODS_NAME + '</option>'
          })
          $("#goodsId").html(h)
          $('#goodsId').multiselect("rebuild")
        }
      })
      .catch(function (error) {
      });

  }
  
  queryAllProductList()
  function queryAllProductList() {
    var url = $utils.ajax.setUrl("product/queryAllProductList");
    var params = {}
    axios.post(url, params)
      .then(function (response) {
        if (response.body.length > 0) {
          var h = '<option value="">不选</option>'
          $.each(response.body, function (key, val) {
            h += '<option value="' + val.PRODUCT_ID + '">' + val.PRODUCT_NAME + '</option>'
          })
          $("#productId").html(h)
          $('#productId').multiselect("rebuild")
        }
      })
      .catch(function (error) {
      });
  }


  renderrecommendPosition()

  function renderrecommendPosition() {
    var recommendPosition = JSON.parse(localStorage.getItem("recommendPosition"));
    if (recommendPosition.length > 0) {
      var h = '<option value="">不选</option>'
      $.each(recommendPosition, function (key, val) {
        h += '<option value="' + val.ITEM_CODE + '">' + val.ITEM_NAME + '</option>'
      })
      $("#recommendPosition").html(h)
      $('#recommendPosition').multiselect("rebuild")
    }
  }
  window.changeBannerType = changeBannerType;
  function changeBannerType(v){
    if(v==1){
      $("#productDiv").show();
      $("#strategyDiv").hide();
    }else if(v==2){
      $("#productDiv").hide();
      $("#strategyDiv").hide();
    }else if(v==3){
      $("#productDiv").css('display','none');
      $("#strategyDiv").css('display','block');
    }else if(v==4){
      $("#productDiv").css('display','none');
      $("#strategyDiv").css('display','none');
      $("#goodsDiv").css('display','block');
    }
  }

  $("#add").click(function () {
    addRecommendPosition()
  })

  function addRecommendPosition() {
    var url = $utils.ajax.setUrl("sysBanner/addBanner");
    var params = {};
    params.type=$("#bannerType").val();
    params.bannerDetail = $("#bannerDetail").val();
    if($("#bannerType").val()==3){
      params.productId = $("#strategyId").val();
    }else if($("#bannerType").val()==4){
      params.productId = $("#goodsId").val();
    }else{
      params.productId = $("#productId").val();
    }
    params.bannerEnTitle = $("#bannerEnTitle").val();
    params.bannerCnTitle = $("#bannerCnTitle").val();
    params.resourceId = $("#mainImg").attr("data-id");


    if($utils.string.isNull(params.resourceId)){
      bootbox.alert("请选择图片")
      return false
    }

    params.sort = $("#sort").val()

    if ($utils.string.isNull(params.productId)) {
      bootbox.alert("关联产品不能为空")
      return false
    }

    axios.post(url, params)
      .then(function (response) {
        bootbox.alert("添加成功")
        window.location.href = "#page/recommendPosition/list"
      })
      .catch(function (error) {
      });
  }
});
