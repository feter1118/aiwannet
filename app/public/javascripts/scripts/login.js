$(function () {
  localStorage.clear()
  //console.log($utils)
  $("#login").click(function () {
    Login()
  })

  function Login() {

    var userAccount = $("#userAccount").val()
    var userPwd = $("#userPwd").val()
    var params = {}
    params.userAccount = userAccount;
    params.userPwd = userPwd;
    var url = $utils.ajax.setUrl("login/login");

    console.log(url)
    return axios.post(url, params)
      .then(function (response) {
        var token = response.body.token;
        setLocalStorage("nav", response.body.navList)
        setLocalStorage('userInfo', {'userName': response.body.userName})
        $.cookie('token', token, {expires: 7, path: '/'});
        window.location.href = "./index.html"
      })
      .catch(function (error) {
      });
  }
})
