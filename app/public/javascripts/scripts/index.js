$(function () {
  var token=$.cookie('token');
  if(token==undefined||token==null){
    bootbox.alert("没有登录或者登陆过期")
    window.location.href="./login.html"
  }

  var nav = JSON.parse(localStorage.getItem("nav"));

  function computeNav(nav) {
    var tree_array = {}
    var root = []
    for (var i = 0; i < nav.length; i++) {
      var tree = nav[i];
      tree_array[tree.menuId] = tree;
      if (tree['menuPid'] == 0) {
        root.push(tree);
      }
    }

    for (var i = 0; i < nav.length; i++) {
      var tree = nav[i];
      var parents = {};
      if (tree['menuPid'] != 0) {
        parents = tree_array[tree['menuPid']];
        if (parents['children'] == null) {
          parents['children'] = [];
        }
        parents['children'].push(tree);
      }
    }
    return root;
  }


  var navObj = computeNav(nav)

  localStorage.setItem("computedNav", JSON.stringify(navObj));
  var string = ""
  computeRender(navObj)

  $("#sidebar>.nav-list").html(string)

  function computeRender(obj) {
    $.each(obj, function (key, val) {
      if (val.children != undefined) {
        string += '<li class="">'
        string += '<a data-url="javascript:void(0)" class="dropdown-toggle">'
        string += '<i class="menu-icon icon iconfont '+val.menuIcon+'"></i>'
        string += '<span class="menu-text">' + val.menuName + '</span>'
        string += '<b class="arrow fa fa-angle-down"></b>'
        string += '</a>'
        string += '<b class="arrow"></b>'
        string += '<ul class="submenu">'
        computeRender(val.children)
        string += '</ul>'
        string += '</li>'
      } else {
        string += '<li class="">'
        string += '<a data-url="page' + val.menuUrl + '" href="#page' + val.menuUrl + '">'
        string += '<i class="menu-icon icon iconfont '+val.menuIcon+'"></i>'
        string += '<span class="menu-text">' + val.menuName + '</span>'
        string += '<b class="arrow"></b>'
        string += '</a>'
        string += '</li>'
      }
    })
  }

  var userInfo=getLocalStorage('userInfo')
  $("#headerUserName").html(userInfo.userName)




  /*
  * 订单交易提醒
  * */
  OrderReminding()
  function OrderReminding() {
    var path='/order/list'
    if(hasNavRight(path)){
      var url = $utils.ajax.setUrl("order/queryDealOrderNum");
      axios.post(url)
        .then(function (response) {
          $("#waitingOrderNumber").html(response.body)
        })
    }else{
      $("#waitingOrderNumberBox").remove()
    }
  }

  $("#waitingOrderNumberBox").click(function () {
    window.location.href="#page/order/list?wait=1"
  })


  /*
  * 收缩条件经常使用的字典表请求
  * */
  getDicItemListByDicCode()

  function getDicItemListByDicCode() {
    //资源应用范围
    var url = $utils.ajax.setUrl("dicItem/getDicItemListByDicCode");
    if ($utils.string.isNull(getLocalStorage("appResScope"))) {
      axios.post(url, {'dicCode': 'D00000011'})
        .then(function (response) {
          setLocalStorage('appResScope',response.body)
        })
    }
    //资源类型
    if ($utils.string.isNull(getLocalStorage("appResType"))) {
      axios.post(url, {'dicCode': 'D00000010'})
        .then(function (response) {
          setLocalStorage('appResType',response.body)
        })
    }
    //推荐位
    if ($utils.string.isNull(getLocalStorage("recommendPosition"))) {
      axios.post(url, {'dicCode': 'D00000012'})
        .then(function (response) {
          setLocalStorage('recommendPosition',response.body)
        })
    }
    //发布状态
    if ($utils.string.isNull(getLocalStorage("releaseStatus"))) {
      axios.post(url, {'dicCode': 'D00000005 '})
        .then(function (response) {
          setLocalStorage('releaseStatus',response.body)
        })
    }

    //产品类型字典
    if ($utils.string.isNull(getLocalStorage("activityType"))) {
      axios.post(url, {'dicCode': 'D00000013 '})
        .then(function (response) {
          setLocalStorage('activityType',response.body)
        })
    }

    //产品类型字典
    if ($utils.string.isNull(getLocalStorage("productType"))) {
      axios.post(url, {'dicCode': 'D00000004'})
        .then(function (response) {
          setLocalStorage('productType',response.body)
        })
    }
    //国家
    if ($utils.string.isNull(getLocalStorage("country"))) {
      axios.post(url, {'dicCode': 'D00000019'})
        .then(function (response) {
          setLocalStorage('country',response.body)
        })
    }

    //目的地类型
    if ($utils.string.isNull(getLocalStorage("destinationType"))) {
      axios.post(url, {'dicCode': 'D00000020'})
        .then(function (response) {
          setLocalStorage('destinationType',response.body)
        })
    }

    //获取所有目的地数据并存放session
    if ($utils.string.isNull(getLocalStorage("destination"))) {
      resetDestination()
    }
  }

  $("#loginOut").click(function () {
    var url = $utils.ajax.setUrl("login/logout");
    bootbox.confirm( '确认退出吗', function (v) {
      if (v) {
        axios.post(url)
          .then(function (response) {
            window.location.href = "./login.html"
          })
      }
    })
  })
})
