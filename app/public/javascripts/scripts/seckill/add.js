var scripts = [null, '../assets/js/bootstrap-multiselect.js', "../assets/js/date-time/bootstrap-datepicker.js", "../assets/js/date-time/bootstrap-timepicker.js", "../assets/js/jquery.validate.js", "../assets/js/jquery.validate_msgzh.js", null]
$('.page-content-area').ace_ajax('loadScripts', scripts, function () {


  $('.date-picker').datepicker({
    language: 'ch',
    autoclose: true,
    todayHighlight: true
  }).next().on(ace.click_event, function () {
    $(this).prev().focus();
  });
  var productArr = {}
  $('#productId').multiselect({
    enableFiltering: true,
    buttonClass: 'btn btn-white btn-primary',
    maxHeight: 200,
    numberDisplayed:1,
    templates: {
      button: '<button type="button" class="multiselect dropdown-toggle" data-toggle="dropdown"></button>',
      ul: '<ul class="multiselect-container dropdown-menu"></ul>',
      filter: '<li class="multiselect-item filter"><div class="input-group"><span class="input-group-addon"><i class="fa fa-search"></i></span><input class="form-control multiselect-search" type="text"></div></li>',
      filterClearBtn: '<span class="input-group-btn"><button class="btn btn-default btn-white btn-grey multiselect-clear-filter" type="button"><i class="fa fa-times-circle red2"></i></button></span>',
      li: '<li><a href="javascript:void(0);"><label></label></a></li>',
      divider: '<li class="multiselect-item divider"></li>',
      liGroup: '<li class="multiselect-item group"><label class="multiselect-group"></label></li>'
    },
    onChange: function (element, checked) {
      renderSettingList($(element).val(), checked)
    }
  });

  queryAllProductList()

  function queryAllProductList() {
    var url = $utils.ajax.setUrl("product/queryAllSecKillProductList");
    var params = {}
    axios.post(url, params)
      .then(function (response) {
        if (response.body.length > 0) {
          var h = ''
          $.each(response.body, function (key, val) {
            h += '<option value="' + val.PRODUCT_ID + '">' + val.PRODUCT_NAME + '</option>'
            productArr[val.PRODUCT_ID] = val.PRODUCT_NAME
          })
          $("#productId").html(h)
          $('#productId').multiselect("rebuild")
        }
      })
      .catch(function (error) {
      });
  }

  function renderSettingList(id, checked) {
    if (checked) {
      var html = '<div class="form-group" data-content-id="' + id + '">' +
        '        <div class="col-sm-2">' +
        '          <label class="col-sm-12 control-label"> ' + productArr[id] + '</label>' +
        '        </div>' +
        '        <div class="col-sm-2">' +
        '          <label class="col-sm-6 control-label" for="">秒杀价格</label>' +
        '          <input type="text" class="col-xs-6 seckillPrice">' +
        '        </div>' +
        '        <div class="col-sm-2">' +
        '          <label class="col-sm-6 control-label" for="">秒杀总数</label>' +
        '          <input type="text"  class="col-xs-6 seckillNum">' +
        '        </div>' +
        '        <div class="col-sm-2">' +
        '          <label class="col-sm-6 control-label" for="">剩余数量</label>' +
        '          <input type="text" class="col-xs-6 seckillRemainNum">' +
        '        </div>' +
        '        <div class="col-sm-2">' +
        '          <label class="col-sm-6 control-label" for="">排序</label>' +
        '          <select class="col-sm-6 seckillSort" >' +
        '            <option value="1">1</option>' +
        '            <option value="2">2</option>' +
        '            <option value="3">3</option>' +
        '            <option value="4">4</option>' +
        '            <option value="5">5</option>' +
        '            <option value="6">6</option>' +
        '            <option value="7">7</option>' +
        '            <option value="8">8</option>' +
        '            <option value="9">9</option>' +
        '            <option value="10">10</option>' +
        '          </select>' +
        '        </div>' +
        '      </div>'

      $('#setttingList').append(html)
    } else {
      $('#setttingList').find(".form-group[data-content-id=" + id + "]").remove()
    }
  }

  $('#add').click(function () {
    addOrUpdateProductSecProduct()
  })

  function addOrUpdateProductSecProduct() {
    var url = $utils.ajax.setUrl("productSecKill/addOrUpdateProductSecProduct");
    var params = {}
    params.seckillTitle = $("#seckillTitle").val()
    params.seckillStartTime = $("#seckillStartTime").val()
    params.seckillEndTime = $("#seckillEndTime").val();
    var list = []
    $('#setttingList .form-group').each(function (index, ele) {
      var item = {}
      item.productId = $(ele).attr("data-content-id");
      item.seckillPrice = $(ele).find(".seckillPrice").val();
      item.seckillNum = $(ele).find(".seckillNum").val();
      item.seckillRemainNum = $(ele).find(".seckillRemainNum").val();
      item.seckillSort = $(ele).find(".seckillSort").val();
      list.push(item)
    })
    params.list = list


    if (params.list.length < 4) {
      bootbox.alert("秒杀产品数量必须大于四个")
      return false
    }

    axios.post(url, params)
      .then(function (response) {
        bootbox.alert("添加成功")
        if (response.code = 10000) {
          window.location.href = '#page/seckill/list'
        }
      })
  }
});
