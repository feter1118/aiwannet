var scripts = [null, '../assets/js/bootstrap-multiselect.js', "../assets/js/date-time/bootstrap-datepicker.js", "../assets/js/date-time/bootstrap-timepicker.js", "../assets/js/jquery.validate.js", "../assets/js/jquery.validate_msgzh.js", null]
$('.page-content-area').ace_ajax('loadScripts', scripts, function () {

  function getData() {
    var url = $utils.ajax.setUrl("productSecKill/getProductSecProductBySeckillId");
    var params = {}
    params.seckillId = $utils.string.getQueryParam("id")
    return axios.post(url, params)
  }

  function queryAllProductList() {
    var url = $utils.ajax.setUrl("product/queryAllSecKillProductList");
    var params = {}
    return axios.post(url, params)
  }

  $('.date-picker').datepicker({
    language: 'ch',
    autoclose: true,
    todayHighlight: true
  }).next().on(ace.click_event, function () {
    $(this).prev().focus();
  });
  $('#productId').multiselect({
    enableFiltering: true,
    buttonClass: 'btn btn-white btn-primary',
    maxHeight: 200,
    numberDisplayed:1,
    templates: {
      button: '<button type="button" class="multiselect dropdown-toggle" data-toggle="dropdown"></button>',
      ul: '<ul class="multiselect-container dropdown-menu"></ul>',
      filter: '<li class="multiselect-item filter"><div class="input-group"><span class="input-group-addon"><i class="fa fa-search"></i></span><input class="form-control multiselect-search" type="text"></div></li>',
      filterClearBtn: '<span class="input-group-btn"><button class="btn btn-default btn-white btn-grey multiselect-clear-filter" type="button"><i class="fa fa-times-circle red2"></i></button></span>',
      li: '<li><a href="javascript:void(0);"><label></label></a></li>',
      divider: '<li class="multiselect-item divider"></li>',
      liGroup: '<li class="multiselect-item group"><label class="multiselect-group"></label></li>'
    },
    onChange: function (element, checked) {
      renderSettingList($(element).val(), checked)
    }
  });
  var productArr = {}
  var selectedProduct = []
  axios.all([getData(), queryAllProductList()])
    .then(axios.spread(function (data1, data2) {

      $.each(data2.body, function (key, val) {
        productArr[val.PRODUCT_ID] = val.PRODUCT_NAME
      })
      renderSecKill(data1.body)

      if (data2.body.length > 0) {
        var h = ''
        $.each(data2.body, function (key, val) {
          h += '<option value="' + val.PRODUCT_ID + '">' + val.PRODUCT_NAME + '</option>'
        })
        $("#productId").html(h)
        $('#productId').multiselect("rebuild")
        $('#productId').multiselect('select', selectedProduct)
      }
    }));

  function renderSecKill(body) {
    $("#seckillTitle").val(body.secKillInfo.seckillTitle)
    $("#seckillIsOpen").val(body.secKillInfo.seckillIsOpen)
    $("#seckillStartTime").val(moment(body.secKillInfo.seckillStartTime).format("YYYY-MM-DD"))
    $("#seckillEndTime").val(moment(body.secKillInfo.seckillEndTime).format("YYYY-MM-DD"));
    if (body.productList.length > 0) {
      var html = ''
      $.each(body.productList, function (key, val) {
        selectedProduct.push(val.PRODUCT_ID);
        var selectArr = ['', '', '', '', '', '', '', '', '', '', '', '']
        selectArr[val.SECKILL_SORT] = 'selected'
        html += '<div class="form-group" data-content-id="' + val.PRODUCT_ID + '">' +
          '        <div class="col-sm-2">' +
          '          <label class="col-sm-12 control-label"> ' + productArr[val.PRODUCT_ID] + '</label>' +
          '        </div>' +
          '        <div class="col-sm-2">' +
          '          <label class="col-sm-6 control-label" for="">秒杀价格</label>' +
          '          <input type="text" class="col-xs-6 seckillPrice " value="' + val.SECKILL_PRICE + '">' +
          '        </div>' +
          '        <div class="col-sm-2">' +
          '          <label class="col-sm-6 control-label" for="">秒杀总数</label>' +
          '          <input type="text" class="col-xs-6 seckillNum"  value="' + val.SECKILL_NUM + '">' +
          '        </div>' +
          '        <div class="col-sm-2">' +
          '          <label class="col-sm-6 control-label" for="">剩余数量</label>' +
          '          <input type="text" class="col-xs-6 seckillRemainNum" value="' + val.SECKILL_REMAIN_NUM + '">' +
          '        </div>' +
          '        <div class="col-sm-2">' +
          '          <label class="col-sm-6 control-label" for="">排序</label>' +
          '          <select class="col-sm-6 seckillSort">' +
          '            <option ' + selectArr[1] + ' value="1">1</option>' +
          '            <option ' + selectArr[2] + ' value="2">2</option>' +
          '            <option ' + selectArr[3] + ' value="3">3</option>' +
          '            <option ' + selectArr[4] + '  value="4">4</option>' +
          '            <option ' + selectArr[5] + '  value="5">5</option>' +
          '            <option ' + selectArr[6] + '  value="6">6</option>' +
          '            <option ' + selectArr[7] + '  value="7">7</option>' +
          '            <option ' + selectArr[8] + '  value="8">8</option>' +
          '            <option ' + selectArr[9] + '   value="9">9</option>' +
          '            <option ' + selectArr[10] + '   value="10">10</option>' +
          '          </select>' +
          '        </div>' +
          '      </div>'
      })
      $('#setttingList').html(html)
    }
  }


  function renderSettingList(id, checked) {
    if (checked) {
      var html = '<div class="form-group" data-content-id="' + id + '">' +
        '        <div class="col-sm-2">' +
        '          <label class="col-sm-12 control-label"> ' + productArr[id] + '</label>' +
        '        </div>' +
        '        <div class="col-sm-2">' +
        '          <label class="col-sm-6 control-label" for="">秒杀价格</label>' +
        '          <input type="text" id="" class="col-xs-6 seckillPrice">' +
        '        </div>' +
        '        <div class="col-sm-2">' +
        '          <label class="col-sm-6 control-label" for="">秒杀总数</label>' +
        '          <input type="text" id="" class="col-xs-6 seckillNum">' +
        '        </div>' +
        '        <div class="col-sm-2">' +
        '          <label class="col-sm-6 control-label" for="">剩余数量</label>' +
        '          <input type="text" id="" class="col-xs-6 seckillRemainNum">' +
        '        </div>' +
        '        <div class="col-sm-2">' +
        '          <label class="col-sm-6 control-label" for="">排序</label>' +
        '          <select class="col-sm-6 seckillSort" id="menuSort">' +
        '            <option value="1">1</option>' +
        '            <option value="2">2</option>' +
        '            <option value="3">3</option>' +
        '            <option value="4">4</option>' +
        '            <option value="5">5</option>' +
        '            <option value="6">6</option>' +
        '            <option value="7">7</option>' +
        '            <option value="8">8</option>' +
        '            <option value="9">9</option>' +
        '            <option value="10">10</option>' +
        '          </select>' +
        '        </div>' +
        '      </div>'

      $('#setttingList').append(html)
    } else {
      $('#setttingList').find(".form-group[data-content-id=" + id + "]").remove()
    }
  }

  $('#add').click(function () {
    addOrUpdateProductSecProduct()
  })

  function addOrUpdateProductSecProduct() {
    var url = $utils.ajax.setUrl("productSecKill/addOrUpdateProductSecProduct");
    var params = {}
    params.seckillId=$utils.string.getQueryParam("id")
    params.seckillTitle = $("#seckillTitle").val()
    params.seckillIsOpen = $("#seckillIsOpen").val()
    params.seckillStartTime = $("#seckillStartTime").val()
    params.seckillEndTime = $("#seckillEndTime").val();
    var list = []
    $('#setttingList .form-group').each(function (index, ele) {
      var item = {}
      item.productId = $(ele).attr("data-content-id");
      item.seckillPrice = $(ele).find(".seckillPrice").val();
      item.seckillNum = $(ele).find(".seckillNum").val();
      item.seckillRemainNum = $(ele).find(".seckillRemainNum").val();
      item.seckillSort = $(ele).find(".seckillSort").val();
      list.push(item)
    })
    params.list = list

    if (params.list.length < 4) {
      bootbox.alert("秒杀产品数量必须大于四个")
      return false
    }

    axios.post(url, params)
      .then(function (response) {
        if (response.code = 10000) {
          bootbox.alert("修改成功")
          window.location.href = '#page/seckill/list'
        }
      })
  }

});
