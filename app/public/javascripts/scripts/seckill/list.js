var scripts = [null, "", null];
var grid_productSecKill = "#grid-table";
var pager_productSecKill = "#grid-pager";
var url = $utils.ajax.setUrl("productSecKill/queryList")
$('.page-content-area').ace_ajax('loadScripts', scripts, function () {
  jQuery(function ($) {
    $(window).on('resize.jqGrid', function () {
      $(grid_productSecKill).jqGrid('setGridWidth', $(".page-content").width());
    })
    var parent_column = $(grid_productSecKill).closest('[class*="col-"]');
    $(document).on('settings.ace.jqGrid', function (ev, event_name, collapsed) {
      if (event_name === 'sidebar_collapsed' || event_name === 'main_container_fixed') {
        //setTimeout is for webkit only to give time for DOM changes and then redraw!!!
        setTimeout(function () {
          $(grid_productSecKill).jqGrid('setGridWidth', parent_column.width());
        }, 0);
      }
    })
    var optinons = jqSetting(grid_productSecKill, pager_productSecKill, url);
    optinons.url = url;
    optinons.caption = "秒杀管理"
    optinons.colNames = ['操作 ', 'ID', '主题', '开始时间', '结束时间', '状态'];
    optinons.colModel = [
      {
        name: 'operation', index: '', width: 80, fixed: true, sortable: false, resize: false,
        formatter: operation
      },
      {
        display: "SECKILL_ID",
        name: 'SECKILL_ID',
        index: 'SECKILL_ID',
        width: 60,
        sorttype: "int",
        hidden: true,
        editable: true
      },
      {
        display: "主题",
        name: 'SECKILL_TITLE',
        index: 'SECKILL_TITLE',
        width: 180,
        editable: true,
        sorttype: "date",
        hidden: false,
        formatter: theme
      },
      {
        display: "开始时间",
        name: 'SECKILL_START_TIME',
        index: 'SECKILL_START_TIME',
        width: 90,
        editable: true,
        sorttype: "date",
        hidden: false,
        formatter: pickDate
      },
      {
        display: "结束时间",
        name: 'SECKILL_END_TIME',
        index: 'SECKILL_END_TIME',
        width: 90,
        editable: true,
        sorttype: "date",
        hidden: false,
        formatter: pickDate
      },
      {
        display: "状态",
        name: 'SECKILL_IS_OPEN',
        index: 'SECKILL_IS_OPEN',
        width: 120,
        editable: true,
        sorttype: "date",
        hidden: false,
        formatter: status
      },
    ];
    jQuery(grid_productSecKill).jqGrid(optinons);
    $(window).triggerHandler('resize.jqGrid');//trigger window resize to make the grid get the correct size
    function theme(v, o, r) {
      console.log(r)

      var html = '<a href="#page/seckill/edit?id=' + r.SECKILL_ID + '">' + v + '</a>'
      return html
    }

    function status(v, o, r) {
      var html = ""
      if (r.SECKILL_IS_OPEN == 1) {
        html = '<span class="prd10">开启</span>' + '<span class="btn btn-sm btn-white" onclick="changeSecKillStatus(' + r.SECKILL_ID + ',' + '0' + ')">关闭</span>'
      } else {
        html = '<span class="prd10">关闭</span>' + '<span class="btn btn-sm btn-white" onclick="changeSecKillStatus(' + r.SECKILL_ID + ',' + '1' + ')">开启</span>'
      }
      return html
    }

    function operation(v, o, r) {
      var html = ""
      html += '<span onclick="del(' + r.SECKILL_ID + ')" class="ace-icon fa fa-trash-o red bigger-150 cursor-p tt-cursor"></span>'
      return html
    }

    $(document).one('ajaxloadstart.page', function (e) {
      $(grid_productSecKill).jqGrid('GridUnload');
      $('.ui-jqdialog').remove();
    });


  });
});

function search() {
  var params = {}
  params.seckillTitle = $("#seckillTitle").val();
  params.seckillIsOpen = $("#seckillIsOpen").val();
  console.info(params);
  $(grid_productSecKill).jqGrid('setGridParam', {
    url: url,
    contentType: 'application/json;charset=utf-8',
    page: 1,
    postData: params
    // 发送数据
  }).trigger("reloadGrid"); // 重新载入
}

function del(id) {
  bootbox.confirm("确定删除吗", function (result) {
    if (result) {
      var url = $utils.ajax.setUrl("productSecKill/deleteProductSecKillById");
      var params = {}
      params.seckillId = id;
      axios.post(url, params)
        .then(function (response) {
          $(grid_productSecKill).trigger("reloadGrid");
        })
        .catch(function (error) {
        });
    }
  });
}

function changeSecKillStatus(id,type) {
  var msg='';
  if(type==0){
    msg="确认要关闭秒杀吗"
  }else{
    msg="确认要开启秒杀吗"
  }
  var url=$utils.ajax.setUrl("productSecKill/updateProductSecKillStatusById");
  var params={}
  params.seckillId=id
  params.seckillIsOpen=type
  bootbox.confirm(msg,function (v) {
    if(v){
      axios.post(url, params)
        .then(function (response) {
          $(grid_productSecKill).trigger("reloadGrid");
        })
    }
  })
}
