var scripts = [null, '../assets/js/bootstrap-multiselect.js', "../assets/js/jquery.validate.js", "../assets/js/jquery.validate_msgzh.js", '../assets/js/bootstrap-tagsinput.js', null]

$('.page-content-area').ace_ajax('loadScripts', scripts, function () {
  
	renderClassifyPType('classifyType')
	
  $("#add").click(function () {
    addClassify()
  })
  
  $("#webuploader-pick").click(function () {
    resourceModel(1, 11, 11, function (data) {
      $(".mainImg").attr("src", data[0].src);
      $(".mainImg").attr("data-id", data[0].id);
      getImageWidth(data[0].src, function (w, h) {
        $("#Imgsize").html(w + '*' + h)
      });
    })
  })

  function addClassify() {
    var params = {}
    params.goodsClassifyName = $("#goodsClassifyName").val();
    params.resourceId = $(".mainImg").attr("data-id");
    params.goodsClassifyPid = $("#classifyType").val();
    
    if ($utils.string.isNull(params.resourceId)) {
    	bootbox.alert("商品类别主图不能为空")
        return false
    }
    
    if ($utils.string.isNull(params.goodsClassifyName)) {
      bootbox.alert("请输入商品类别名称")
      return false
    }
    
    if (params.goodsClassifyName.length>100) {
        bootbox.alert("商品类别名称不能超过100字")
        return false
    }

    var url = $utils.ajax.setUrl("store/classify/saveOrUpdateClassify");
    axios.post(url, params)
      .then(function (response) {
        bootbox.alert(response.msg);
        window.location.href = "#page/store/classify/list"
      })
  }
  
  /*积分商城商品类别*/
  function renderClassifyPType(id) {
  	var url = $utils.ajax.setUrl("/store/classify/getClassifyAll");
  	axios.post(url, {})
  		.then(function (response) {
  		var classify = response.body;
  		var html = '<option value="0">无</option>'
  		$.each(classify, function (key, val) {
  		    html += '<option value="' + val.CLASSIFY_ID + '">' + val.GOODS_CLASSIFY_NAME + '</option>'
  		});
  		$("#" + id).html(html);
  	});

  }

});

