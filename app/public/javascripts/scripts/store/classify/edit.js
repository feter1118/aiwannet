var scripts = [null, '../assets/js/bootstrap-multiselect.js', "../assets/js/jquery.validate.js", "../assets/js/jquery.validate_msgzh.js", '../assets/js/bootstrap-tagsinput.js', null]

$('.page-content-area').ace_ajax('loadScripts', scripts, function () {

  getData();
  
  $("#update").click(function () {
      updateClassify();
  })
  
  $("#webuploader-pick").click(function () {
    resourceModel(1, 1, 11, function (data) {
      $(".mainImg").attr("src", data[0].src);
      $(".mainImg").attr("data-id", data[0].id);
      getImageWidth(data[0].src, function (w, h) {
        $("#Imgsize").html(w + '*' + h)
      });
    })
  })

  function updateClassify() {
    var params = {}
    params.classifyId = $utils.string.getQueryParam("id");
    params.goodsClassifyName = $("#goodsClassifyName").val();
    params.goodsClassifyPid = $("#classifyType").val();
    params.resourceId = $(".mainImg").attr("data-id");
    
    if ($utils.string.isNull(params.goodsClassifyName)) {
        bootbox.alert("请输入商品类别名称")
        return false
    }
    
    if (params.goodsClassifyName.length>100) {
        bootbox.alert("商品类别名称不能超过100字")
        return false
    }

    var url = $utils.ajax.setUrl("store/classify/saveOrUpdateClassify");
    axios.post(url, params)
      .then(function (response) {
        bootbox.alert(response.msg);
          window.location.href = "#page/store/classify/list"
      })
  }
  
});

function getData() {
    var url=$utils.ajax.setUrl("store/classify/getClassifyById");
    var params={}
    params.classifyId=$utils.string.getQueryParam("id")
    axios.post(url, params)
      .then(function (response) {
        renderClassify(response.body)
      })
      .catch(function (error) {
      });
}

function renderClassify(body) {
    $("#goodsClassifyName").val(body.GOODS_CLASSIFY_NAME);
    
    var resourceImg = body.RESOURCE_ID;
    if (!$utils.string.isNull(resourceImg)) {
        $(".mainImg").attr("src", body.IMAGE_URL);
        $(".mainImg").attr("data-id", body.RESOURCE_ID);
        getImageWidth(body.IMAGE_URL,function(w,h){
        	$("#Imgsize").html(w+'*'+h)
        });
      }
    var url = $utils.ajax.setUrl("/store/classify/getClassifyAll");
  	axios.post(url, {})
  		.then(function (response) {
  		var classify = response.body;
  		var html = '<option value="0">无</option>'
  		$.each(classify, function (key, val) {
  		    html += '<option value="' + val.CLASSIFY_ID + '">' + val.GOODS_CLASSIFY_NAME + '</option>'
  		});
  		$("#classifyType").html(html);
  		$("#classifyType").val(body.GOODS_CLASSIFY_PID);
  	});
    
}


