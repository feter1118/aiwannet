var scripts = [null, "", null];
var grid_classify = "#grid-table";
var pager_classify = "#grid-pager";
var url = $utils.ajax.setUrl("store/classify/queryList")
$('.page-content-area').ace_ajax('loadScripts', scripts, function() {
  renderClassifyPType('classifyType');
  jQuery(function($) {
    $(window).on('resize.jqGrid', function () {
      $(grid_classify).jqGrid( 'setGridWidth', $(".page-content").width() );
    })
    var parent_column = $(grid_classify).closest('[class*="col-"]');
    $(document).on('settings.ace.jqGrid' , function(ev, event_name, collapsed) {
      if( event_name === 'sidebar_collapsed' || event_name === 'main_container_fixed' ) {
        //setTimeout is for webkit only to give time for DOM changes and then redraw!!!
        setTimeout(function() {
          $(grid_classify).jqGrid( 'setGridWidth', parent_column.width() );
        }, 0);
      }
    })
    var optinons=jqSetting(grid_classify,pager_classify,url);
    optinons.url=url;
    optinons.caption="积分商城商品类别列表"
    optinons.colNames=['操作 ','ID', '商品类别','父级类别', '更新时间'];
    optinons.colModel=[
      {display:"操作",name:'operation',index:'', width:80, fixed:true, sortable:false, resize:false,
        formatter:operation
      },
      {display:"CLASSIFY_ID",name:'CLASSIFY_ID',index:'CLASSIFY_ID', width:60, sorttype:"int", hidden:true,editable: true},
      {display:"商品类别名称",name:'GOODS_CLASSIFY_NAME',index:'GOODS_CLASSIFY_NAME',width:120, editable:true, sorttype:"date",hidden:false,formatter:linkToDetail},
      {display:"父级类别",name:'GOODS_CLASSIFY_PNAME',index:'GOODS_CLASSIFY_PNAME',width:120, editable:true, sorttype:"date",hidden:false},
      {display:"更新时间",name:'UPDATE_TIME',index:'UPDATE_TIME', width:150, editable: true,hidden:false}
    ];
    jQuery(grid_classify).jqGrid(optinons);
    $(window).triggerHandler('resize.jqGrid');//trigger window resize to make the grid get the correct size

    function linkToDetail(v,o,r) {
      return '<a href="#page/store/classify/edit?id=' + r.CLASSIFY_ID + '">'+v+ '</a>'
    }
    
    function operation(v,o,r) {
      var html=""
      html+='<span onclick="goEdit('+r.CLASSIFY_ID+')" class="ace-icon fa fa-pencil blue bigger-150 cursor-p tt-cursor pld10 prd10"></span>'
      html+='<span onclick="del('+r.CLASSIFY_ID+')" class="ace-icon fa fa-trash-o red bigger-150 cursor-p tt-cursor"></span>'
      return html
    }
    $(document).one('ajaxloadstart.page', function(e) {
      $(grid_classify).jqGrid('GridUnload');
      $('.ui-jqdialog').remove();
    });
  });
  
  /*积分商城商品类别*/
  function renderClassifyPType(id) {
  	var url = $utils.ajax.setUrl("/store/classify/getClassifyAll");
  	axios.post(url, {})
  		.then(function (response) {
  		var classify = response.body;
  		var html = '<option value="">全部</option>'
  		$.each(classify, function (key, val) {
  		    html += '<option value="' + val.CLASSIFY_ID + '">' + val.GOODS_CLASSIFY_NAME + '</option>'
  		});
  		$("#" + id).html(html);
  	});

  }
  
});
function goEdit(id) {
  window.location.href="#page/store/classify/edit?id="+id;
}
function del(id) {
  bootbox.confirm("确定删除吗",function (result) {
    if(result){
      var url=$utils.ajax.setUrl("store/classify/deleteClassifyById");
      var params={}
      params.classifyId =id;
      axios.post(url, params)
        .then(function (response) {
          $(grid_classify).trigger("reloadGrid");
        })
        .catch(function (error) {
        });
    }
  });
}

function search() {
  var url = $utils.ajax.setUrl("store/classify/queryList");
  var params = {}
  params.goodsClassifyName = $("#goodsClassifyName").val();
  params.goodsClassifyPid = $("#classifyType").val();
  $(grid_classify).jqGrid('setGridParam', {
    url: url,
    contentType: 'application/json;charset=utf-8',
    page: 1,
    postData: params
    // 发送数据
  }).trigger("reloadGrid");
}

