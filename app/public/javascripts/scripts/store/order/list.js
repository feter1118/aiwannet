var scripts = [null, "", null];
var grid_order = "#grid-table";
var pager_order = "#grid-pager";
var url = $utils.ajax.setUrl("store/order/queryList")
$('.page-content-area').ace_ajax('loadScripts', scripts, function() {
  renderExpressCompanyAll();
  jQuery(function($) {
    $(window).on('resize.jqGrid', function () {
      $(grid_order).jqGrid( 'setGridWidth', $(".page-content").width() );
    })
    var parent_column = $(grid_order).closest('[class*="col-"]');
    $(document).on('settings.ace.jqGrid' , function(ev, event_name, collapsed) {
      if( event_name === 'sidebar_collapsed' || event_name === 'main_container_fixed' ) {
        //setTimeout is for webkit only to give time for DOM changes and then redraw!!!
        setTimeout(function() {
          $(grid_order).jqGrid( 'setGridWidth', parent_column.width() );
        }, 0);
      }
    })
    var optinons=jqSetting(grid_order,pager_order,url);
    optinons.url=url;
    optinons.caption="积分商城订单列表"
    optinons.colNames=['操作 ','ID', '订单编号','物流编号','商品编号','商品名称','兑换数量','兑换积分','兑换用户','物流公司','订单状态', '更新时间'];
    optinons.colModel=[
      {display:"操作",name:'operation',index:'', width:80, fixed:true, sortable:false, resize:false,
        formatter:operation
      },
      {display:"ORDER_ID",name:'ORDER_ID',index:'ORDER_ID', width:60, sorttype:"int", hidden:true,editable: true},
      {display:"订单编号",name:'ORDER_NO',index:'ORDER_NO',width:120, editable:true, sorttype:"date",hidden:false,formatter:linkToDetail},
      {display:"物流编号",name:'EXPRESS_NO',index:'EXPRESS_NO', width:150, editable: true,hidden:false},
      {display:"商品编号",name:'GOODS_CODE',index:'GOODS_CODE', width:150, editable: true,hidden:false},
      {display:"商品名称",name:'GOODS_NAME',index:'GOODS_NAME', width:150, editable: true,hidden:false},
      {display:"兑换数量",name:'GOODS_NUM',index:'GOODS_NUM', width:60, editable: true,hidden:false},
      {display:"兑换积分",name:'ORDER_INTEGRAL',index:'ORDER_INTEGRAL', width:60, editable: true,hidden:false},
      {display:"兑换用户",name:'BUSER_MOBILE',index:'BUSER_MOBILE', width:100, editable: true,hidden:false},
      {display:"物流公司",name:'EXPRESS_COMPANY',index:'EXPRESS_COMPANY', width:80, editable: true,hidden:false},
      {display:"订单状态",name:'ORDER_STATUS',index:'ORDER_STATUS', width:60, editable: true,hidden:false,formatter:orderStatus},
      {display:"更新时间",name:'UPDATE_TIME',index:'UPDATE_TIME', width:100, editable: true,hidden:false}
    ];
    jQuery(grid_order).jqGrid(optinons);
    $(window).triggerHandler('resize.jqGrid');//trigger window resize to make the grid get the correct size

    function linkToDetail(v,o,r) {
      return '<a href="#page/store/order/edit?id=' + r.ORDER_ID + '">'+v+ '</a>'
    }
    
    function orderStatus(v,o,r) {
    	if (v == 1) {
    		return '待付款';
    	}else if(v == 2){
    		return '已付款';
    	}else if(v == 3){
    		return '处理中';
    	}else if(v == 4){
    		return '已完成';
    	}else if(v == 5){
    		return '已取消';
    	}else if(v == 6){
    		return '待发货';
    	}else if(v == 7){
    		return '已发货';
    	}else if(v == 8){
    		return '已签收';
    	}
    }
    
    function operation(v,o,r) {
      var html=""
      html+='<span onclick="goEdit('+r.ORDER_ID+')" class="ace-icon fa fa-pencil blue bigger-150 cursor-p tt-cursor pld10 prd10"></span>'
      html+='<span onclick="del('+r.ORDER_ID+')" class="ace-icon fa fa-trash-o red bigger-150 cursor-p tt-cursor"></span>'
      return html
    }
    $(document).one('ajaxloadstart.page', function(e) {
      $(grid_order).jqGrid('GridUnload');
      $('.ui-jqdialog').remove();
    }); 
  });
  /*快递公司列表*/
  function renderExpressCompanyAll() {
  	var url = $utils.ajax.setUrl("store/order/getExpressCompanyAll");
  	axios.post(url, {})
  		.then(function (response) {
  		var companyList = response.body;
  		var html = '<option value="">全部</option>'
  		$.each(companyList, function (key, val) {
  		    html += '<option value="' + val.COMPANY_ID + '">' + val.EXPRESS_COMPANY + '</option>'
  		});
  		$("#companyId").html(html);
  	});
  }
});
function goEdit(id) {
  window.location.href="#page/store/order/edit?id="+id;
}
function del(id) {
  bootbox.confirm("确定删除吗",function (result) {
    if(result){
      var url=$utils.ajax.setUrl("store/order/deleteOrderById");
      var params={}
      params.orderId =id;
      axios.post(url, params)
        .then(function (response) {
          $(grid_order).trigger("reloadGrid");
        })
        .catch(function (error) {
        });
    }
  });
}

function search() {
  var url = $utils.ajax.setUrl("store/order/queryList");
  var params = {}
  params.orderNo = $("#orderNo").val()
  params.expressNo = $("#expressNo").val()
  params.goodsCode = $("#goodsCode").val()
  params.companyId = $("#companyId").val()
  $(grid_order).jqGrid('setGridParam', {
    url: url,
    contentType: 'application/json;charset=utf-8',
    page: 1,
    postData: params
    // 发送数据
  }).trigger("reloadGrid");
}

