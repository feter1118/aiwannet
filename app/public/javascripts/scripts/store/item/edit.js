var scripts = [null, '../assets/js/bootstrap-multiselect.js', "../assets/js/jquery.validate.js", "../assets/js/jquery.validate_msgzh.js", '../assets/js/bootstrap-tagsinput.js', null]

$('.page-content-area').ace_ajax('loadScripts', scripts, function () {

  getData();
  
  $("#update").click(function () {
      updateItem();
  })

  function updateItem() {
    var params = {}
    params.itemId = $utils.string.getQueryParam("id");
    params.itemName = $("#itemName").val();
    //params.itemDefaultValue = $("#itemDefaultValue").val();
    
    if ($utils.string.isNull(params.itemName)) {
        bootbox.alert("请输入商品数据项名称")
        return false
    }
    
    if (params.itemName.length>100) {
        bootbox.alert("商品数据项名称不能超过100字")
        return false
    }
    
    /*if (params.itemDefaultValue.length>200) {
        bootbox.alert("商品数据项默认值不能超过200字")
        return false
    }*/

    var url = $utils.ajax.setUrl("store/item/saveOrUpdateItem");
    axios.post(url, params)
      .then(function (response) {
        bootbox.alert(response.msg);
          window.location.href = "#page/store/item/list"
      })
  }

});

function getData() {
    var url=$utils.ajax.setUrl("store/item/getItemById");
    var params={}
    params.itemId=$utils.string.getQueryParam("id")
    axios.post(url, params)
      .then(function (response) {
        renderClassify(response.body)
      })
      .catch(function (error) {
      });
}

function renderClassify(body) {
    $("#itemName").val(body.itemName);
    $("#itemDefaultValue").val(body.itemDefaultValue);
}


