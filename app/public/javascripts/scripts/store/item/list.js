var scripts = [null, "", null];
var grid_item = "#grid-table";
var pager_item = "#grid-pager";
var url = $utils.ajax.setUrl("store/item/queryList")
$('.page-content-area').ace_ajax('loadScripts', scripts, function() {
  jQuery(function($) {
    $(window).on('resize.jqGrid', function () {
      $(grid_item).jqGrid( 'setGridWidth', $(".page-content").width() );
    })
    var parent_column = $(grid_item).closest('[class*="col-"]');
    $(document).on('settings.ace.jqGrid' , function(ev, event_name, collapsed) {
      if( event_name === 'sidebar_collapsed' || event_name === 'main_container_fixed' ) {
        //setTimeout is for webkit only to give time for DOM changes and then redraw!!!
        setTimeout(function() {
          $(grid_item).jqGrid( 'setGridWidth', parent_column.width() );
        }, 0);
      }
    })
    var optinons=jqSetting(grid_item,pager_item,url);
    optinons.url=url;
    optinons.caption="积分商城商品数据项列表"
    optinons.colNames=['操作 ','ID', '数据项名称'/*,'数据项默认值'*/, '更新时间'];
    optinons.colModel=[
      {display:"操作",name:'operation',index:'', width:80, fixed:true, sortable:false, resize:false,
        formatter:operation
      },
      {display:"ITEM_ID",name:'ITEM_ID',index:'ITEM_ID', width:60, sorttype:"int", hidden:true,editable: true},
      {display:"数据项名称",name:'ITEM_NAME',index:'ITEM_NAME',width:120, editable:true, sorttype:"date",hidden:false,formatter:linkToDetail},
      /*{display:"数据项默认值",name:'ITEM_DEFAULT_VALUE',index:'ITEM_DEFAULT_VALUE', width:150, editable: true,hidden:false},*/
      {display:"更新时间",name:'UPDATE_TIME',index:'UPDATE_TIME', width:150, editable: true,hidden:false}
    ];
    jQuery(grid_item).jqGrid(optinons);
    $(window).triggerHandler('resize.jqGrid');//trigger window resize to make the grid get the correct size

    function linkToDetail(v,o,r) {
      return '<a href="#page/store/item/edit?id=' + r.ITEM_ID + '">'+v+ '</a>'
    }
    
    function operation(v,o,r) {
      var html=""
      html+='<span onclick="goEdit('+r.ITEM_ID+')" class="ace-icon fa fa-pencil blue bigger-150 cursor-p tt-cursor pld10 prd10"></span>'
      html+='<span onclick="del('+r.ITEM_ID+')" class="ace-icon fa fa-trash-o red bigger-150 cursor-p tt-cursor"></span>'
      return html
    }
    $(document).one('ajaxloadstart.page', function(e) {
      $(grid_item).jqGrid('GridUnload');
      $('.ui-jqdialog').remove();
    });
  });
});
function goEdit(id) {
  window.location.href="#page/store/item/edit?id="+id;
}
function del(id) {
  bootbox.confirm("确定删除吗",function (result) {
    if(result){
      var url=$utils.ajax.setUrl("store/item/deleteItemById");
      var params={}
      params.itemId =id;
      axios.post(url, params)
        .then(function (response) {
          $(grid_item).trigger("reloadGrid");
        })
        .catch(function (error) {
        });
    }
  });
}

function search() {
  var url = $utils.ajax.setUrl("store/item/queryList");
  var params = {}
  params.itemName = $("#itemName").val()
  $(grid_item).jqGrid('setGridParam', {
    url: url,
    contentType: 'application/json;charset=utf-8',
    page: 1,
    postData: params
    // 发送数据
  }).trigger("reloadGrid");
}

