var scripts = [null, '../assets/js/bootstrap-multiselect.js', "../assets/js/jquery.validate.js", "../assets/js/jquery.validate_msgzh.js", '../assets/js/bootstrap-tagsinput.js', null]

$('.page-content-area').ace_ajax('loadScripts', scripts, function () {
  
  $("#add").click(function () {
    addItem()
  })

  function addItem() {
    var params = {}
    params.itemName = $("#itemName").val();
    //params.itemDefaultValue = $("#itemDefaultValue").val();

    if ($utils.string.isNull(params.itemName)) {
      bootbox.alert("请输入商品数据项名称")
      return false
    }
    
    if (params.itemName.length>100) {
        bootbox.alert("商品数据项名称不能超过100字")
        return false
    }

    var url = $utils.ajax.setUrl("store/item/saveOrUpdateItem");
    axios.post(url, params)
      .then(function (response) {
        bootbox.alert(response.msg);
          window.location.href = "#page/store/item/list"
      })
  }

});

