var scripts = [null, '../assets/js/bootstrap-multiselect.js', "../assets/js/jquery.validate.js", "../assets/js/jquery.validate_msgzh.js", '../assets/js/bootstrap-tagsinput.js', null]

$('.page-content-area').ace_ajax('loadScripts', scripts, function () {
  
  $("#add").click(function () {
	  addExpress()
  })

  function addExpress() {
    var params = {}
    params.expressCompany = $("#expressCompany").val();
    params.expressPhone = $("#expressPhone").val();

    if ($utils.string.isNull(params.expressCompany)) {
      bootbox.alert("请输入物流公司名称")
      return false
    }
    
    if (params.expressCompany.length>100) {
        bootbox.alert("物流公司名称不能超过100字")
        return false
    }
    
    if (params.expressPhone.length > 20) {
        bootbox.alert("物流客服电话长度不能超过20字")
        return false
    }

    var url = $utils.ajax.setUrl("store/express/saveOrUpdateExpress");
    axios.post(url, params)
      .then(function (response) {
        bootbox.alert(response.msg);
          window.location.href = "#page/store/express/list"
      })
  }

});

