var scripts = [null, "", null];
var grid_express = "#grid-table";
var pager_express = "#grid-pager";
var url = $utils.ajax.setUrl("store/express/queryList")
$('.page-content-area').ace_ajax('loadScripts', scripts, function() {
  jQuery(function($) {
    $(window).on('resize.jqGrid', function () {
      $(grid_express).jqGrid( 'setGridWidth', $(".page-content").width() );
    })
    var parent_column = $(grid_express).closest('[class*="col-"]');
    $(document).on('settings.ace.jqGrid' , function(ev, event_name, collapsed) {
      if( event_name === 'sidebar_collapsed' || event_name === 'main_container_fixed' ) {
        //setTimeout is for webkit only to give time for DOM changes and then redraw!!!
        setTimeout(function() {
          $(grid_express).jqGrid( 'setGridWidth', parent_column.width() );
        }, 0);
      }
    })
    var optinons=jqSetting(grid_express,pager_express,url);
    optinons.url=url;
    optinons.caption="物流公司列表"
    optinons.colNames=['操作 ','ID', '物流公司名称','物流客服电话', '更新时间'];
    optinons.colModel=[
      {display:"操作",name:'operation',index:'', width:80, fixed:true, sortable:false, resize:false,
        formatter:operation
      },
      {display:"COMPANY_ID",name:'COMPANY_ID',index:'COMPANY_ID', width:60, sorttype:"int", hidden:true,editable: true},
      {display:"物流公司名称",name:'EXPRESS_COMPANY',index:'EXPRESS_COMPANY',width:120, editable:true, sorttype:"date",hidden:false,formatter:linkToDetail},
      {display:"物流客服电话",name:'EXPRESS_PHONE',index:'EXPRESS_PHONE', width:150, editable: true,hidden:false},
      {display:"更新时间",name:'UPDATE_TIME',index:'UPDATE_TIME', width:150, editable: true,hidden:false}
    ];
    jQuery(grid_express).jqGrid(optinons);
    $(window).triggerHandler('resize.jqGrid');//trigger window resize to make the grid get the correct size

    function linkToDetail(v,o,r) {
      return '<a href="#page/store/express/edit?id=' + r.COMPANY_ID + '">'+v+ '</a>'
    }
    
    function operation(v,o,r) {
      var html=""
      html+='<span onclick="goEdit('+r.COMPANY_ID+')" class="ace-icon fa fa-pencil blue bigger-150 cursor-p tt-cursor pld10 prd10"></span>'
      html+='<span onclick="del('+r.COMPANY_ID+')" class="ace-icon fa fa-trash-o red bigger-150 cursor-p tt-cursor"></span>'
      return html
    }
    $(document).one('ajaxloadstart.page', function(e) {
      $(grid_express).jqGrid('GridUnload');
      $('.ui-jqdialog').remove();
    });
  });
});
function goEdit(id) {
  window.location.href="#page/store/express/edit?id="+id;
}
function del(id) {
  bootbox.confirm("确定删除吗",function (result) {
    if(result){
      var url=$utils.ajax.setUrl("store/express/deleteExpressById");
      var params={}
      params.companyId =id;
      axios.post(url, params)
        .then(function (response) {
          $(grid_express).trigger("reloadGrid");
        })
        .catch(function (error) {
        });
    }
  });
}

function search() {
  var url = $utils.ajax.setUrl("store/express/queryList");
  var params = {}
  params.expressCompany = $("#expressCompany").val()
  $(grid_express).jqGrid('setGridParam', {
    url: url,
    contentType: 'application/json;charset=utf-8',
    page: 1,
    postData: params
    // 发送数据
  }).trigger("reloadGrid");
}

