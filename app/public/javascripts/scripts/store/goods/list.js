var scripts = [null, "", null];
var grid_goods = "#grid-table";
var pager_goods = "#grid-pager";
var url = $utils.ajax.setUrl("store/goods/queryList")
$('.page-content-area').ace_ajax('loadScripts', scripts, function() {
  jQuery(function($) {
    $(window).on('resize.jqGrid', function () {
      $(grid_goods).jqGrid( 'setGridWidth', $(".page-content").width() );
    })
    var parent_column = $(grid_goods).closest('[class*="col-"]');
    $(document).on('settings.ace.jqGrid' , function(ev, event_name, collapsed) {
      if( event_name === 'sidebar_collapsed' || event_name === 'main_container_fixed' ) {
        //setTimeout is for webkit only to give time for DOM changes and then redraw!!!
        setTimeout(function() {
          $(grid_goods).jqGrid( 'setGridWidth', parent_column.width() );
        }, 0);
      }
    })
    var optinons=jqSetting(grid_goods,pager_goods,url);
    optinons.url=url;
    optinons.caption="积分商城商品列表"
    optinons.colNames=['操作 ','ID', '商品名称', '商品编号','商品分类','商品兑换积分','商品总数','商品库存量','商品发布状态', '更新时间'];
    optinons.colModel=[
      {display:"操作",name:'operation',index:'', width:80, fixed:true, sortable:false, resize:false,
        formatter:operation
      },
      {display:"GOODS_ID",name:'GOODS_ID',index:'GOODS_ID', width:60, sorttype:"int", hidden:true,editable: true},
      {display:"商品名称",name:'GOODS_NAME',index:'GOODS_NAME',width:150, editable:true, sorttype:"date",hidden:false,formatter:linkToDetail},
      {display:"商品编号",name:'GOODS_CODE',index:'GOODS_CODE', width:100, editable: true,hidden:false},
      {display:"商品分类",name:'GOODS_CLASSIFY_NAME',index:'GOODS_CLASSIFY_NAME',width:90, editable:true, sorttype:"date",hidden:false},
      {display:"商品兑换积分",name:'GOODS_INTEGRAL',index:'GOODS_INTEGRAL', width:90, editable: true,hidden:false},
      {display:"商品总数",name:'GOODS_SUM',index:'GOODS_SUM', width:150,editable: true,hidden:false},
      {display:"商品剩余数量",name:'GOODS_REMAIN_NUM',index:'GOODS_REMAIN_NUM', width:150,editable: true,hidden:false},
      {display:"商品发布状态",name:'GOODS_STATUS',index:'GOODS_STATUS', width:150,editable: false, sortable:false,hidden:false,
    	  formatter: function (v, o, r) {
              if (v == 1) {
                return '<span class="prd10">已发布</span>' + '<span class="btn btn-sm btn-white" onclick="changePublish(' + r.GOODS_ID + ',' + '0' + ')">暂存</span>'
              } else {
                return '<span class="prd10">暂存</span>' + '<span class="btn btn-sm btn-white" onclick="changePublish(' + r.GOODS_ID + ',' + '1' + ')">发布</span>'
              }
            }
      },
      {display:"更新时间",name:'UPDATE_TIME',index:'UPDATE_TIME', width:150, editable: true,hidden:false}
    ];
    jQuery(grid_goods).jqGrid(optinons);
    $(window).triggerHandler('resize.jqGrid');//trigger window resize to make the grid get the correct size

    function linkToDetail(v,o,r) {
      return '<a href="#page/store/goods/edit?id=' + r.GOODS_ID + '">'+v+ '</a>'
    }
    
    function operation(v,o,r) {
      var html=""
      html+='<span onclick="goEdit('+r.GOODS_ID+')" class="ace-icon fa fa-pencil blue bigger-150 cursor-p tt-cursor pld10 prd10"></span>'
      html+='<span onclick="del('+r.GOODS_ID+')" class="ace-icon fa fa-trash-o red bigger-150 cursor-p tt-cursor"></span>'
      return html
    }
    $(document).one('ajaxloadstart.page', function(e) {
      $(grid_goods).jqGrid('GridUnload');
      $('.ui-jqdialog').remove();
    });
  });
});
function goEdit(id) {
  window.location.href="#page/store/goods/edit?id="+id;
}
function del(id) {
  bootbox.confirm("确定删除吗",function (result) {
    if(result){
      var url=$utils.ajax.setUrl("store/goods/deleteGoodsById");
      var params={}
      params.goodsId =id;
      axios.post(url, params)
        .then(function (response) {
          $(grid_goods).trigger("reloadGrid");
        })
        .catch(function (error) {
        });
    }
  });
}

function search() {
  var url = $utils.ajax.setUrl("store/goods/queryList");
  var params = {}
  params.goodsCode = $("#goodsCode").val()
  params.goodsName = $("#goodsName").val()
  params.goodsStatus = $("#goodsStatus").val()
  $(grid_goods).jqGrid('setGridParam', {
    url: url,
    contentType: 'application/json;charset=utf-8',
    page: 1,
    postData: params
    // 发送数据
  }).trigger("reloadGrid");
}

/**
 * 发布，暂存
 * @param id
 * @param type
 */
function changePublish(id,type) {
	  var msg='';
	  if(type==0){
	    msg="确认要暂存吗"
	  }else{
	    msg="确认要发布吗"
	  }
	  var url=$utils.ajax.setUrl("store/goods/publishGoods");
	  var params={}
	  params.goodsId=id
	  params.goodsStatus=type
	  bootbox.confirm(msg,function (v) {
	    if(v){
	      axios.post(url, params)
	        .then(function (response) {
	          $(grid_goods).trigger("reloadGrid");
	        })
	    }
	  })
	}

