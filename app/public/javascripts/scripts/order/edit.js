var scripts = [null, "../assets/js/jquery.validate.js", "../assets/js/jquery.validate_msgzh.js", null]
$('.page-content-area').ace_ajax('loadScripts', scripts, function () {
  jQuery(function ($) {
    getOrderById()

    function getOrderById() {
      var url = $utils.ajax.setUrl("order/getOrderById");
      var params = {}
      params.orderId = $utils.string.getQueryParam("id")
      return axios.post(url, params)
        .then(function (response) {
          renderOrder(response.body)
        })
        .catch(function (error) {
        });
    }

    function renderOrder(body) {
      $("#orderNo").val(body.ORDER_NO)
      $("#productName").val(body.PRODUCT_NAME)
      $("#orderOriginalPrice").val(body.ORDER_ORIGINAL_PRICE)
      $("#orderTransactPrice").val(body.ORDER_TRANSACT_PRICE)
      $("#orderDiscountRate").val(body.ORDER_DISCOUNT_RATE * 100 + '%')
      $("#orderProductPrice").val(body.ORDER_PRODUCT_PRICE)
      $("#orderProductChildrenPrice").val(body.ORDER_CHILDREN_PRICE)
      $("#orderIsStage").val(body.ORDER_IS_STAGE)
      $("#createTime").val(moment(body.CREATE_TIME).format("YYYY-MM-DD"))
      $("#priceDate").val(body.PRICE_DATE)
      $("#orderActualPrice").val(body.ORDER_ACTUAL_PRICE)
      $("#orderPersonNum").val(body.ORDER_PERSON_NUM)
      $("#orderChildrenNum").val(body.ORDER_CHILDREN_NUM)
      $("#orderRoomNum").val(body.ORDER_ROOM_NUM)
      $("#orderStatus").val(body.ORDER_STATUS)

      renderOrderInsuranceList(body.insuranceList)
      renderOrderPassengerList(body.passengerList)
    }

    $("#update").click(function () {
      updateOrder()
    })

    function updateOrder() {
      var url = $utils.ajax.setUrl("order/updateOrderById");
      var params = {}
      params.orderId = $utils.string.getQueryParam("id")
      params.orderActualPrice = $("#orderActualPrice").val()
      params.orderPersonNum = $("#orderPersonNum").val()
      params.orderRoomNum = $("#orderRoomNum").val()
      params.orderStatus = $("#orderStatus").val()
      axios.post(url, params)
        .then(function (response) {
          bootbox.alert(response.msg)
          window.location.href = "#page/order/list"
        })
    }


    function renderOrderInsuranceList(insuranceList) {

      if (insuranceList == null) {
        insuranceList = []
      }

      var grid_selector = "#grid-table-InsuranceList";
      var options = jqSettingLocal(grid_selector, insuranceList);
      options.colNames = ['险种', '险种内容','数量' ,'单价','总费'];
      options.colModel = [
        {
          display:'险种',
          name: 'INSURANCE_TYPE',
          index: 'INSURANCE_TYPE',
          width: 160,
          align: "center",
          editable: false,
          "sortable": false
        },
        {
          display:'险种内容',
          name: 'INSURANCE_NAME',
          index: 'INSURANCE_NAME',
          width: 160,
          align: "center",
          editable: false,
          "sortable": false
        },
        {
          display:'数量',
          name: 'INSURANCE_NUM',
          index: 'INSURANCE_NUM',
          width: 50,
          align: "center",
          editable: false,
          "sortable": false
        },
        {
          display:'单价',
          name: 'INSURANCE_PRICE',
          index: 'INSURANCE_PRICE',
          width: 50,
          align: "center",
          editable: false,
          "sortable": false
        },
        {
          display:'总费',
          name: 'INSURANCE_TOTAL',
          index: 'INSURANCE_TOTAL',
          width: 50,
          align: "center",
          editable: false,
          "sortable": false
        }
      ];
      /*jqGrid 初始化*/
      jQuery(grid_selector).jqGrid(options);
      //初始化表格宽度
      var parent_column = $(grid_selector).parents('.tab-content').width() - 15;
      initGridWidth(grid_selector, parent_column);
    }


    function renderOrderPassengerList(passengerList) {
      if (passengerList == null) {
        passengerList = []
      }
      var grid_selector = "#grid-table-Passenger";
      var options = jqSettingLocal(grid_selector, passengerList);
      options.colNames = ['姓名', '类型','证件类型' ,'证件号'];
      options.colModel = [
        {
          display:'姓名',
          name: 'PASSENGER_NAME',
          index: 'PASSENGER_NAME',
          width: 50,
          align: "center",
          editable: false,
          "sortable": false
        },
        {
          display:'类型',
          name: 'PASSENGER_CERT_TYPE',
          index: 'PASSENGER_CERT_TYPE',
          width: 50,
          align: "center",
          editable: false,
          "sortable": false,
          formatter:function (v,o,r) {
            return  PASSENGERTYPE(v)
          }
        },
        {
          display:'证件类型',
          name: 'PASSENGER_CERT_TYPE',
          index: 'PASSENGER_CERT_TYPE',
          width: 50,
          align: "center",
          editable: false,
          "sortable": false,
          formatter:function (v,o,r) {
           return CERTTYPE(v)
          }
        },
        {
          display:'证件号',
          name: 'PASSENGER_CERT_NO',
          index: 'PASSENGER_CERT_NO',
          width: 50,
          align: "center",
          editable: false,
          "sortable": false
        }
      ];
      /*jqGrid 初始化*/
      jQuery(grid_selector).jqGrid(options);
      //初始化表格宽度
      var parent_column = $(grid_selector).parents('.tab-content').width() - 15;
      initGridWidth(grid_selector, parent_column);
    }
  });
});
