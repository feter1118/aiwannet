var scripts = [null, "../assets/js/jquery.validate.js", "../assets/js/jquery.validate_msgzh.js", null]
$('.page-content-area').ace_ajax('loadScripts', scripts, function () {
  jQuery(function ($) {
    getOrderById()

    function getOrderById() {
      var url = $utils.ajax.setUrl("order/getOrderById");
      var params = {}
      params.orderId = $utils.string.getQueryParam("id")
      return axios.post(url, params)
        .then(function (response) {
          renderOrder(response.body)
        })
        .catch(function (error) {
        });
    }

    function renderOrder(body) {
      $("#orderNo").val(body.ORDER_NO)
      $("#productName").val(body.PRODUCT_NAME)
      $("#orderOriginalPrice").val(body.ORDER_ORIGINAL_PRICE)
      $("#orderTransactPrice").val(body.ORDER_TRANSACT_PRICE)
      $("#orderDiscountRate").val(body.ORDER_DISCOUNT_RATE * 100 + '%')
      $("#orderProductPrice").val(body.ORDER_PRODUCT_PRICE)
      $("#orderIsStage").val(body.ORDER_IS_STAGE)
      $("#createTime").val(moment(body.CREATE_TIME).format("YYYY-MM-DD"))
      $("#priceDate").val(body.PRICE_DATE)
      $("#orderActualPrice").val(body.ORDER_ACTUAL_PRICE)
      $("#orderPersonNum").val(body.ORDER_PERSON_NUM)
      $("#orderChildrenNum").val(body.ORDER_CHILDREN_NUM)
      $("#orderRoomNum").val(body.ORDER_ROOM_NUM)
      $("#orderStatus").val(body.ORDER_STATUS)
    }
  });
});
