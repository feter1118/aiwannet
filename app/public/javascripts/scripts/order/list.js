var scripts = [null, "../assets/js/date-time/bootstrap-datepicker.js",'../assets/js/date-time/bootstrap-timepicker.js',null];
var grid_selectorOrder= "#grid-table";
var pager_selectorOrder= "#grid-pager";
var url = $utils.ajax.setUrl("order/queryList")
$('.page-content-area').ace_ajax('loadScripts', scripts, function() {
  jQuery(function($) {
    $('.date-picker').datepicker({
      language: 'ch',
      autoclose: true,
      todayHighlight: true
    });
    $(window).on('resize.jqGrid', function () {
      $(grid_selectorOrder).jqGrid( 'setGridWidth', $(".page-content").width() );
    })
    var parent_column = $(grid_selectorOrder).closest('[class*="col-"]');
    $(document).on('settings.ace.jqGrid' , function(ev, event_name, collapsed) {
      if( event_name === 'sidebar_collapsed' || event_name === 'main_container_fixed' ) {
        //setTimeout is for webkit only to give time for DOM changes and then redraw!!!
        setTimeout(function() {
          $(grid_selectorOrder).jqGrid( 'setGridWidth', parent_column.width() );
        }, 0);
      }
    })
    var optinons=jqSetting(grid_selectorOrder,pager_selectorOrder,url);
    optinons.url=url;
    optinons.caption="订单列表"
    if($utils.string.getQueryParam("wait")==1){
      $("#status").val(1)
      optinons.postData={
        orderStatus:1
      }
    }
    optinons.colNames=['操作 ','PRODUCT_ID', 'ORDER_ID','BUSER_ID','ACCOUNT_MEMBER_GRADE','订单编号','产品名称','购买者','用户手机','原价','实收价格', '交易价格', '人数','产品单价','房间价格','房间数','是否分期','状态','下单时间'];
    optinons.colModel=[
      {name:'operation',index:'', width:80, fixed:true, sortable:false, resize:false, formatter:operation },
      {display:"产品id",name:'PRODUCT_ID',index:'PRODUCT_ID', width:60, sorttype:"int", hidden:true,editable: true},
      {display:"订单id",name:'ORDER_ID',index:'ORDER_ID',width:90, editable:true, sorttype:"date",hidden:true},
      {display:"购买者id",name:'ACCOUNT_MEMBER_GRADE',index:'ACCOUNT_MEMBER_GRADE',width:90, editable:true, sorttype:"date",hidden:true},
      {display:"购买者等级",name:'BUSER_ID',index:'BUSER_ID',width:90, editable:true, sorttype:"date",hidden:true},
      {display:"订单编号",name:'ORDER_NO',index:'ORDER_NO',width:120, editable:true,hidden:false,formatter:goOrderDetail},
      {display:"产品名称",name:'PRODUCT_NAME',index:'PRODUCT_NAME',width:150, editable:true,hidden:false},
      {display:"购买者",name:'BUSER_NAME',index:'BUSER_NAME',width:150, editable:true,hidden:false,formatter:goUserDetail},
      {display:"用户手机",name:'BUSER_MOBILE',index:'BUSER_MOBILE', width:120,editable: false},
      {display:"原价",name:'ORDER_ORIGINAL_PRICE',index:'ORDER_ORIGINAL_PRICE', width:90,editable: false},
      {display:"实收",name:'ORDER_ACTUAL_PRICE',index:'ORDER_ACTUAL_PRICE', width:90,editable: false},
      {display:"交易价格",name:'ORDER_TRANSACT_PRICE',index:'ORDER_TRANSACT_PRICE', width:70, editable: false},
      {display:"人数",name:'ORDER_PERSON_NUM',index:'ORDER_PERSON_NUM', width:50, editable: false},
      {display:"产品单价",name:'ORDER_PRODUCT_PRICE',index:'ORDER_PRODUCT_PRICE', width:90, editable: false},
      {display:"房间价格",name:'ORDER_ROOM_PRICE',index:'ORDER_ROOM_PRICE', width:90, editable: false},
      {display:"房间数",name:'ORDER_ROOM_NUM',index:'ORDER_ROOM_NUM', width:50, editable: false},
      {display:"是否分期",name:'ORDER_IS_STAGE',index:'ORDER_IS_STAGE', width:50, editable: false,formatter:function (v,o,r) {
          if(v==0){
            return '否'
          }else{
            return '是'
          }
        }},
      {display:"状态",name:'ORDER_STATUS',index:'ORDER_STATUS', width:90, editable: false,formatter:function (v,o,r) {
          return renderProductStatus(v)
        }},
      {display:"下单时间",name:'CREATE_TIME',index:'CREATE_TIME', width:120, sortable:false,editable: false,formatter:pickDate}
    ];
    jQuery(grid_selectorOrder).jqGrid(optinons);
    $(window).triggerHandler('resize.jqGrid');//trigger window resize to make the grid get the correct size

    function operation(v,o,r) {
      var html=""
      html+='<span onclick="del('+r.ORDER_ID+')" class="ace-icon fa fa-trash-o red bigger-150 cursor-p tt-cursor"></span>'
      return html
    }
    function goOrderDetail(v,o,r){
      var html="";
      html += '<a href="#page/order/edit?id=' + r.ORDER_ID + '">'+ r.ORDER_NO + '</a>';
      return html;
    }
    function goUserDetail(v,o,r) {
      var html=""
      var GRADE = MEMBER_GRADE_TYPE2(r.ACCOUNT_MEMBER_GRADE)
      var html = ""
      if (r.BUSER_NAME == null || r.BUSER_NAME == "") {
        v = "匿名"
      }
      html += '<a href="#page/biuser/detail?id=' + r.BUSER_ID + '">'+ GRADE+ r.BUSER_NAME + '</a>'
      return html
    }

    $(document).one('ajaxloadstart.page', function(e) {
      $(grid_selectorOrder).jqGrid('GridUnload');
      $('.ui-jqdialog').remove();
    });
  });
});
function goEdit(id) {
  window.location.href="#page/order/edit?id="+id;
}
function del(id) {
  bootbox.confirm("确定删除吗",function (result) {
    if(result){
      var url=$utils.ajax.setUrl("order/deleteOrderById");
      var params={}
      params.orderId = id;
      axios.post(url, params)
        .then(function (response) {
          $(grid_selectorOrder).trigger("reloadGrid");
        })
        .catch(function (error) {
        });
    }
  });
}

function search() {
  var url = $utils.ajax.setUrl("order/queryList");
  var params = {};
  params.productName = $("#productName").val();
  params.phone = $("#phone").val();
  params.orderStatus = $("#status").val();
  params.startTime = $("#startTime").val();
  params.endTime = $("#endTime").val();
  $(grid_selectorOrder).jqGrid('setGridParam', {
    url: url,
    contentType: 'application/json;charset=utf-8',
    page: 1,
    postData: params
    // 发送数据
  }).trigger("reloadGrid");
}
