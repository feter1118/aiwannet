var scripts = [null, "", null];
var grid_welfare = "#grid-table";
var pager_welfare = "#grid-pager";
var url = $utils.ajax.setUrl("buserBenefits/queryList")
$('.page-content-area').ace_ajax('loadScripts', scripts, function () {
  jQuery(function ($) {
    $(window).on('resize.jqGrid', function () {
      $(grid_welfare).jqGrid('setGridWidth', $(".page-content").width());
    })
    var parent_column = $(grid_welfare).closest('[class*="col-"]');
    $(document).on('settings.ace.jqGrid', function (ev, event_name, collapsed) {
      if (event_name === 'sidebar_collapsed' || event_name === 'main_container_fixed') {
        //setTimeout is for webkit only to give time for DOM changes and then redraw!!!
        setTimeout(function () {
          $(grid_welfare).jqGrid('setGridWidth', parent_column.width());
        }, 0);
      }
    })
    var optinons = jqSetting(grid_welfare, pager_welfare, url);
    optinons.url = url;
    optinons.caption = "福利管理"
    optinons.colNames = ['操作 ', 'BENEFITS_ID', '用户名称', '手机号', '会员等级', '福利内容', '已使用福利', '方案类型', '领取日期', '失效日期'];
    optinons.colModel = [
      {
        name: 'operation', index: '', width: 80, fixed: true, sortable: false, resize: false,
        formatter: operation
      },
      {
        display: "BENEFITS_ID",
        name: 'BENEFITS_ID',
        index: 'BENEFITS_ID',
        width: 60,
        sorttype: "int",
        hidden: true,
        editable: true
      },
      {
        display: "用户名称",
        name: 'BUSER_NAME',
        index: 'BUSER_NAME',
        width: 90,
        editable: true,
        sorttype: "date",
        hidden: false
      },
      {
        display: "手机号",
        name: 'BUSER_MOBILE',
        index: 'BUSER_MOBILE',
        width: 90,
        editable: true,
        sorttype: "date",
        hidden: false
      },
      {
        display: "会员等级",
        name: 'ACCOUNT_MEMBER_GRADE',
        index: 'ACCOUNT_MEMBER_GRADE',
        width: 90,
        editable: true,
        sorttype: "date",
        hidden: false,
        formatter: function (v, o, r) {
          return MEMBER_GRADE(v)
        }
      },
      {
        display: "福利内容",
        name: 'BENEFITS_CONTENT',
        index: 'BENEFITS_CONTENT',
        width: 180,
        editable: true,
        sorttype: "date",
        hidden: false
      },
      {
        display: "已使用福利",
        name: 'USED_CONTENT',
        index: 'USED_CONTENT',
        width: 180,
        editable: true,
        sorttype: "date",
        hidden: false
      },
      {
        display: "方案类型",
        name: 'BENEFITS_TITLE',
        index: 'BENEFITS_TITLE',
        width: 90,
        editable: true,
        sorttype: "date",
        hidden: false
      },
      {
        display: "领取日期",
        name: 'CREATE_TIME',
        index: 'CREATE_TIME',
        width: 90,
        editable: true,
        sorttype: "date",
        hidden: false,
        formatter: pickDate
      },
      {
        display: "失效时间",
        name: 'BENEFITS_EXPIRED_DATE',
        index: 'BENEFITS_EXPIRED_DATE',
        width: 120,
        editable: true,
        sorttype: "date",
        hidden: false,
        formatter: pickDate
      },
    ];
    jQuery(grid_welfare).jqGrid(optinons);
    $(window).triggerHandler('resize.jqGrid');//trigger window resize to make the grid get the correct size
    function theme(v, o, r) {
      console.log(r)

      var html = '<a href="#page/seckill/edit?id=' + r.SECKILL_ID + '">' + v + '</a>'
      return html
    }

    function status(v, o, r) {
      var html = ""
      if (r.SECKILL_IS_OPEN == 1) {
        html = "开启";
      } else {
        html = "关闭";
      }
      return html
    }

    function operation(v, o, r) {
      var html = ""
      html += '<span onclick="del(' + r.BENEFITS_ID + ')" class="ace-icon fa fa-trash-o red bigger-150 cursor-p tt-cursor"></span>'
      return html
    }

    $(document).one('ajaxloadstart.page', function (e) {
      $(grid_welfare).jqGrid('GridUnload');
      $('.ui-jqdialog').remove();
    });


  });
});

function search() {
  var params = {}
  params.usedStatus = $("#usedStatus").val();
  params.memberGrade = $("#memberGrade").val();
  params.userPhone = $("#userPhone").val();
  console.info(params);
  $(grid_welfare).jqGrid('setGridParam', {
    url: url,
    contentType: 'application/json;charset=utf-8',
    page: 1,
    postData: params
    // 发送数据
  }).trigger("reloadGrid"); // 重新载入
}

function del(id) {
  bootbox.confirm("确定删除吗", function (result) {
    if (result) {
      var url = $utils.ajax.setUrl("buserBenefits/deleteUserBenefit");
      var params = {}
      params.benefitsId = id;
      axios.post(url, params)
        .then(function (response) {
          $(grid_welfare).trigger("reloadGrid");
        })
        .catch(function (error) {
        });
    }
  });
}
