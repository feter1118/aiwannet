var scripts = [null, "", null];
var grid_sysDomain = "#grid-table";
var pager_sysRole = "#grid-pager";
var url = $utils.ajax.setUrl("domain/queryList")
$('.page-content-area').ace_ajax('loadScripts', scripts, function() {
  jQuery(function($) {
    var urlResourceCategory=$utils.ajax.setUrl("dicItem/getDicItemListByDicCode");
    var params={}
    params.dicCode ='D00000011'
    axios.post(urlResourceCategory, params)
      .then(function (response) {
        console.info(response)
        var array = response.body;
        for(var i=0;i<array.length;i++){
          $("#resourceCategory").append("<option value='"+array[i].ITEM_CODE+"'>"+array[i].ITEM_NAME+"</option>");
        }
      })
      .catch(function (error) {
      });

    $(window).on('resize.jqGrid', function () {
      $(grid_sysDomain).jqGrid( 'setGridWidth', $(".page-content").width() );
    })
    var parent_column = $(grid_sysDomain).closest('[class*="col-"]');
    $(document).on('settings.ace.jqGrid' , function(ev, event_name, collapsed) {
      if( event_name === 'sidebar_collapsed' || event_name === 'main_container_fixed' ) {
        //setTimeout is for webkit only to give time for DOM changes and then redraw!!!
        setTimeout(function() {
          $(grid_sysDomain).jqGrid( 'setGridWidth', parent_column.width() );
        }, 0);
      }
    })
    var optinons=jqSetting(grid_sysDomain,pager_sysRole,url);
    optinons.url=url;
    optinons.caption="域名列表"
    optinons.colNames=['操作 ','ID', '域名', '应用范围', '创建时间'];
    optinons.colModel=[
      {name:'operation',index:'', width:80, fixed:true, sortable:false, resize:false, hidden:true, formatter:operation },
      {display:"DOMAIN_ID",name:'DOMAIN_ID',index:'DOMAIN_ID', width:60, sorttype:"int", hidden:true,editable: true},
      {display:"域名",name:'DOMAIN_NAME',index:'DOMAIN_NAME',width:90, editable:true, sorttype:"date",hidden:false},
      {display:"应用范围",name:'ITEM_NAME',index:'ITEM_NAME',width:90, editable:true, sorttype:"date",hidden:false},
      {display:"创建时间",name:'UPDATE_TIME',index:'UPDATE_TIME', width:150, sortable:false,editable: false,formatter:pickDate}
    ];
    jQuery(grid_sysDomain).jqGrid(optinons);
    $(window).triggerHandler('resize.jqGrid');//trigger window resize to make the grid get the correct size

    function operation(v,o,r) {
      var html=""
      html+='<span onclick="goEdit('+r.DOMAIN_ID+')" class="ace-icon fa fa-pencil blue bigger-150 cursor-p tt-cursor pld10 prd10"></span>'
      html+='<span onclick="del('+r.DOMAIN_ID+')" class="ace-icon fa fa-trash-o red bigger-150 cursor-p tt-cursor"></span>'
      return html
    }
    $(document).one('ajaxloadstart.page', function(e) {
      $(grid_sysDomain).jqGrid('GridUnload');
      $('.ui-jqdialog').remove();
    });
  });
});

function search(){
  var params={}
  params.domainName =$("#domainName").val();
  params.resourceCategory=$("#resourceCategory").val();
  $(grid_sysDomain).jqGrid('setGridParam', {
    url: url,
    contentType: 'application/json;charset=utf-8',
    page: 1,
    postData: params
    // 发送数据
  }).trigger("reloadGrid"); // 重新载入
}


function goEdit(id) {
  window.location.href="#page/domain/edit?id="+id;
}
function del(id) {
    bootbox.confirm("确定删除吗",function (result) {
    if(result){
      var url=$utils.ajax.setUrl("domain/deleteDomainById");
      var params={}
      params.domainId=id;
      axios.post(url, params)
        .then(function (response) {
         $(grid_sysDomain).trigger("reloadGrid");
        })
        .catch(function (error) {
        });
     }
     });
}
