var scripts = [null, "../assets/js/jquery.validate.js", "../assets/js/jquery.validate_msgzh.js",null]
$('.page-content-area').ace_ajax('loadScripts', scripts, function () {
  jQuery(function ($) {
    getData()
    function getData() {
      var urlResourceCategory=$utils.ajax.setUrl("dicItem/getDicItemListByDicCode");
      var params={}
      params.dicCode ='D00000011'
      axios.post(urlResourceCategory, params)
        .then(function (response) {
          console.info(response)
          var array = response.body;
          for(var i=0;i<array.length;i++){
            $("#resourceCategory").append("<option value='"+array[i].ITEM_CODE+"'>"+array[i].ITEM_NAME+"</option>");
          }
        })
        .catch(function (error) {
        });

      var url=$utils.ajax.setUrl("domain/getDomainById");
      var params={}
      params.domainId=$utils.string.getQueryParam("id")
      axios.post(url, params)
        .then(function (response) {
          renderSysDomain(response.body)
        })
        .catch(function (error) {
        });
    }

    function renderSysDomain(body) {
      $("#domainName").val(body.domainName)
      $("#resourceCategory").val(body.resourceCategory)
    }

    $("#add").click(function () {
      updateSysDomain()
    })

    function updateSysDomain() {
      var url=$utils.ajax.setUrl("domain/updateDomainById");
      var params={}
      params.domainId=$utils.string.getQueryParam("id")
      params.domainName=$("#domainName").val()
      params.resourceCategory=$("#resourceCategory").val()
      var flag = $("#editForm").valid();
      if(!flag){
        return false
      }

      axios.post(url, params)
        .then(function (response) {
          window.location.href = "#page/domain/list"
        })
        .catch(function (error) {
        });

    }
  });


});
