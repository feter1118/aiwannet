var scripts = [null, "../assets/js/jquery.validate.js", "../assets/js/jquery.validate_msgzh.js",null]
$('.page-content-area').ace_ajax('loadScripts', scripts, function () {
  jQuery(function ($) {

    var url=$utils.ajax.setUrl("dicItem/getDicItemListByDicCode");
    var params={}
    params.dicCode ='D00000011'
    axios.post(url, params)
      .then(function (response) {
        console.info(response)
        var array = response.body;
        for(var i=0;i<array.length;i++){
          $("#resourceCategory").append("<option value='"+array[i].ITEM_CODE+"'>"+array[i].ITEM_NAME+"</option>");
        }
      })
      .catch(function (error) {
      });

    $("#add").click(function () {
      addSysDomain()
    })
  });
  function addSysDomain() {

    var params={}
    params.domainName=$("#domainName").val()
    params.resourceCategory=$("#resourceCategory").val()
    var flag = $("#editForm").valid();
    if(!flag){
      return false
    }
    var url=$utils.ajax.setUrl("domain/addDomain");
    axios.post(url, params)
      .then(function (response) {
        window.location.href="#page/domain/list"
      })
      .catch(function (error) {
      });
  }
});
