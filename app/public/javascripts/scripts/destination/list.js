var scripts = [null, "", null];
var grid_destination = "#grid-table";
var pager_destination = "#grid-pager";
var url = $utils.ajax.setUrl("destination/queryList")
$('.page-content-area').ace_ajax('loadScripts', scripts, function () {
  jQuery(function ($) {
    $(window).on('resize.jqGrid', function () {
      $(grid_destination).jqGrid('setGridWidth', $(".page-content").width());
    })
    var parent_column = $(grid_destination).closest('[class*="col-"]');
    $(document).on('settings.ace.jqGrid', function (ev, event_name, collapsed) {
      if (event_name === 'sidebar_collapsed' || event_name === 'main_container_fixed') {
        //setTimeout is for webkit only to give time for DOM changes and then redraw!!!
        setTimeout(function () {
          $(grid_destination).jqGrid('setGridWidth', parent_column.width());
        }, 0);
      }
    })
    var optinons = jqSetting(grid_destination, pager_destination, url);
    optinons.url = url;
    optinons.caption = "目的地管理"
    optinons.colNames = ['操作 ', 'ID', '名称', '描述','类型', '国内外','层级', '排序', '是否热门', '创建时间','主图',];
    optinons.colModel = [
      {
        name: 'operation', index: '', width: 80, fixed: true, sortable: false, resize: false,
        formatter: operation
      },
      {display: "DESTINATION_ID", name: 'DESTINATION_ID', index: 'DESTINATION_ID', width: 60, sorttype: "int", hidden: true, editable: true},
      {
        display: "名称",
        name: 'DESTINATION_NAME',
        index: 'DESTINATION_NAME',
        width: 90,
        editable: true,
        hidden: false
      }, {
        display: "描述",
        name: 'DESTINATION_DETAIL',
        index: 'DESTINATION_DETAIL',
        width: 150,
        sortable: false,
        editable: false
      },
      {
        display: "",
        name: 'DESTINATION_TYPE',
        index: 'DESTINATION_TYPE',
        width: 60,
        editable: true,
        sorttype: "date",
        hidden: false,
        formatter:function (v,o,r) {
          if(v==-1){
            return '周边游'
          }else if(v==0){
            return '国内游'
          }else if(v==1){
            return '海岛游'
          }else if(v==2){
            return '日本、韩国、蒙古'
          }else if(v==3){
            return '东南亚、南亚洲'
          }else if(v==4){
            return '欧洲'
          }else if(v==5){
            return '美洲'
          }else if(v==6){
            return '澳新、中东非洲'
          }else if(v==7){
            return '邮轮'
          }else if(v==8){
            return '极地'
          }else{
            return ''
          }
        }
      },
      {
        display: "",
        name: 'TYPE',
        index: 'TYPE',
        width: 60,
        editable: true,
        sorttype: "date",
        hidden: false,
        formatter:function (v,o,r) {
          if(v==1){
            return '国内'
          }else{
            return '国外'
          }
        }
      },
      {
        display: "",
        name: 'AREA_ID',
        index: 'AREA_ID',
        width: 60,
        editable: true,
        sorttype: "date",
        hidden: false,
        formatter:function (v,o,r) {
          if(v==1){
            return '区域'
          }else  if(v==2){
            return '目的地'
          }else{
            return '未知'
          }
        }
      },
      {
        display: "排序",
        name: 'NUM_SORT',
        index: 'NUM_SORT',
        width: 150,
        sortable: false,
        editable: false,
        hidden:true
      },
      {
        display: "是否热门",
        name: 'NUM_HOT',
        index: 'NUM_HOT',
        width: 150,
        sortable: false,
        editable: false,
        formatter:function (v,o,r) {
          if(v==1){
            return '是'
          }else{
            return '否'
          }
        }
      },
      {
        display: "创建时间",
        name: 'CREATE_TIME',
        index: 'CREATE_TIME',
        width: 150,
        sortable: false,
        editable: false,
        formatter:pickDate
      },{
        display: "资源URL",
        name: 'IMAGE_URL',
        index: 'IMAGE_URL',
        width: 80,
        sortable: false,
        editable: false,
        formatter:imageUrl
      }

    ];
    jQuery(grid_destination).jqGrid(optinons);
    $(window).triggerHandler('resize.jqGrid');//trigger window resize to make the grid get the correct size
    function imageUrl(v, o, r) {
      var html = "";
      html = '<a target="_blank" href="'+ r.IMAGE_URL+ '">'
      html = html +  ' <img  width="100%" style="max-height: 100%" src="' + r.IMAGE_URL + '" >';
      html = html + '</a>';
      return html;
    }
    function operation(v, o, r) {
      var html = ""
      html += '<span onclick="goEdit(' + r.DESTINATION_ID + ')" class="ace-icon fa fa-pencil blue bigger-150 cursor-p tt-cursor pld10 prd10"></span>'
      html += '<span onclick="del(' + r.DESTINATION_ID + ')" class="ace-icon fa fa-trash-o red bigger-150 cursor-p tt-cursor"></span>'
      return html
    }

    $(document).one('ajaxloadstart.page', function (e) {
      $(grid_destination).jqGrid('GridUnload');
      $('.ui-jqdialog').remove();
    });
  });
});
renderDestinationType("destinationType");

function goEdit(id) {
  window.location.href = "#page/destination/edit?id=" + id;
}

function del(id) {
  bootbox.confirm("确定删除吗", function (result) {
    if (result) {
      var url = $utils.ajax.setUrl("destination/deleteDestinationById");
      var params = {}
      params.destinationId = id;
      axios.post(url, params)
        .then(function (response) {
          if(response.code=="10000" || response.code==10000){
            resetDestination()
            $(grid_destination).trigger("reloadGrid");
          }else{
            bootbox.alert(response.msg)
          }
        })
        .catch(function (error) {
        });
    }
  });
}


function search() {
  var url = $utils.ajax.setUrl("destination/queryList");
  var params = {}
  params.destinationName = $("#destinationName").val()
  params.areaId = $("#levelType").val()
  params.destinationType = $("#destinationType").val()
  params.hot =  $("#hotType").val()
  $(grid_destination).jqGrid('setGridParam', {
    url: url,
    contentType: 'application/json;charset=utf-8',
    page: 1,
    postData: params
    // 发送数据
  }).trigger("reloadGrid");
}
