var scripts = [null,'../assets/js/bootstrap-multiselect.js', "../assets/js/jquery.validate.js", "../assets/js/jquery.validate_msgzh.js",null]
$('.page-content-area').ace_ajax('loadScripts', scripts, function () {
  jQuery(function ($) {
    var map = new BMap.Map("allmap",{minZoom:1,maxZoom:12});
    map.enableScrollWheelZoom();   //启用滚轮放大缩小，默认禁用
    map.enableContinuousZoom();    //启用地图惯性拖拽，默认禁用
    var $isNearBox = $("#isNear-box");
    var $typeBox = $("#type-box");
    var $isNear = $("#isNear");
    var $destinationType = $("#destinationType-box");





    renderDestinationType("destinationType")
    getData()
    function getData() {
      var url=$utils.ajax.setUrl("destination/getDestinationById");
      var params={}
      params.destinationId=$utils.string.getQueryParam("id")
      axios.post(url, params)
        .then(function (response) {
          renderDestination2(response.body)
        })
        .catch(function (error) {
        });
    }

    $("#webuploader-pick").click(function () {
      resourceModel(1, 1, 3,function (data) {
        console.log(data);
        $(".mainImg").attr("src", data[0].src);
        $(".mainImg").attr("data-id", data[0].id);


        getImageWidth(data[0].src,function(w,h){
          $("#Imgsize").html(w+'*'+h)
        });
      })
    })

    function renderDestination2(body) {
      $("#destinationLevel").val(body.AREA_ID);  //区域类型
      $("#destinationDetail").val(body.DESTINATION_DETAIL);
      $("#destinationName").val(body.DESTINATION_NAME);
      $("#type").val(body.TYPE);//国内国外
      $("#sort").val(body.NUM_SORT);
      $("#hot").val(body.NUM_HOT);
      $("#mapLongitude").val(body.MAP_LONGITUDE);
      $("#mapLatitude").val(body.MAP_LATITUDE);
      $("#isNear").val(body.IS_NEAR);


      if(body.NUM_HOT==0){
        $('#positionType').prop("disabled",true)
      }else{
        $('#positionType').prop("disabled",false)
      }


      if(body.AREA_ID == 1){
        $('#isNear').prop("disabled",true)
        $isNearBox.show()
        $typeBox.show()
        if(body.Type==2){
          $destinationType.show()
        }else{
          $destinationType.hide()
        }
      }else{
        $isNearBox.hide()
        $typeBox.hide()
        $destinationType.hide()
      }

      $("#positionType").val(body.POSITION_TYPE);
      $(".mainImg").attr("src", body.IMAGE_URL);
      $(".mainImg").attr("data-id", body.RESOURCE_ID);

      getImageWidth(body.IMAGE_URL,function(w,h){
        $("#Imgsize").html(w+'*'+h)
      });


      if(body.MAP_LONGITUDE==undefined||body.MAP_LATITUDE==undefined){
        map.centerAndZoom( new BMap.Point(116.404, 39.915), 15);
      }else{
        map.centerAndZoom( new BMap.Point(body.MAP_LONGITUDE, body.MAP_LATITUDE), 15);
        showMarker(body.MAP_LONGITUDE,body.MAP_LATITUDE)
      }

    }

    $("#update").click(function () {
      updateDestination()
    })

    $('#hot').change(function (e) {
      v = $(this).val()
      if (v == 1) {
        $('#positionType').prop("disabled",false)
      }else{
        $('#positionType').prop("disabled",true)
      }
    })

    $("#type").change(function (e) {
      var type = $(this).val();
      if (type == "1") {
        $isNearBox.show()
        $destinationtype.hide()
      } else if (type == "2") {
        $isNearBox.hide()
        $destinationtype.show()
        $isNear.val(0)
      }
    });

    function updateDestination() {
      var url=$utils.ajax.setUrl("destination/updateDestinationById");
      var params={}

      params.destinationId=$utils.string.getQueryParam("id")
      params.destinationName=$("#destinationName").val()
      params.destinationDetail=$("#destinationDetail").val()
      params.sort =$("#sort").val()
      params.resourceId = $(".mainImg").attr("data-id")
      //params.positionType = $("#positionType").val() //图片类型
      params.hot=$("#hot").val()
      params.mapLongitude =$("#mapLongitude").val()
      params.mapLatitude=$("#mapLatitude").val()

      if($("#destinationLevel").val()==1){
       // params.isNear=$("#isNear").val()
       // params.destinationType=$("#destinationType").val()
       // params.type=$("#type").val()
      }

      if($utils.string.isNull(params.resourceId)){
        bootbox.alert("请选择图片")
        return false
      }
    /*  if(params.hot==1&&params.positionType==0){
        bootbox.alert("热门目的地的图片长宽比不能是其他")
        return false
      }*/

      axios.post(url, params)
        .then(function (response) {
          resetDestination(function () {
            window.location.href = "#page/destination/list"
          })
        })
    }



    function showMarker(mapLongitude,mapLatitude) {
      var marker = new BMap.Marker(new BMap.Point(mapLongitude, mapLatitude));
      map.addOverlay(marker);
    }

    addMarker()
    function addMarker() {
      // 百度地图API功能
      var geoc = new BMap.Geocoder();
      map.addEventListener("click", function(e){
        //alert(e.point.lng + ", " + e.point.lat);
        $("#mapLongitude").val(e.point.lng)
        $("#mapLatitude").val(e.point.lat)

        var pt = e.point;
        geoc.getLocation(pt, function(rs){
          var addComp = rs.addressComponents;
          //alert(addComp.province + ", " + addComp.city + ", " + addComp.district);
        });
        var marker = new BMap.Marker(new BMap.Point(e.point.lng, e.point.lat)); // 创建点
        remove_overlay(marker)
        add_overlay(marker)
      });
    }
    function add_overlay(marker){
      map.addOverlay(marker);            //增加点
    }
    //清除覆盖物
    function remove_overlay(marker){
      map.clearOverlays(marker);
    }
  });
});
