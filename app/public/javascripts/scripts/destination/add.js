var scripts = [null, '../assets/js/bootstrap-multiselect.js', "../assets/js/jquery.validate.js", "../assets/js/jquery.validate_msgzh.js", null]
$('.page-content-area').ace_ajax('loadScripts', scripts, function () {
  var $areaIdBox = $("#areaId-box");
  var $area2IdBox = $("#area2Id-box");
  var $areaId = $("#areaId");
  var $area2Id = $("#area2Id");
  var $isNearBox = $("#isNear-box");
  var $isNear = $("#isNear");
  var $typeBox = $("#type-box");
  var $destinationtypeBox = $("#destinationType-box");




  renderDestinationType("destinationType")

  $("#add").click(function () {
    addDestination()
  })
  $("#webuploader-pick").click(function () {
    resourceModel(1, 1, 3, function (data) {
      $(".mainImg").attr("src", data[0].src);
      $(".mainImg").attr("data-id", data[0].id);
      getImageWidth(data[0].src, function (w, h) {
        $("#Imgsize").html(w + '*' + h)
      });
    })
  })

  $('#hot').change(function (e) {
    v = $(this).val()
    if (v == 1) {
      $('#positionType').prop("disabled", false)
    } else {
      $('#positionType').prop("disabled", true)
    }
  })

  function addDestination() {
    var params = {}

    var levelId = $("#destinationLevel .btn.active").attr("data");
    params.levelId = levelId
    if (levelId == 1) {
      params.pId = 0
      params.type = $("#type").val()
    } else if (levelId == 2) {
      params.pId = $areaId.val()
    }
    params.destinationName = $("#destinationName").val()
    params.destinationDetail = $("#destinationDetail").val()

    if(params.type==1){
       params.isNear = $("#isNear").val();
       if($("#isNear").val()==0){
        params.destinationType=0
       }else{
        params.destinationType=-1
       }
    }else if(params.type==2){
      params.destinationType = $("#destinationType").val();
      params.isNear=0
    }

    params.hot = $("#hot").val()
    params.sort = $("#sort").val()
    params.resourceId = $(".mainImg").attr("data-id");
    params.positionType = $("#positionType").val() //图片类型
    params.mapLongitude = $("#mapLongitude").val()
    params.mapLatitude = $("#mapLatitude").val()


    if ($utils.string.isNull(params.resourceId)) {
      bootbox.alert("请选择图片")
      return false
    }
   /* if (params.hot == 1 && params.positionType == 0) {
      bootbox.alert("热门目的地的图片长宽比不能是其他")
      return false
    }*/

    var url = $utils.ajax.setUrl("destination/addDestination");
    axios.post(url, params)
      .then(function (response) {
        bootbox.alert(response.msg)
        resetDestination(function () {
          window.location.href = "#page/destination/list"
        })
      })
  }

  $("#type").change(function (e) {
    var type = $(this).val();
    if (type == "1") {
      $isNearBox.show()
      $destinationtypeBox.hide()
    } else if (type == "2") {
      $isNearBox.hide()
      $destinationtypeBox.show()
      $isNear.val(0)
    }
  });


  $("#destinationLevel span.btn").click(function (e) {
    var type = $(this).attr("data");
    $(this).addClass("active").siblings().removeClass("active");
    if (type == "1") {
      $areaIdBox.hide()
      $area2IdBox.hide()
      $typeBox.show()
      $isNearBox.show()
      if($("#type").val()==1){
        $destinationtypeBox.hide();
      }else{
        $destinationtypeBox.show();
      }
    } else if (type == "2") {
      $areaIdBox.show()
      $area2IdBox.hide()
      $typeBox.hide()
      $isNearBox.hide()
      $destinationtypeBox.hide()
    }
  });

  $areaId.multiselect({
    enableFiltering: true,
    buttonClass: 'btn btn-white btn-primary',
    maxHeight: 200,
    numberDisplayed: 1,
    templates: {
      button: '<button type="button" class="multiselect dropdown-toggle" data-toggle="dropdown"></button>',
      ul: '<ul class="multiselect-container dropdown-menu"></ul>',
      filter: '<li class="multiselect-item filter"><div class="input-group"><span class="input-group-addon"><i class="fa fa-search"></i></span><input class="form-control multiselect-search" type="text"></div></li>',
      filterClearBtn: '<span class="input-group-btn"><button class="btn btn-default btn-white btn-grey multiselect-clear-filter" type="button"><i class="fa fa-times-circle red2"></i></button></span>',
      li: '<li><a href="javascript:void(0);"><label></label></a></li>',
      divider: '<li class="multiselect-item divider"></li>',
      liGroup: '<li class="multiselect-item group"><label class="multiselect-group"></label></li>'
    },
    onChange: function (option, checked) {//change事件改变
      console.log($(option).val());
      getChildrenNode($(option).val())
    },
  });

  $area2Id.multiselect({
    enableFiltering: true,
    buttonClass: 'btn btn-white btn-primary',
    maxHeight: 200,
    numberDisplayed: 1,
    templates: {
      button: '<button type="button" class="multiselect dropdown-toggle" data-toggle="dropdown"></button>',
      ul: '<ul class="multiselect-container dropdown-menu"></ul>',
      filter: '<li class="multiselect-item filter"><div class="input-group"><span class="input-group-addon"><i class="fa fa-search"></i></span><input class="form-control multiselect-search" type="text"></div></li>',
      filterClearBtn: '<span class="input-group-btn"><button class="btn btn-default btn-white btn-grey multiselect-clear-filter" type="button"><i class="fa fa-times-circle red2"></i></button></span>',
      li: '<li><a href="javascript:void(0);"><label></label></a></li>',
      divider: '<li class="multiselect-item divider"></li>',
      liGroup: '<li class="multiselect-item group"><label class="multiselect-group"></label></li>'
    }
  });

  getChildrenNode(0)

  function getChildrenNode(id) {
    var params = {}
    params.pId = id;
    var url = $utils.ajax.setUrl("destination/getDestinationByPid");
    axios.post(url, params)
      .then(function (response) {
        renderChildrenNode(id, response.body)
      })
  }

  function renderChildrenNode(id, list) {
    var html = ``;
    if (list.length > 0) {
      $.each(list, function (key, val) {
        html += `<option value="${val.DESTINATION_ID}">${val.DESTINATION_NAME}</option>`
      })
    }
    if (id == 0) {
      $areaId.html(html)
      $areaId.multiselect("rebuild")
      getChildrenNode($areaId.val())
    } else {
      $area2Id.html(html)
      $area2Id.multiselect("rebuild")
    }
  }

  addMarker("allmap")

  function addMarker(id) {
    // 百度地图API功能
    var map = new BMap.Map(id, {minZoom: 1, maxZoom: 12});
    var point = new BMap.Point(116.404, 39.915);
    map.centerAndZoom(point, 15);
    map.enableScrollWheelZoom();   //启用滚轮放大缩小，默认禁用
    map.enableContinuousZoom();    //启用地图惯性拖拽，默认禁用

    var geoc = new BMap.Geocoder();
    map.addEventListener("click", function (e) {
      //alert(e.point.lng + ", " + e.point.lat);
      $("#mapLongitude").val(e.point.lng)
      $("#mapLatitude").val(e.point.lat)

      var pt = e.point;
      geoc.getLocation(pt, function (rs) {
        var addComp = rs.addressComponents;
        //alert(addComp.province + ", " + addComp.city + ", " + addComp.district);
      });
      var marker = new BMap.Marker(new BMap.Point(e.point.lng, e.point.lat)); // 创建点
      remove_overlay(marker)
      add_overlay(marker)
    });

    //添加覆盖物
    function add_overlay(marker) {
      map.addOverlay(marker);            //增加点
    }

    //清除覆盖物
    function remove_overlay(marker) {
      map.clearOverlays(marker);
    }
  }
});
