var scripts = [null, '../assets/js/bootstrap-multiselect.js', null];
var grid_strategy = "#grid-table";
var pager_strategy = "#grid-pager";
var url = $utils.ajax.setUrl("sysNotes/queryList")
$('.page-content-area').ace_ajax('loadScripts', scripts, function () {
  var $area2Id = $("#destinationId");
  $area2Id.multiselect({
    enableFiltering: true,
    buttonClass: 'btn btn-white btn-primary',
    maxHeight: 200,
    numberDisplayed: 1,
    templates: {
      button: '<button type="button" class="multiselect dropdown-toggle" data-toggle="dropdown"></button>',
      ul: '<ul class="multiselect-container dropdown-menu"></ul>',
      filter: '<li class="multiselect-item filter"><div class="input-group"><span class="input-group-addon"><i class="fa fa-search"></i></span><input class="form-control multiselect-search" type="text"></div></li>',
      filterClearBtn: '<span class="input-group-btn"><button class="btn btn-default btn-white btn-grey multiselect-clear-filter" type="button"><i class="fa fa-times-circle red2"></i></button></span>',
      li: '<li><a href="javascript:void(0);"><label></label></a></li>',
      divider: '<li class="multiselect-item divider"></li>',
      liGroup: '<li class="multiselect-item group"><label class="multiselect-group"></label></li>'
    }
  });

  renderDestination("destinationId");
  $area2Id.multiselect('rebuild');

  jQuery(function ($) {
    $(window).on('resize.jqGrid', function () {
      $(grid_strategy).jqGrid('setGridWidth', $(".page-content").width());
    })
    var parent_column = $(grid_strategy).closest('[class*="col-"]');
    $(document).on('settings.ace.jqGrid', function (ev, event_name, collapsed) {
      if (event_name === 'sidebar_collapsed' || event_name === 'main_container_fixed') {
        //setTimeout is for webkit only to give time for DOM changes and then redraw!!!
        setTimeout(function () {
          $(grid_strategy).jqGrid('setGridWidth', parent_column.width());
        }, 0);
      }
    })
    var optinons = jqSetting(grid_strategy, pager_strategy, url);
    optinons.url = url;
    optinons.caption = "游记列表"
    optinons.colNames = ['操作 ', 'ID', '名称','目的地','类型','途径','行程','天数','人数','人均费用','玩法','更新时间'];
    optinons.colModel = [
      {
        name: 'operation', index: '', width: 90, fixed: true, sortable: false, resize: false,
        formatter: operation,
      },{
        display: "NOTES_ID",
        name: 'NOTES_ID',
        index: 'NOTES_ID',
        width: 60,
        sorttype: "int",
        hidden: true,
        editable: true
      },{
        display: "游记名称",
        name: 'NOTES_NAME',
        index: 'NOTES_NAME',
        width: 100,
        hidden: false
      },{
        display: "目的地",
        name: 'DESTINATION_NAME',
        index: 'DESTINATION_NAME',
        width: 50,
        hidden: false
      },{
        display: "类型",
        name: 'NOTE_TYPE',
        index: 'NOTE_TYPE',
        width: 50,
        hidden: true
      },{
        display: "途径",
        name: 'NOTES_TRAVEL',
        index: 'NOTES_TRAVEL',
        width: 100,
        hidden: false
      },{
        display: "行程",
        name: 'NOTES_ROUTE',
        index: 'NOTES_ROUTE',
        width: 100,
        hidden: false
      },{
        display: "天数",
        name: 'NOTES_NUM_DAYS',
        index: 'NOTES_NUM_DAYS',
        width: 30,
        hidden: false
      },{
        display: "人数",
        name: 'NOTES_NUM_PERSON',
        index: 'NOTES_NUM_PERSON',
        width: 30,
        hidden: false
      },{
        display: "人均费用",
        name: 'NOTES_PER_MONEY',
        index: 'NOTES_PER_MONEY',
        width: 30,
        hidden: false
      },{
        display: "玩法",
        name: 'NOTES_PLAY_STYLE',
        index: 'NOTES_PLAY_STYLE',
        width: 30,
        hidden: false
      },{
        display: "更新时间",
        name: 'UPDATE_TIME',
        index: 'UPDATE_TIME',
        width: 60,
        sortable: false,
        editable: false,
        formatter: pickDate
      }
    ];
    jQuery(grid_strategy).jqGrid(optinons);
    $(window).triggerHandler('resize.jqGrid');//trigger window resize to make the grid get the correct size

    function operation(v, o, r) {
      var html = ""
      html += '<span onclick="goEdit(' + r.NOTES_ID + ')" class="ace-icon fa fa-pencil blue bigger-150 cursor-p tt-cursor pld10 prd10"></span>'
      html += '<span onclick="del(' + r.NOTES_ID + ')" class="ace-icon fa fa-trash-o red bigger-150 cursor-p tt-cursor"></span>'
      return html
    }

    $(document).one('ajaxloadstart.page', function (e) {
      $(grid_strategy).jqGrid('GridUnload');
      $('.ui-jqdialog').remove();
    });
  });
});

function goEdit(id) {
  window.location.href = "#page/strategy/notes/edit?id=" + id;
}

function del(id) {
  bootbox.confirm("确定删除吗", function (result) {
    if (result) {
      var url = $utils.ajax.setUrl("sysNotes/deleteNotesById");
      var params = {}
      params.notesId = id;
      axios.post(url, params).then(function (response) {
        $(grid_strategy).trigger("reloadGrid");
      })
    }
  });
}

function search() {
  var params = {}
  params.notesName = $("#notesName").val();
  params.destinationId = $("#destinationId").val();
  params.noteType = $("#notesType").val();
  $(grid_strategy).jqGrid('setGridParam', {
    url: url,
    contentType: 'application/json;charset=utf-8',
    page: 1,
    postData: params
    // 发送数据
  }).trigger("reloadGrid"); // 重新载入
}


