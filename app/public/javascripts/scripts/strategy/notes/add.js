const scripts = [
  null,
  '../assets/js/bootstrap-multiselect.js',
  "../assets/js/fuelux/fuelux.wizard.js",
  '../assets/js/bootstrap-tagsinput.js',
  "../assets/js/jquery.validate.js",
  "../assets/js/date-time/bootstrap-datepicker.js",
  "../assets/js/date-time/bootstrap-timepicker.js",
  '../assets/ueditor/ueditor.config.js',
  '../assets/ueditor/ueditor.all.min.js',
  null
]

$('.page-content-area').ace_ajax('loadScripts', scripts, function () {
  jQuery(function ($) {
    /**
     * 一些jquery DOM
     */
    const $add = $('#add')
    const $destinationId = $("#destinationId");
    const $strategyId = $('#strategyId');
    const $mainImg = $(".mainImg")
    const $webuploaderPick = $("#webuploader-pick")
    const $webuploaderPickFocus = $("#webuploader-pick-focus")
    const $noteName = $('#noteName')
    const $numOfDays = $('#numOfDays')
    const $travel = $('#travel')
    const $route = $('#route')
    const $startDate = $('#startDate')
    const $notesNumPerson = $('#notesNumPerson')
    const $notesPerMoney = $('#notesPerMoney')
    const $notesPlayStyle = $('#notesPlayStyle')
    // 实例化编辑器
    const ue = window.UE.getEditor('editorContainer')

    const hasValue = (val) => !!val
    const getString = (val) => '' + val
    const doAlert = (val, cb) => bootbox.alert(val, cb)

    $notesPlayStyle.tagsinput('items')

    $('.date-picker').datepicker({
      language: 'ch',
      autoclose: true,
      todayHighlight: true
    }).next().on(ace.click_event, function () {
      $(this).prev().focus();
    })

    /**
     * focusIDs 攻略焦点图
     * */
    const focusIDs = [];
    const templatesForMultiselect = {
      button: '<button type="button" class="multiselect dropdown-toggle" data-toggle="dropdown"></button>',
      ul: '<ul class="multiselect-container dropdown-menu"></ul>',
      filter: '<li class="multiselect-item filter"><div class="input-group"><span class="input-group-addon"><i class="fa fa-search"></i></span><input class="form-control multiselect-search" type="text"></div></li>',
      filterClearBtn: '<span class="input-group-btn"><button class="btn btn-default btn-white btn-grey multiselect-clear-filter" type="button"><i class="fa fa-times-circle red2"></i></button></span>',
      li: '<li><a href="javascript:void(0);"><label></label></a></li>',
      divider: '<li class="multiselect-item divider"></li>',
      liGroup: '<li class="multiselect-item group"><label class="multiselect-group"></label></li>'
    }

    $destinationId.multiselect({
      enableFiltering: true,
      buttonClass: 'btn btn-white btn-primary',
      maxHeight: 200,
      numberDisplayed: 1,
      templates: templatesForMultiselect,
      onChange: rebuildStrategySelect
    });

    $strategyId.multiselect({
      enableFiltering: true,
      buttonClass: 'btn btn-white btn-primary',
      maxHeight: 200,
      numberDisplayed: 1,
      templates: templatesForMultiselect
    });

    renderDestination("destinationId")
    $destinationId.multiselect('rebuild')
    rebuildStrategySelect()

    renderStrategy("strategyId")
    $strategyId.multiselect('rebuild')

    function rebuildStrategySelect() {
      const destinationId = $destinationId.val()
      if (!destinationId) return
      const url = $utils.ajax.setUrl("sysNotes/queryStrategyList")
      const requestData = {
        destinationId: destinationId
      }
      axios.post(url, requestData).then(data => {
        if (data.code === '10000') {
          const list = data.body || []
          renderStrategy('strategyId', list)
          $strategyId.multiselect('rebuild')
        } else {
          bootbox.alert('查询攻略列表失败')
        }
      })
    }

    $webuploaderPick.click(function () {
      resourceModel(1, 9, 9, function (data) {
        $mainImg.attr("src", data[0].src);
        $mainImg.attr("data-id", data[0].id);
      })
    })

    function delStrategyImg(obj) {
      $(obj).parents(".item").remove()
      removeArrItem(focusIDs, $(obj).prev("img").attr("data-id"))
    }

    window.delStrategyImg = delStrategyImg;

    function removeItem(obj) {
      $(obj).parents(".form-group").remove()
    }

    window.removeItem = removeItem;

    $add.click(insertNotes)

    /**
     * 添加游记
     */
    function insertNotes() {
      if (!validateFields()) return
      const url = $utils.ajax.setUrl("sysNotes/insertNotes")
      const requestData = {
        destinationId: $destinationId.val(),
        notesName: $noteName.val(),
        notesTypes: (() => {
          const arr = []
          $("#noteType input:checked").each(function (index, ele) {
            arr.push($(this).val())
          })
          return arr.join(',')
        })(),
        notesStartDate: $startDate.val(),
        notesNumDays: $numOfDays.val(),
        notesContents: ue.getContent(),
        notesTravel: $travel.val(),
        notesRoute: $route.val(),
        resourceId: $mainImg.attr('data-id'),
        focusedResourcesId: focusIDs.join(','),
        strategyId: $strategyId.val(),
        notesNumPerson: $notesNumPerson.val(),
        notesPerMoney: $notesPerMoney.val(),
        notesPlayStyle: $notesPlayStyle.val()
      }
      axios.post(url, requestData).then(res => {
        if (res.code === '10000') {
          bootbox.alert('添加游记成功！', () => {
            window.location.href = '/views/index.html#page/strategy/notes/list'
          })
        } else {
          bootbox.alert('添加攻略信息失败')
        }
      }).catch(() => {
        bootbox.alert('添加攻略信息出错了')
      })
    }

    /**攻略图片*/
    $webuploaderPickFocus.click(function () {
      resourceModel(2, 9, 9, function (data) {
        $.each(data, function (key, val) {
          if ($.inArray(val.id, focusIDs) === -1) {
            focusIDs.push(val.id);
            const html = '<li class="item">' +
              ' <div class="thumb">' +
              '<img  data-id="' + val.id + '"' + 'src="' + val.src + '">' +
              '<span onclick="delStrategyImg(this,1)" class="btn btn-sm btn-white"> <icon class="ace-icon glyphicon glyphicon-remove red"></icon>删除</span>' +
              '</div>' +
              '</li>'
            $(".imgslist-focus").append(html)
          }
        })
      })
    })

    function validateFields() {
      const destinationId = getString($destinationId.val())
      const noteName = getString($noteName.val())
      const noteTypes = (() => {
        const arr = []
        $("#noteType input:checked").each(function (index, ele) {
          arr.push($(this).val())
        })
        return arr.join(',')
      })()
      const notesStartDate = getString($startDate.val())
      const notesNumDays = getString($numOfDays.val())
      const content = getString(ue.getContent())
      const travel = getString($travel.val())
      const route = getString($route.val())
      const resourceId = getString($mainImg.attr('data-id'))
      const focuseResourceIdNum = focusIDs.length;
      const focusedResourcesId = focusIDs.join(',')
      const strategyId = $strategyId.val()
      const notesNumPerson = $notesNumPerson.val()
      const notesPerMoney = $notesPerMoney.val()
      const notesPlayStyle = $notesPlayStyle.val()
      const isInteger = val => /^[0-9]+$/.test(val)
      if (!hasValue(destinationId)) {
        doAlert('请选择目的地！')
        return false
      }
      if (!hasValue(noteName)) {
        doAlert('请输入游记名称！')
        return false
      }
      if (!hasValue(noteTypes)) {
        doAlert('请选择类型！')
        return false
      }
      if (!hasValue(notesStartDate)) {
        doAlert(('请选择出发日期！'))
        return false
      }
      if (!hasValue(notesNumDays)) {
        doAlert('请选择天数！')
        return false
      }
      if (notesNumDays <= 0) {
        doAlert('天数应大于0！')
        return false
      }
      if (!hasValue(content)) {
        doAlert('请输入正文！')
        return false
      }
      if (!hasValue(travel)) {
        doAlert('请输入途径！')
        return false
      }
      if (!hasValue(route)) {
        doAlert('请输入行程！')
        return false
      }
      if (!hasValue(resourceId)) {
        doAlert('请选择主图！')
        return false
      }
      if(focuseResourceIdNum!=4){
        doAlert('焦点图请选择四张！')
        return false
      }
      if (!hasValue(focusedResourcesId)) {
        doAlert('请选择焦点图！')
        return false
      }
      if (!hasValue(strategyId)) {
        doAlert('请选择攻略！')
        return false
      }
      if (!hasValue(notesNumPerson)) {
        doAlert('请输入人数！')
        return false
      }
      if (!isInteger(notesNumPerson)) {
        doAlert('人数应为整数！')
        return false
      }
      if (!hasValue(notesPerMoney)) {
        doAlert('请输入人均费用！')
        return false
      }
      if (!isInteger(notesPerMoney)) {
        doAlert('人均费用应为整数！')
        return false
      }
      if (!hasValue(notesPlayStyle)) {
        doAlert('请输入玩法！')
        return false
      }
      return true
    }
  })
});
