var scripts = [null, '../assets/js/bootstrap-multiselect.js', null];
var grid_food = "#grid-table";
var pager_food = "#grid-pager";
var url = $utils.ajax.setUrl("sysStrategyFood/queryList")
$('.page-content-area').ace_ajax('loadScripts', scripts, function () {
  var $area2Id = $("#destinationId");
  $area2Id.multiselect({
    enableFiltering: true,
    buttonClass: 'btn btn-white btn-primary',
    maxHeight: 200,
    numberDisplayed: 1,
    templates: {
      button: '<button type="button" class="multiselect dropdown-toggle" data-toggle="dropdown"></button>',
      ul: '<ul class="multiselect-container dropdown-menu"></ul>',
      filter: '<li class="multiselect-item filter"><div class="input-group"><span class="input-group-addon"><i class="fa fa-search"></i></span><input class="form-control multiselect-search" type="text"></div></li>',
      filterClearBtn: '<span class="input-group-btn"><button class="btn btn-default btn-white btn-grey multiselect-clear-filter" type="button"><i class="fa fa-times-circle red2"></i></button></span>',
      li: '<li><a href="javascript:void(0);"><label></label></a></li>',
      divider: '<li class="multiselect-item divider"></li>',
      liGroup: '<li class="multiselect-item group"><label class="multiselect-group"></label></li>'
    }
  });

  renderDestination("destinationId")
  $area2Id.multiselect('rebuild')

  jQuery(function ($) {
    $(window).on('resize.jqGrid', function () {
      $(grid_food).jqGrid('setGridWidth', $(".page-content").width());
    })
    var parent_column = $(grid_food).closest('[class*="col-"]');
    $(document).on('settings.ace.jqGrid', function (ev, event_name, collapsed) {
      if (event_name === 'sidebar_collapsed' || event_name === 'main_container_fixed') {
        //setTimeout is for webkit only to give time for DOM changes and then redraw!!!
        setTimeout(function () {
          $(grid_food).jqGrid('setGridWidth', parent_column.width());
        }, 0);
      }
    })
    var optinons = jqSetting(grid_food, pager_food, url);
    optinons.url = url;
    optinons.caption = "美食列表"
    optinons.colNames = ['操作 ', 'ID', '美食名称','美食描述', '目的地','更新时间'];
    optinons.colModel = [
      {
        name: 'operation', index: '', width: 120, fixed: true, sortable: false, resize: false,
        formatter: operation,
      },
      {
        display: "FOOD_ID",
        name: 'FOOD_ID',
        index: 'FOOD_ID',
        width: 60,
        sorttype: "int",
        hidden: true,
        editable: true
      },
      {
        display: "美食名称",
        name: 'FOOD_NAME',
        index: 'FOOD_NAME',
        width: 150,
        editable: true,
        sorttype: "date",
        hidden: false
      },
      {
        display: "美食描述",
        name: 'FOOD_DETAIL',
        index: 'FOOD_DETAIL',
        width: 250,
        editable: true,
        sorttype: "date",
        hidden: false
      },
      {
        display: "目的地",
        name: 'DESTINATION_NAME',
        index: 'DESTINATION_NAME',
        width: 50,
        editable: true,
        sorttype: "date",
        hidden: false
      },
      {
        display: "更新时间",
        name: 'UPDATE_TIME',
        index: 'UPDATE_TIME',
        width: 60,
        sortable: false,
        editable: false,
        formatter: pickDate
      }
    ];
    jQuery(grid_food).jqGrid(optinons);
    $(window).triggerHandler('resize.jqGrid');//trigger window resize to make the grid get the correct size

    function operation(v, o, r) {
      var html = ""
      html += '<span onclick="goEdit(' + r.FOOD_ID + ')" class="ace-icon fa fa-pencil blue bigger-150 cursor-p tt-cursor pld10 prd10"></span>'
      html += '<span onclick="del(' + r.FOOD_ID + ')" class="ace-icon fa fa-trash-o red bigger-150 cursor-p tt-cursor"></span>'
      return html
    }

    $(document).one('ajaxloadstart.page', function (e) {
      $(grid_food).jqGrid('GridUnload');
      $('.ui-jqdialog').remove();
    });
  });
});

function goEdit(id) {
  window.location.href = "#page/strategy/food/edit?id=" + id;
}

function del(id) {
  bootbox.confirm("删除此美食项后，与攻略关联的美食也被删除，确认删除吗", function (result) {
    if (result) {
      var url = $utils.ajax.setUrl("sysStrategyFood/deleteFoodById");
      var params = {}
      params.foodId = id;
      axios.post(url, params).then(function (response) {
          $(grid_food).trigger("reloadGrid");
        })
    }
  });
}

function search() {
  var params = {}
  params.foodName = $("#foodName").val();
  params.destinationId=$("#destinationId").val();
  $(grid_food).jqGrid('setGridParam', {
    url: url,
    contentType: 'application/json;charset=utf-8',
    page: 1,
    postData: params
    // 发送数据
  }).trigger("reloadGrid"); // 重新载入
}


