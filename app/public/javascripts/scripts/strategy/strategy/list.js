var scripts = [null, '../assets/js/bootstrap-multiselect.js', null];
var grid_strategy = "#grid-table";
var pager_strategy = "#grid-pager";
var url = $utils.ajax.setUrl("sysStrategy/queryList")
$('.page-content-area').ace_ajax('loadScripts', scripts, function () {
  var $area2Id = $("#destinationId");
  $area2Id.multiselect({
    enableFiltering: true,
    buttonClass: 'btn btn-white btn-primary',
    maxHeight: 200,
    numberDisplayed: 1,
    templates: {
      button: '<button type="button" class="multiselect dropdown-toggle" data-toggle="dropdown"></button>',
      ul: '<ul class="multiselect-container dropdown-menu"></ul>',
      filter: '<li class="multiselect-item filter"><div class="input-group"><span class="input-group-addon"><i class="fa fa-search"></i></span><input class="form-control multiselect-search" type="text"></div></li>',
      filterClearBtn: '<span class="input-group-btn"><button class="btn btn-default btn-white btn-grey multiselect-clear-filter" type="button"><i class="fa fa-times-circle red2"></i></button></span>',
      li: '<li><a href="javascript:void(0);"><label></label></a></li>',
      divider: '<li class="multiselect-item divider"></li>',
      liGroup: '<li class="multiselect-item group"><label class="multiselect-group"></label></li>'
    }
  });

  renderDestination("destinationId");
  $area2Id.multiselect('rebuild');

  jQuery(function ($) {
    $(window).on('resize.jqGrid', function () {
      $(grid_strategy).jqGrid('setGridWidth', $(".page-content").width());
    })
    var parent_column = $(grid_strategy).closest('[class*="col-"]');
    $(document).on('settings.ace.jqGrid', function (ev, event_name, collapsed) {
      if (event_name === 'sidebar_collapsed' || event_name === 'main_container_fixed') {
        //setTimeout is for webkit only to give time for DOM changes and then redraw!!!
        setTimeout(function () {
          $(grid_strategy).jqGrid('setGridWidth', parent_column.width());
        }, 0);
      }
    })
    var optinons = jqSetting(grid_strategy, pager_strategy, url);
    optinons.url = url;
    optinons.caption = "攻略列表"
    optinons.colNames = ['操作 ', 'ID', '攻略名称','类型', '目的地','目的地描述','收藏数','评论数','分享数','更新时间','状态'];
    optinons.colModel = [
      {
        name: 'operation', index: '', width: 120, fixed: true, sortable: false, resize: false,
        formatter: operation,
      },{
        display: "STRATEGY_ID",
        name: 'STRATEGY_ID',
        index: 'STRATEGY_ID',
        width: 60,
        sorttype: "int",
        hidden: true,
        editable: true
      },{
        display: "攻略名称",
        name: 'STRATEGY_NAME',
        index: 'STRATEGY_NAME',
        width: 150,
        editable: true,
        sorttype: "date",
        hidden: false
      },{
        display: "类型",
        name: 'STRATEGY_TYPE',
        index: 'STRATEGY_TYPE',
        width: 60,
        editable: true,
        sorttype: "date",
        hidden: false
      },{
        display: "目的地",
        name: 'DESTINATION_NAME',
        index: 'DESTINATION_NAME',
        width: 50,
        editable: true,
        sorttype: "date",
        hidden: false
      },{
        display: "目的地描述",
        name: 'DESTINATION_DETAIL',
        index: 'DESTINATION_DETAIL',
        width: 250,
        editable: true,
        sorttype: "date",
        hidden: false
      },{
        display: "收藏数",
        name: 'NUM_COLLECT',
        index: 'NUM_COLLECT',
        width: 50,
        editable: true,
        sorttype: "date",
        hidden: false
      },{
        display: "评论数",
        name: 'NUM_COMMENT',
        index: 'NUM_COMMENT',
        width: 50,
        editable: true,
        sorttype: "date",
        hidden: false
      },{
        display: "分享数",
        name: 'NUM_SHARE',
        index: 'NUM_SHARE',
        width: 50,
        editable: true,
        sorttype: "date",
        hidden: false
      },{
        display: "更新时间",
        name: 'UPDATE_TIME',
        index: 'UPDATE_TIME',
        width: 60,
        sortable: false,
        editable: false,
        formatter: pickDate
      },
      {
        display: "状态",
        name: 'STRATEGY_STATUS',
        index: 'STRATEGY_STATUS',
        width: 90,
        editable: true,
        sorttype: "date",
        hidden: false,
        formatter: function (v, o, r) {
          if (v == 1) {
            return '<span class="prd10">已发布</span>' + '<span class="btn btn-sm btn-white" onclick="changePublishStrategy(' + r.STRATEGY_ID + ',' + '0' + ')">暂存</span>'
          } else {
            return '<span class="prd10">暂存</span>' + '<span class="btn btn-sm btn-white" onclick="changePublishStrategy(' + r.STRATEGY_ID + ',' + '1' + ')">发布</span>'
          }
        }
      }
    ];
    jQuery(grid_strategy).jqGrid(optinons);
    $(window).triggerHandler('resize.jqGrid');//trigger window resize to make the grid get the correct size

    function operation(v, o, r) {
      var html = ""
      html += '<span onclick="goEdit(' + r.STRATEGY_ID + ')" class="ace-icon fa fa-pencil blue bigger-150 cursor-p tt-cursor pld10 prd10"></span>'
      html += '<span onclick="del(' + r.STRATEGY_ID + ')" class="ace-icon fa fa-trash-o red bigger-150 cursor-p tt-cursor"></span>'
      html += preview(r.STRATEGY_ID)

      return html
    }

    /*
* 攻略预览
* */
    function preview(id) {
      var PREVPATH = 'http://www.aiwanlx.com/strategy/hot-destination/'
      if(window.location.hostname=='localhost'||window.location.hostname=='47.95.215.100'){
        PREVPATH='http://47.95.215.100/strategy/hot-destination/'
      }
      var Suffix = '.html?preview=1'
      var path = PREVPATH + id + Suffix;
      return '<a class="pld10 prd10" target="_blank" href="' + path + '"><icon class="ace-icon fa fa-eye bigger-150"></icon></a>'
    }
    $(document).one('ajaxloadstart.page', function (e) {
      $(grid_strategy).jqGrid('GridUnload');
      $('.ui-jqdialog').remove();
    });
  });
});

function goEdit(id) {
  window.location.href = "#page/strategy/strategy/edit?id=" + id;
}

function changePublishStrategy(id,type) {
  var msg='';
  if(type==0){
    msg="确认要暂存吗"
  }else{
    msg="确认要发布吗"
  }
  var url=$utils.ajax.setUrl("sysStrategy/publishStrategy");
  var params={}
  params.strategyId=id
  params.strategyStatus=type
  bootbox.confirm(msg,function (v) {
    if(v){
      axios.post(url, params)
        .then(function (response) {
          $(grid_strategy).trigger("reloadGrid");
        })
    }
  })
}

function del(id) {
  bootbox.confirm("确定删除吗", function (result) {
    if (result) {
      var url = $utils.ajax.setUrl("sysStrategy/deleteStrategyById");
      var params = {}
      params.strategyId = id;
      axios.post(url, params).then(function (response) {
          $(grid_strategy).trigger("reloadGrid");
        })
    }
  });
}

function search() {
  var params = {}
  params.strategyName = $("#strategyName").val();
  params.destinationId=$("#destinationId").val();
  if($("#strategyType").val()!=''){
    params.strategyType=$("#strategyType").val();
  }else{
    params.strategyType=null;
  }
  $(grid_strategy).jqGrid('setGridParam', {
    url: url,
    contentType: 'application/json;charset=utf-8',
    page: 1,
    postData: params
    // 发送数据
  }).trigger("reloadGrid"); // 重新载入
}


