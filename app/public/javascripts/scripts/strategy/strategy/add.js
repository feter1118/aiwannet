const scripts = [
  null,
  '../assets/js/bootstrap-multiselect.js',
  "../assets/js/fuelux/fuelux.wizard.js",
  '../assets/js/bootstrap-tagsinput.js',
  "../assets/js/jquery.validate.js",
  '../assets/ueditor/ueditor.config.js',
  '../assets/ueditor/ueditor.all.min.js',
  null
]

$('.page-content-area').ace_ajax('loadScripts', scripts, function () {
  const hasValue = (val) => !!val
  const doAlert = (val, cb) => bootbox.alert(val, cb)
  jQuery(function ($) {
    /**
     * focusIDs 攻略焦点图
     * */
    var focusIDs = [];
    var strategyId;
    var isHasStrategyId = false;
    var $destinationId = $("#destinationId");
    // 实例化编辑器
    const ue = window.UE.getEditor('editorContainer')

    $destinationId.multiselect({
      enableFiltering: true,
      buttonClass: 'btn btn-white btn-primary',
      maxHeight: 200,
      numberDisplayed: 1,
      templates: {
        button: '<button type="button" class="multiselect dropdown-toggle" data-toggle="dropdown"></button>',
        ul: '<ul class="multiselect-container dropdown-menu"></ul>',
        filter: '<li class="multiselect-item filter"><div class="input-group"><span class="input-group-addon"><i class="fa fa-search"></i></span><input class="form-control multiselect-search" type="text"></div></li>',
        filterClearBtn: '<span class="input-group-btn"><button class="btn btn-default btn-white btn-grey multiselect-clear-filter" type="button"><i class="fa fa-times-circle red2"></i></button></span>',
        li: '<li><a href="javascript:void(0);"><label></label></a></li>',
        divider: '<li class="multiselect-item divider"></li>',
        liGroup: '<li class="multiselect-item group"><label class="multiselect-group"></label></li>'
      }
    });

    renderDestination("destinationId")
    $destinationId.multiselect('rebuild')

    $('#fuelux-wizard-container')
      .ace_wizard({
        step: 1,//optional argument. wizard will jump to step "2" at first
      })
      .on('actionclicked.fu.wizard', function (e, info) {
        if (info.step == 1 && info.direction == 'next') {
          if ($(".mainImg").attr("data-id") == "") {
            alert("请添加主图")
            return false
          }
          if ($(".focusedImg").attr("data-id") == "") {
            alert("请添加焦点图")
            return false
          }
          if($(".hotImg").attr("data-id") == "" && $("#hotCheckBox").is(':checked') ){
            alert("请添加热门图")
            return false
          }
          if($(".recommendImg").attr("data-id") == "" && $("#recommendCheckBox").is(':checked') ){
            alert("请添加推荐图")
            return false
          }
          if (!isHasStrategyId) {
            addStrategyBaseInfo()
          }
        }
        if (info.step == 2 && info.direction == 'next') {
          if (!isHasStrategyId) {
            alert("攻略基本信息暂未保存")
            return false
          } else {
            insertOrUpdateStrategyTrafficInfo();
            queryRefList('1')
            queryRefList('2')
            queryRefList('3')
          }
        }
      })
      .on('finished.fu.wizard', function (e) {
        if (!isHasStrategyId) {
          alert("攻略基本信息暂未保存")
          return false
        } else {
          insertOrUpdateStrategyRefInfo()
        }
      });

    $("#webuploader-pick-main").click(function () {
      resourceModel(1, 10, 10, function (data) {//
        $(".mainImg").attr("src", data[0].src);
        $(".mainImg").attr("data-id", data[0].id);
      })
    })

    $("#webuploader-pick-focused").click(function () {
      resourceModel(1, 10, 10, function (data) {//
        $(".focusedImg").attr("src", data[0].src);
        $(".focusedImg").attr("data-id", data[0].id);
      })
    })
    $("#webuploader-pick-hot").click(function () {
      resourceModel(1, 10, 10, function (data) {//
        $(".hotImg").attr("src", data[0].src);
        $(".hotImg").attr("data-id", data[0].id);
      })
    })
    $("#webuploader-pick-recommend").click(function () {
      resourceModel(1, 10, 10, function (data) {//
        $(".recommendImg").attr("src", data[0].src);
        $(".recommendImg").attr("data-id", data[0].id);
      })
    })

    function insertOrUpdateStrategyRefInfo() {
      const getIds = (wrapperSelector) => {
        const $wrapper = $(wrapperSelector)
        const arr = []
        $wrapper.find('.card.active').each(function (idx, elem) {
          arr.push($(elem).data('id') || '')
        })
        return arr.join(',')
      }
      const url = $utils.ajax.setUrl("sysStrategy/insertOrUpdateStrategyRefInfo");
      const params = {}
      params.strategyId = strategyId//攻略Id
      params.foodsIds = getIds('#cardsFood')
      params.scenicIds = getIds('#cardsScene')
      params.shoppingIds = getIds('#cardsPurchase')
      axios.post(url, params).then(function (response) {
        if (response.code === "10000") {
          bootbox.alert("添加攻略关联细项成功", () => {
            // 添加成功后返回列表页
            window.location.href = '/views/index.html#page/strategy/strategy/list'
          })
        } else {
          bootbox.alert("添加攻略关联细项失败")
        }
      })
    }

    function insertOrUpdateStrategyTrafficInfo() {
      var url = $utils.ajax.setUrl("sysStrategy/insertOrUpdateStrategyTrafficInfo");
      var params = {}
      params.strategyId = strategyId//攻略Id
      params.strategyTraffic = $("#strategyTraffic").val()//交通
      params.strategyVisa = $("#strategyVisa").val()//签证
      axios.post(url, params).then(function (response) {
        if (response.code == "10000") {
          bootbox.alert("添加交通/签证成功")
        } else {
          bootbox.alert("添加交通/签证失败")
        }
      })
    }

    /**新增攻略基本信息*/
    function addStrategyBaseInfo() {
      var url = $utils.ajax.setUrl("sysStrategy/insertOrUpdateStrategyBaseInfo");
      const strategyName = $("#strategyName").val()
      const strategyTitle = $("#strategyTitle").val()
      const strategySubTitle = $("#strategySubTitle").val()
      const strategyDetail = ue.getContent()
      const destinationDetail = $('#destinationDetail').val()
      const resourceId = $(".mainImg").attr("data-id");
      const focusedId =  $(".focusedImg").attr("data-id");
      const hotResourcesId = $(".hotImg").attr("data-id");
      const recommendResourcesId = $(".recommendImg").attr("data-id");
     // const focusedResourcesId = focusIDs.join(",");
      if (!hasValue(strategyName)) {
        doAlert('请输入攻略名称！')
        return false
      }
      if(!hasValue(strategyTitle)){
        doAlert('请输入攻略标题！')
        return false
      }
      if(!hasValue(strategySubTitle)){
        doAlert('请输入攻略副标题！')
        return false
      }
      if(!hasValue(strategyDetail)){
        doAlert('请输入攻略描述！')
        return false
      }
      if(!hasValue(destinationDetail)){
        doAlert('请输入目的地描述！')
        return false
      }

      var params = {}
      params.strategyName = strategyName //攻略名称
      params.strategyTitle = strategyTitle //攻略主题
      params.strategySubTitle = strategySubTitle //攻略副主题
      params.strategyDetail = strategyDetail //攻略描述
      params.destinationId = $("#destinationId").val()//目的地
      params.destinationDetail = destinationDetail //目的地描述
      params.resourceId = resourceId;
      params.focusedId = focusedId;
      params.hotResourcesId = hotResourcesId;
      params.recommendResourcesId = recommendResourcesId;
      var strategyTypeArr = []
      $("#strategyType input:checked").each(function (index, ele) {
        strategyTypeArr.push($(this).val())
      })
      if (strategyTypeArr.length > 0) {
        params.strategyTypes = strategyTypeArr.join(',')
      }

      axios.post(url, params).then(function (response) {
        if (response.code == "10000") {
          bootbox.alert("添加攻略成功")
          isHasStrategyId = true;
          console.info(JSON.stringify(response));
          strategyId = response.body;
        } else {
          bootbox.alert("添加攻略失败")
        }
      })
    }

    /**攻略图片*/
   /* $("#webuploader-pick-focus").click(function () {
      resourceModel(2, 10, 10, function (data) {
        $.each(data, function (key, val) {
          if ($.inArray(val.id, focusIDs) == -1) {
            focusIDs.push(val.id);
            var html = '<li class="item">' +
              ' <div class="thumb">' +
              '<img  data-id="' + val.id + '"' + 'src="' + val.src + '">' +
              '<span onclick="delStrategyImg(this,1)" class="btn btn-sm btn-white"> <icon class="ace-icon glyphicon glyphicon-remove red"></icon>删除</span>' +
              '</div>' +
              '</li>'
            $(".imgslist-focus").append(html)
          }
        })
      })
    })

    function delStrategyImg(obj) {
      $(obj).parents(".item").remove()
      removeArrItem(focusIDs, $(obj).prev("img").attr("data-id"))
    }

    window.delStrategyImg = delStrategyImg;

    function removeItem(obj) {
      $(obj).parents(".form-group").remove()
    }*/

   // window.removeItem = removeItem;

    /**
     * 查询美食、景点、购物信息
     * @param itemType {string} 1=美食 2=景点 3=购物
     */
    function queryRefList (itemType) {
      const url = $utils.ajax.setUrl("sysStrategy/queryRefList")
      const requestData = {
        destinationId: $('#destinationId').val(),
        itemType
      }
      axios.post(url, requestData).then(res => {
        if (res.code === '10000') {
          const prefix = (() => {
            switch ('' + itemType) {
              case '1': return 'FOOD'
              case '2': return 'SCENIC'
              case '3': return 'SHOPPING'
              default: return ''
            }
          })()
          const list = (res.body || []).map(item => {
            return {
              id: item[prefix + '_ID'],
              imgUrl: item.IMAGE_URL,
              name: item[prefix + '_NAME']
            }
          })
          renderRefList(itemType, list)
        } else {
          const itemTypeText = (() => {
            switch (itemType) {
              case '1': return '美食'
              case '2': return '景点'
              case '3': return '购物'
              default: return ''
            }
          })()
          bootbox.alert(`查询${itemTypeText}信息失败`)
        }
      })
    }

    /**
     * 渲染美食、景点、购物信息到页面上
     * @param itemType {string} 1=美食 2=景点 3=购物
     * @param data
     */
    function renderRefList (itemType, data) {
      itemType = '' + itemType
      const targetId = (() => {
        switch (itemType) {
          case '1': return 'cardsFood'
          case '2': return 'cardsScene'
          case '3': return 'cardsPurchase'
          default: return ''
        }
      })()
      const $wrapper = $('#' + targetId)
      const itemHtml = `
        <li class="card" data-id="{{id}}">
          <div class="card-img" style="background-image: url('{{imgUrl}}');"></div>
          <div class="card-name">{{name}}</div>
        </li>
      `
      let html = ''
      data.forEach(item => {
        html += itemHtml
          .replace('{{id}}', item.id)
          .replace('{{imgUrl}}', item.imgUrl)
          .replace('{{name}}', `${item.name}`)
      })
      $wrapper.html(html)
    }

    /**
     * 点击美食、景点、购物卡片，切换勾选状态
     */
    $('.cards').on('click', '.card', function (e) {
      const $this = $(this)
      $this.toggleClass('active');
    })
  })
});
