var scripts = [null,'../assets/js/bootstrap-multiselect.js', "../assets/js/fuelux/fuelux.wizard.js", '../assets/js/bootstrap-tagsinput.js',
  "../assets/js/jquery.validate.js",
  null
]

/**
 * focusIDs 美食焦点图
 * */
var focusIDs = [];
var $destinationId = $("#destinationId");
$('.page-content-area').ace_ajax('loadScripts', scripts, function () {
  const hasValue = (val) => !!val
  const doAlert = (val, cb) => bootbox.alert(val, cb)

  jQuery(function ($) {
    $destinationId.multiselect({
      enableFiltering: true,
      buttonClass: 'btn btn-white btn-primary',
      maxHeight: 200,
      numberDisplayed: 1,
      templates: {
        button: '<button type="button" class="multiselect dropdown-toggle" data-toggle="dropdown"></button>',
        ul: '<ul class="multiselect-container dropdown-menu"></ul>',
        filter: '<li class="multiselect-item filter"><div class="input-group"><span class="input-group-addon"><i class="fa fa-search"></i></span><input class="form-control multiselect-search" type="text"></div></li>',
        filterClearBtn: '<span class="input-group-btn"><button class="btn btn-default btn-white btn-grey multiselect-clear-filter" type="button"><i class="fa fa-times-circle red2"></i></button></span>',
        li: '<li><a href="javascript:void(0);"><label></label></a></li>',
        divider: '<li class="multiselect-item divider"></li>',
        liGroup: '<li class="multiselect-item group"><label class="multiselect-group"></label></li>'
      }
    });

    renderDestination("destinationId")
    $destinationId.multiselect('rebuild')

    $("#add").click(function () {
      addScenic()
    })
  })

  $("#webuploader-pick").click(function () {
    resourceModel(1, 7, 7, function (data) {//
      console.log(data)
      $(".mainImg").attr("src", data[0].src);
      $(".mainImg").attr("data-id", data[0].id);
    })
  })


  /**新增景点基本信息*/
  function addScenic() {
    var url = $utils.ajax.setUrl("sysStrategyScenic/insertScenic");
    const scenicName = $("#scenicName").val()//景点名称
    const scenicDetail = $("#scenicDetail").val()//景点描述
    const destinationId = $("#destinationId").val()//景点目的地
    const resourceId = $(".mainImg").attr("data-id");//主图
    const focusedResourcesId =focusIDs.join(",");//焦点图
    if (!hasValue(scenicName)) {
      doAlert('请输入景点名称！')
      return false
    }
    if (!hasValue(destinationId)) {
      doAlert('请选择景点目的地！')
      return false
    }
    if (!hasValue(resourceId)) {
      doAlert('请选择景点主图！')
      return false
    }
   /* if (!hasValue(focusedResourcesId)) {
      doAlert('请选择景点焦点图！')
      return false
    }*/
    var params = {}
    params.scenicName = scenicName;
    params.scenicDetail = scenicDetail;
    params.destinationId = destinationId;
    params.resourceId = resourceId;
    //params.focusedResourcesId = focusedResourcesId;
    axios.post(url, params).then(function (response) {
        if(response.code=="10000"){
          bootbox.alert("添加景点成功")
        }else{
          bootbox.alert("添加景点失败")
        }
      })
  }
  /**景点图片*/
  $("#webuploader-pick-focus").click(function () {
    resourceModel(2, 7, 7, function (data) {
      $.each(data, function (key, val) {
        if ($.inArray(val.id, focusIDs) == -1) {
          focusIDs.push(val.id);
          var html = '<li class="item">' +
            ' <div class="thumb">' +
            '<img  data-id="' + val.id + '"' + 'src="' + val.src + '">' +
            '<span onclick="delScenicImg(this,1)" class="btn btn-sm btn-white"> <icon class="ace-icon glyphicon glyphicon-remove red"></icon>删除</span>' +
            '</div>' +
            '</li>'
          $(".imgslist-focus").append(html)
        }
      })
    })
  })
});

function delScenicImg(obj) {
  $(obj).parents(".item").remove()
  removeArrItem(focusIDs, $(obj).prev("img").attr("data-id"))
}
function removeItem(obj) {
  $(obj).parents(".form-group").remove()
}


