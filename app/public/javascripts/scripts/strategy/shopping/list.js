var scripts = [null, '../assets/js/bootstrap-multiselect.js', null];
var grid_shopping = "#grid-table";
var pager_shopping = "#grid-pager";
var url = $utils.ajax.setUrl("sysStrategyShopping/queryList")
$('.page-content-area').ace_ajax('loadScripts', scripts, function () {
  var $area2Id = $("#destinationId");
  $area2Id.multiselect({
    enableFiltering: true,
    buttonClass: 'btn btn-white btn-primary',
    maxHeight: 200,
    numberDisplayed: 1,
    templates: {
      button: '<button type="button" class="multiselect dropdown-toggle" data-toggle="dropdown"></button>',
      ul: '<ul class="multiselect-container dropdown-menu"></ul>',
      filter: '<li class="multiselect-item filter"><div class="input-group"><span class="input-group-addon"><i class="fa fa-search"></i></span><input class="form-control multiselect-search" type="text"></div></li>',
      filterClearBtn: '<span class="input-group-btn"><button class="btn btn-default btn-white btn-grey multiselect-clear-filter" type="button"><i class="fa fa-times-circle red2"></i></button></span>',
      li: '<li><a href="javascript:void(0);"><label></label></a></li>',
      divider: '<li class="multiselect-item divider"></li>',
      liGroup: '<li class="multiselect-item group"><label class="multiselect-group"></label></li>'
    }
  });

  renderDestination("destinationId")
  $area2Id.multiselect('rebuild')

  jQuery(function ($) {
    $(window).on('resize.jqGrid', function () {
      $(grid_shopping).jqGrid('setGridWidth', $(".page-content").width());
    })
    var parent_column = $(grid_shopping).closest('[class*="col-"]');
    $(document).on('settings.ace.jqGrid', function (ev, event_name, collapsed) {
      if (event_name === 'sidebar_collapsed' || event_name === 'main_container_fixed') {
        //setTimeout is for webkit only to give time for DOM changes and then redraw!!!
        setTimeout(function () {
          $(grid_shopping).jqGrid('setGridWidth', parent_column.width());
        }, 0);
      }
    })
    var optinons = jqSetting(grid_shopping, pager_shopping, url);
    optinons.url = url;
    optinons.caption = "购物列表"
    optinons.colNames = ['操作 ', 'ID', '购物名称','购物描述', '目的地','更新时间'];
    optinons.colModel = [
      {
        name: 'operation', index: '', width: 120, fixed: true, sortable: false, resize: false,
        formatter: operation,
      },
      {
        display: "SHOPPING_ID",
        name: 'SHOPPING_ID',
        index: 'SHOPPING_ID',
        width: 60,
        sorttype: "int",
        hidden: true,
        editable: true
      },
      {
        display: "购物名称",
        name: 'SHOPPING_NAME',
        index: 'SHOPPING_NAME',
        width: 150,
        editable: true,
        sorttype: "date",
        hidden: false
      },
      {
        display: "购物描述",
        name: 'SHOPPING_DETAIL',
        index: 'SHOPPING_DETAIL',
        width: 250,
        editable: true,
        sorttype: "date",
        hidden: false
      },
      {
        display: "目的地",
        name: 'DESTINATION_NAME',
        index: 'DESTINATION_NAME',
        width: 50,
        editable: true,
        sorttype: "date",
        hidden: false
      },
      {
        display: "更新时间",
        name: 'UPDATE_TIME',
        index: 'UPDATE_TIME',
        width: 60,
        sortable: false,
        editable: false,
        formatter: pickDate
      }
    ];
    jQuery(grid_shopping).jqGrid(optinons);
    $(window).triggerHandler('resize.jqGrid');//trigger window resize to make the grid get the correct size

    function operation(v, o, r) {
      var html = ""
      html += '<span onclick="goEdit(' + r.SHOPPING_ID + ')" class="ace-icon fa fa-pencil blue bigger-150 cursor-p tt-cursor pld10 prd10"></span>'
      html += '<span onclick="del(' + r.SHOPPING_ID + ')" class="ace-icon fa fa-trash-o red bigger-150 cursor-p tt-cursor"></span>'
      return html
    }

    $(document).one('ajaxloadstart.page', function (e) {
      $(grid_shopping).jqGrid('GridUnload');
      $('.ui-jqdialog').remove();
    });
  });
});

function goEdit(id) {
  window.location.href = "#page/strategy/shopping/edit?id=" + id;
}

function del(id) {
  bootbox.confirm("删除此购物项后，与攻略关联的购物也被删除，确定删除吗", function (result) {
    if (result) {
      var url = $utils.ajax.setUrl("sysStrategyShopping/deleteShoppingById");
      var params = {}
      params.shoppingId = id;
      axios.post(url, params).then(function (response) {
          $(grid_shopping).trigger("reloadGrid");
        })
    }
  });
}

function search() {
  var params = {}
  params.shoppingName = $("#shoppingName").val();
  params.destinationId=$("#destinationId").val();
  $(grid_shopping).jqGrid('setGridParam', {
    url: url,
    contentType: 'application/json;charset=utf-8',
    page: 1,
    postData: params
    // 发送数据
  }).trigger("reloadGrid"); // 重新载入
}


