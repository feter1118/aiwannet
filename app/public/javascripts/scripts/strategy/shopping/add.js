var scripts = [null,'../assets/js/bootstrap-multiselect.js', "../assets/js/fuelux/fuelux.wizard.js", '../assets/js/bootstrap-tagsinput.js',
  "../assets/js/jquery.validate.js",
  null
]

/**
 * focusIDs 购物焦点图
 * */
var focusIDs = [];
var $destinationId = $("#destinationId");
$('.page-content-area').ace_ajax('loadScripts', scripts, function () {
  const hasValue = (val) => !!val
  const doAlert = (val, cb) => bootbox.alert(val, cb)
  jQuery(function ($) {
    $destinationId.multiselect({
      enableFiltering: true,
      buttonClass: 'btn btn-white btn-primary',
      maxHeight: 200,
      numberDisplayed: 1,
      templates: {
        button: '<button type="button" class="multiselect dropdown-toggle" data-toggle="dropdown"></button>',
        ul: '<ul class="multiselect-container dropdown-menu"></ul>',
        filter: '<li class="multiselect-item filter"><div class="input-group"><span class="input-group-addon"><i class="fa fa-search"></i></span><input class="form-control multiselect-search" type="text"></div></li>',
        filterClearBtn: '<span class="input-group-btn"><button class="btn btn-default btn-white btn-grey multiselect-clear-filter" type="button"><i class="fa fa-times-circle red2"></i></button></span>',
        li: '<li><a href="javascript:void(0);"><label></label></a></li>',
        divider: '<li class="multiselect-item divider"></li>',
        liGroup: '<li class="multiselect-item group"><label class="multiselect-group"></label></li>'
      }
    });

    renderDestination("destinationId")
    $destinationId.multiselect('rebuild')

    $("#add").click(function () {
      addShopping()
    })
  })

  $("#webuploader-pick").click(function () {
    resourceModel(1, 8, 8, function (data) {//
      console.log(data)
      $(".mainImg").attr("src", data[0].src);
      $(".mainImg").attr("data-id", data[0].id);
    })
  })


  /**新增购物基本信息*/
  function addShopping() {
    var url = $utils.ajax.setUrl("sysStrategyShopping/insertShopping");
    const shoppingName = $("#shoppingName").val();//购物名称
    const shoppingDetail = $("#shoppingDetail").val()//购物描述
    const destinationId = $("#destinationId").val()//购物目的地
    const resourceId = $(".mainImg").attr("data-id");
    const focusedResourcesId = focusIDs.join(",");
    if (!hasValue(shoppingName)) {
      doAlert('请输入购物名称！')
      return false
    }
    if (!hasValue(destinationId)) {
      doAlert('请选择购物目的地！')
      return false
    }
    if (!hasValue(resourceId)) {
      doAlert('请选择购物主图！')
      return false
    }
  /*  if (!hasValue(focusedResourcesId)) {
      doAlert('请选择购物焦点图！')
      return false
    }*/
    var params = {}
    params.shoppingName = shoppingName;
    params.shoppingDetail = shoppingDetail;
    params.destinationId = destinationId;
    params.resourceId = resourceId;
   // params.focusedResourcesId = focusedResourcesId

    axios.post(url, params).then(function (response) {
        if(response.code=="10000"){
          bootbox.alert("添加购物成功")
        }else{
          bootbox.alert("添加购物失败")
        }
      })
  }
  /**购物图片*/
  $("#webuploader-pick-focus").click(function () {
    resourceModel(2, 8, 8, function (data) {
      $.each(data, function (key, val) {
        if ($.inArray(val.id, focusIDs) == -1) {
          focusIDs.push(val.id);
          var html = '<li class="item">' +
            ' <div class="thumb">' +
            '<img  data-id="' + val.id + '"' + 'src="' + val.src + '">' +
            '<span onclick="delShoppingImg(this,1)" class="btn btn-sm btn-white"> <icon class="ace-icon glyphicon glyphicon-remove red"></icon>删除</span>' +
            '</div>' +
            '</li>'
          $(".imgslist-focus").append(html)
        }
      })
    })
  })
});

function delShoppingImg(obj) {
  $(obj).parents(".item").remove()
  removeArrItem(focusIDs, $(obj).prev("img").attr("data-id"))
}
function removeItem(obj) {
  $(obj).parents(".form-group").remove()
}


