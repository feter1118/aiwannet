var scripts = [null, "", null];

$('.page-content-area').ace_ajax('loadScripts', scripts, function () {
  jQuery(function ($) {


    $('.input-file').ace_file_input({
      no_file: '请选择图片文件',
      btn_choose: '选择',
      btn_change: '更改',
      style: 'well',
      no_icon: 'ace-icon fa fa-cloud-upload',
      droppable: true,
      thumbnail: 'small',//large | fit
      'allowExt': ["jpeg", "jpg", "png", "gif", "bmp"],
      'allowMime': ["image/jpg", "image/jpeg", "image/png", "image/gif", "image/bmp"]
    });


    var urlResourceCategory = $utils.ajax.setUrl("dicItem/getDicItemListByDicCode");
    var params = {}
    params.dicCode = 'D00000011'
    axios.post(urlResourceCategory, params)
      .then(function (response) {
        console.info(response)
        var array = response.body;
        for (var i = 0; i < array.length; i++) {
          $("#resourceCategory").append("<option value='" + array[i].ITEM_CODE + "'>" + array[i].ITEM_NAME + "</option>");
        }
      })
      .catch(function (error) {
      });

    var params1 = {}
    params1.dicCode = 'D00000010'
    axios.post(urlResourceCategory, params1)
      .then(function (response) {
        console.info(response)
        var array = response.body;
        for (var i = 0; i < array.length; i++) {
          $("#resourceType").append("<option value='" + array[i].ITEM_CODE + "'>" + array[i].ITEM_NAME + "</option>");
        }
      })
      .catch(function (error) {
      });


    $('#addResource').click(function (e) {

      if ($utils.string.isNull($("#resourceLabel").val())) {
        bootbox.alert("请添加标签")
        return false
      }

      var formData = new FormData();
      var files = $("#imgs-input-file")[0].files;
      var maxSize = 1024 * 1024;//kb转字节
      var checkResult = {}
      if (files.length == 0) {
        bootbox.alert("请添加图片")
      }
      bootbox.confirm("确认添加吗?", function (v) {
        if (v) {
          checkResult.success = true
          checkResult.fileName = ''

          for (var i = 0; i < files.length; i++) {
            var file = files[i];
            if (file.size > maxSize) {
              checkResult.success = false
              checkResult.fileName += file.name
            }else{
              formData.append('file', file);
            }
            if (!checkResult.success) {
              bootbox.alert(file.name + '大于1024KB,不能上传', function () {
                bootbox.hideAll()
              })
              return false
            }
          }
          submitAjax()
        }
      })

      function submitAjax() {
        formData.append("resourceCategory", $("#resourceCategory").val());
        formData.append("resourceCode", $("#resourceCategory").val());
        formData.append("resourceType", $("#resourceType").val());
        formData.append("resourceLabel", $("#resourceLabel").val());
        formData.append("resourceSource", $("#resourceSource").val());
        var urlUploadFile = $utils.ajax.setUrl("upload/uploadFile");
        modelLoading(1)
        $.ajax({
          url: urlUploadFile,
          type: "post",
          async: true,
          cache: false,
          processData: false,
          contentType: false,
          data: formData,
          success: function (data) {
            bootbox.alert(data.msg)
            modelLoading(0)
          },
          error: function () {
            //window.location.href="#page/resource/list";
            alert("添加图片失败");
          }
        });
      }
    })

  });
});





