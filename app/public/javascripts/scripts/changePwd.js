var scripts = [null, "", null]
$('.page-content-area').ace_ajax('loadScripts', scripts, function () {
  $("#update").click(function () {
    addSysUser()
  })
  function addSysUser() {
    if($("#NewuserPwd").val()!=$("#reUserPwd").val()){
      bootbox.alert("重置密码不一致")
      return
    }
    var params = {}
    params.oldPassWord = $("#oldUserPwd").val()
    params.newPassWord = $("#NewuserPwd").val()
    var url = $utils.ajax.setUrl("user/resetPassWord");
    axios.post(url, params)
      .then(function (response) {
        bootbox.alert(response.msg)
      })
  }
});
