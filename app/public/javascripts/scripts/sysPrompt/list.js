var scripts = [null, "", null];
var grid_sysPrompt = "#grid-table";
var pager_sysPrompt = "#grid-pager";
var url = $utils.ajax.setUrl("sysPrompt/getPromptList")
$('.page-content-area').ace_ajax('loadScripts', scripts, function () {
  jQuery(function ($) {
    $(window).on('resize.jqGrid', function () {
      $(grid_sysPrompt).jqGrid('setGridWidth', $(".page-content").width());
    })
    var parent_column = $(grid_sysPrompt).closest('[class*="col-"]');
    $(document).on('settings.ace.jqGrid', function (ev, event_name, collapsed) {
      if (event_name === 'sidebar_collapsed' || event_name === 'main_container_fixed') {
        //setTimeout is for webkit only to give time for DOM changes and then redraw!!!
        setTimeout(function () {
          $(grid_sysPrompt).jqGrid('setGridWidth', parent_column.width());
        }, 0);
      }
    })
    var optinons = jqSetting(grid_sysPrompt, pager_sysPrompt, url);
    optinons.url = url;
    optinons.caption = "温馨提示列表"
    optinons.colNames = ['操作 ', 'ID', '内容'];
    optinons.colModel = [
      {
        name: 'operation', index: '', width: 80, fixed: true, sortable: false, resize: false,
        formatter: operation,
      },
      {
        display: "NOTICE_ID",
        name: 'NOTICE_ID',
        index: 'NOTICE_ID',
        width: 60,
        sorttype: "int",
        hidden: true,
        editable: true
      },
      {
        display: "内容",
        name: 'NOTICE_INFO',
        index: 'NOTICE_INFO',
        width: 90,
        editable: true,
        sorttype: "date",
        hidden: false
      }
    ];
    jQuery(grid_sysPrompt).jqGrid(optinons);
    $(window).triggerHandler('resize.jqGrid');//trigger window resize to make the grid get the correct size
    function operation(v, o, r) {
      var html = ""
      html += '<span onclick="del(' + r.NOTICE_ID + ')" class="ace-icon fa fa-trash-o red bigger-150 cursor-p tt-cursor"></span>'
      return html
    }

    $(document).one('ajaxloadstart.page', function (e) {
      $(grid_sysPrompt).jqGrid('GridUnload');
      $('.ui-jqdialog').remove();
    });
  });
});

function del(id) {
  bootbox.confirm("确定删除吗", function (result) {
    if (result) {
      var url = $utils.ajax.setUrl("sysPrompt/deletePromptById");
      var params = {}
      params.noticeId = id;
      axios.post(url, params)
        .then(function (response) {
          $(grid_sysPrompt).trigger("reloadGrid");
        })
    }
  });
}

function add() {
  modeloneInput('添加温馨提示', '内容', function (data) {
    var url = $utils.ajax.setUrl("sysPrompt/addPrompt");
    var params = {}
    params.noticeInfo = data.t1
    axios.post(url, params)
      .then(function (response) {
        $(grid_sysPrompt).trigger("reloadGrid");
      })
  })
}
