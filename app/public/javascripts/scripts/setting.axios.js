axios.defaults.timeout = 1000*30;
axios.interceptors.request.use(
  config => {
    const token=$.cookie("token");
    if (token) {
      config.headers['token'] = token;
      config.headers.Authorization = token;
    }
    config.headers['Content-Type'] = "application/json;charset=UTF-8";
    return config;
  },
  error => {
    return Promise.reject(error);
  });
axios.interceptors.response.use(
  response => {
    if(response.data.code == '10000'){
      return response.data;
    }else if(response.data.code == '00000'){
      alert("token 过期")
      window.location.href="./login.html"
    }else {
      alert(response.data.msg)
    }
  },
  error => {
    alert(error)
    return Promise.reject(error.response.data)
  });
