var scripts = [null, '../assets/js/bootstrap-multiselect.js', null];
var grid_product = "#grid-table";
var pager_product = "#grid-pager";
var url = $utils.ajax.setUrl("product/queryList")
$('.page-content-area').ace_ajax('loadScripts', scripts, function () {
  var $area2Id = $("#destinationId");
  $area2Id.multiselect({
    enableFiltering: true,
    buttonClass: 'btn btn-white btn-primary',
    maxHeight: 200,
    numberDisplayed: 1,
    templates: {
      button: '<button type="button" class="multiselect dropdown-toggle" data-toggle="dropdown"></button>',
      ul: '<ul class="multiselect-container dropdown-menu"></ul>',
      filter: '<li class="multiselect-item filter"><div class="input-group"><span class="input-group-addon"><i class="fa fa-search"></i></span><input class="form-control multiselect-search" type="text"></div></li>',
      filterClearBtn: '<span class="input-group-btn"><button class="btn btn-default btn-white btn-grey multiselect-clear-filter" type="button"><i class="fa fa-times-circle red2"></i></button></span>',
      li: '<li><a href="javascript:void(0);"><label></label></a></li>',
      divider: '<li class="multiselect-item divider"></li>',
      liGroup: '<li class="multiselect-item group"><label class="multiselect-group"></label></li>'
    }
  });

  renderDestination("destinationId")
  $area2Id.multiselect('rebuild')

  jQuery(function ($) {
    $(window).on('resize.jqGrid', function () {
      $(grid_product).jqGrid('setGridWidth', $(".page-content").width());
    })
    var parent_column = $(grid_product).closest('[class*="col-"]');
    $(document).on('settings.ace.jqGrid', function (ev, event_name, collapsed) {
      if (event_name === 'sidebar_collapsed' || event_name === 'main_container_fixed') {
        //setTimeout is for webkit only to give time for DOM changes and then redraw!!!
        setTimeout(function () {
          $(grid_product).jqGrid('setGridWidth', parent_column.width());
        }, 0);
      }
    })
    var optinons = jqSetting(grid_product, pager_product, url);
    optinons.url = url;
    optinons.caption = "线路列表"
    optinons.colNames = ['操作 ', 'ID', '产品名称','产品编号', '产品类型', '产品价格', '状态', '秒杀状态','更新时间'];
    optinons.colModel = [
      {
        name: 'operation', index: '', width: 120, fixed: true, sortable: false, resize: false,
        formatter: operation,
      },
      {
        display: "ROLE_ID",
        name: 'PRODUCT_ID',
        index: 'PRODUCT_ID',
        width: 60,
        sorttype: "int",
        hidden: true,
        editable: true
      },
      {
        display: "产品名称",
        name: 'PRODUCT_NAME',
        index: 'PRODUCT_NAME',
        width: 150,
        editable: true,
        sorttype: "date",
        hidden: false
      },
      {
        display: "产品编号",
        name: 'PRODUCT_CODE',
        index: 'PRODUCT_CODE',
        width: 50,
        editable: true,
        sorttype: "date",
        hidden: false
      },
      {
        display: "产品类型",
        name: 'PRODUCT_TYPE',
        index: 'PRODUCT_TYPE',
        width: 50,
        editable: true,
        sorttype: "date",
        hidden: false,
        formatter: function (v, o, r) {
          if (v == 1) {
            return '自由行'
          } else {
            return '跟团游'
          }
        }
      },
      {
        display: "产品价格",
        name: 'PRODUCT_PRICE',
        index: 'PRODUCT_PRICE',
        width: 50,
        editable: true,
        sorttype: "date",
        hidden: false
      },
      {
        display: "状态",
        name: 'PRODUCT_STATUS',
        index: 'PRODUCT_STATUS',
        width: 90,
        editable: true,
        sorttype: "date",
        hidden: false,
        formatter: function (v, o, r) {
          if (v == 1) {
            return '<span class="prd10">已发布</span>' + '<span class="btn btn-sm btn-white" onclick="changePublish(' + r.PRODUCT_ID + ',' + '0' + ')">暂存</span>'
          } else {
            return '<span class="prd10">暂存</span>' + '<span class="btn btn-sm btn-white" onclick="changePublish(' + r.PRODUCT_ID + ',' + '1' + ')">发布</span>'
          }
        }
      },
      {
        display: "是否秒杀",
        name: 'PRODUCT_SECKILL_FLAG',
        index: 'PRODUCT_SECKILL_FLAG',
        width: 90,
        editable: true,
        sorttype: "date",
        hidden: false,
        formatter: function (v, o, r) {
          if (v == 1) {
            return '<span class="prd10">已加入</span>' + '<span class="btn btn-sm btn-white" onclick="changeSecKillFlag(' + r.PRODUCT_ID+','+r.PRODUCT_STATUS+ ',' + '0' + ')">取消秒杀</span>'
          } else {
            return '<span class="prd10">未加入</span>' + '<span class="btn btn-sm btn-white" onclick="changeSecKillFlag(' + r.PRODUCT_ID+','+r.PRODUCT_STATUS + ',' + '1' + ')">加入秒杀</span>'
          }
        }
      },
      {
        display: "更新时间",
        name: 'UPDATE_TIME',
        index: 'UPDATE_TIME',
        width: 60,
        sortable: false,
        editable: false,
        formatter: pickDate
      }
    ];
    jQuery(grid_product).jqGrid(optinons);
    $(window).triggerHandler('resize.jqGrid');//trigger window resize to make the grid get the correct size

    function operation(v, o, r) {
      var html = ""
      html += '<span onclick="goEdit(' + r.PRODUCT_ID + ')" class="ace-icon fa fa-pencil blue bigger-150 cursor-p tt-cursor pld10 prd10"></span>'
      html += '<span onclick="del(' + r.PRODUCT_ID + ')" class="ace-icon fa fa-trash-o red bigger-150 cursor-p tt-cursor"></span>'
      html += preview(r.PRODUCT_ID)
      return html
    }

    $(document).one('ajaxloadstart.page', function (e) {
      $(grid_product).jqGrid('GridUnload');
      $('.ui-jqdialog').remove();
    });
  });
});

function goEdit(id) {
  window.location.href = "#page/product/edit?id=" + id;
}

function del(id) {
  bootbox.confirm("确定删除吗", function (result) {
    if (result) {
      var url = $utils.ajax.setUrl("product/deleteProductById");
      var params = {}
      params.productId = id;
      axios.post(url, params)
        .then(function (response) {
          $(grid_product).trigger("reloadGrid");
        })
    }
  });
}

function search() {
  var params = {}
  params.productName = $("#productName").val();
  params.destinationId=$("#destinationId").val();
  params.productCode = $("#productCode").val();
  params.secKillFlag = $("#secKillFlag").val();
  $(grid_product).jqGrid('setGridParam', {
    url: url,
    contentType: 'application/json;charset=utf-8',
    page: 1,
    postData: params
    // 发送数据
  }).trigger("reloadGrid"); // 重新载入
}



function changePublish(id,type) {
  var msg='';
  if(type==0){
    msg="确认要暂存吗"
  }else{
    msg="确认要发布吗"
  }
  var url=$utils.ajax.setUrl("product/publishProduct");
  var params={}
  params.productId=id
  params.productStatus=type
  bootbox.confirm(msg,function (v) {
    if(v){
      axios.post(url, params)
        .then(function (response) {
          $(grid_product).trigger("reloadGrid");
        })
    }
  })
}

function changeSecKillFlag(id,status,type) {
  if (status == 0 && type == 1) {
    bootbox.alert("未发布的产品不能参与秒杀")
    return false
  }

  var msg='';
  if(type==0){
    msg="确认要取消秒杀序列吗"
  }else{
    msg="确认要加入秒杀序列吗"
  }
  var url=$utils.ajax.setUrl("product/updateProductSecKillFlag");
  var params={}
  params.productId=id
  params.secKillFlag=type
  bootbox.confirm(msg,function (v) {
    if(v){
      axios.post(url, params)
        .then(function (response) {
          $(grid_product).trigger("reloadGrid");
        })
    }
  })
}
