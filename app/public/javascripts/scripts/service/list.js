var scripts = [null, "", null];
var grid_sysDomain = "#grid-table";
var pager_sysRole = "#grid-pager";
var url = $utils.ajax.setUrl("userService/queryList")
$('.page-content-area').ace_ajax('loadScripts', scripts, function() {
  jQuery(function($) {
    var urlResourceCategory=$utils.ajax.setUrl("dicItem/getDicItemListByDicCode");
    var params={}
    params.dicCode ='D00000014'
    axios.post(urlResourceCategory, params)
      .then(function (response) {
        console.info(response)
        var array = response.body;
        for(var i=0;i<array.length;i++){
          $("#serviceType").append("<option value='"+array[i].ITEM_CODE+"'>"+array[i].ITEM_NAME+"</option>");
        }
      })
      .catch(function (error) {
      });

    var params1={};
    params1.dicCode ='D00000015';
    axios.post(urlResourceCategory, params1)
      .then(function (response) {
        console.info(response)
        var array = response.body;
        for(var i=0;i<array.length;i++){
          $("#serviceSource").append("<option value='"+array[i].ITEM_CODE+"'>"+array[i].ITEM_NAME+"</option>");
        }
      })
      .catch(function (error) {
      });

    $(window).on('resize.jqGrid', function () {
      $(grid_sysDomain).jqGrid( 'setGridWidth', $(".page-content").width() );
    })
    var parent_column = $(grid_sysDomain).closest('[class*="col-"]');
    $(document).on('settings.ace.jqGrid' , function(ev, event_name, collapsed) {
      if( event_name === 'sidebar_collapsed' || event_name === 'main_container_fixed' ) {
        //setTimeout is for webkit only to give time for DOM changes and then redraw!!!
        setTimeout(function() {
          $(grid_sysDomain).jqGrid( 'setGridWidth', parent_column.width() );
        }, 0);
      }
    })
    var optinons=jqSetting(grid_sysDomain,pager_sysRole,url);
    optinons.url=url;
    optinons.caption="服务列表"
    optinons.colNames=['操作 ','ID', '用户昵称','手机号','服务类型','服务来源','总次数','可用次数', '服务描述','有效时间'];
    optinons.colModel=[
      {name:'operation',index:'', width:100, fixed:true, sortable:false, resize:false, formatter:operation },
      {display:"ID",name:'IN_ID',index:'IN_ID', width:60, sorttype:"int", hidden:true,editable: true},
      {display:"用户昵称",name:'USER_NAME',index:'USER_NAME',width:90, editable:true, sorttype:"date",hidden:false},
      {display:"手机号",name:'USER_PHONE',index:'USER_PHONE',width:90, editable:true, sorttype:"date",hidden:false},
      {display:"服务类型",name:'SERVICETYPE_NAME',index:'SERVICETYPE_NAME',width:120, editable:true, sorttype:"date",hidden:false},
      {display:"服务来源",name:'SERVICE_SOURCE_NAME',index:'SERVICE_SOURCE_NAME',width:120, editable:true, sorttype:"date",hidden:false},
      {display:"总次数",name:'SERVICE_TIMES',index:'SERVICE_TIMES',width:90, editable:true, sorttype:"date",hidden:false},
      {display:"可用次数",name:'SERVICE_AVAILABLE_TIMES',index:'SERVICE_AVAILABLE_TIMES',width:90, editable:true, sorttype:"date",hidden:false},
      {display:"服务描述",name:'SERVICE_DETAIL',index:'SERVICE_DETAIL',width:160, editable:true, sorttype:"date",hidden:false},
      {display:"有效时间",name:'SERVICE_EXPIRED_DATE',index:'SERVICE_EXPIRED_DATE', width:120, sortable:false,editable: false,formatter:pickDate}
    ];
    jQuery(grid_sysDomain).jqGrid(optinons);
    $(window).triggerHandler('resize.jqGrid');//trigger window resize to make the grid get the correct size

    function operation(v,o,r) {
      var html=""
      html+='<span title="修改" onclick="goEdit('+r.IN_ID+')" class="ace-icon fa fa-pencil blue bigger-150 cursor-p tt-cursor pld10 prd10"></span>'
      html+='<span title="删除" onclick="del('+r.IN_ID+')" class="ace-icon fa fa-trash-o red bigger-150 cursor-p tt-cursor prd10"></span>'
      html+='<span title="消费" onclick="customer('+r.IN_ID+')" class="ace-icon glyphicon glyphicon-plus green bigger-120 cursor-p tt-cursor"></span>'
      return html
    }
    $(document).one('ajaxloadstart.page', function(e) {
      $(grid_sysDomain).jqGrid('GridUnload');
      $('.ui-jqdialog').remove();
    });
  });
});

function search(){
  var params={}
  params.servicetype =$("#serviceType").val();
  params.userPhone=$("#userPhone").val();
  params.serviceSource=$("#serviceSource").val();
  $(grid_sysDomain).jqGrid('setGridParam', {
    url: url,
    contentType: 'application/json;charset=utf-8',
    page: 1,
    postData: params
    // 发送数据
  }).trigger("reloadGrid"); // 重新载入
}


function goEdit(id) {
  window.location.href="#page/service/edit?id="+id;
}
function del(id) {
    bootbox.confirm("确定删除吗",function (result) {
    if(result){
      var url=$utils.ajax.setUrl("userService/deleteServiceById");
      var params={}
      params.inId=id;
      axios.post(url, params)
        .then(function (response) {
         $(grid_sysDomain).trigger("reloadGrid");
        })
        .catch(function (error) {
        });
     }
     });
}

function customer(id) {
      modelTemTwoInput('消费服务','消费次数','消费描述',function (data) {
        var url=$utils.ajax.setUrl("userService/customerServiceById");
        var params={}
        params.inId=id;
        params.serviceDetail =data.t2;
        params.serviceTimes = data.t1;
        axios.post(url, params)
           .then(function (response) {
             $(grid_sysDomain).trigger("reloadGrid");
           })
           .catch(function (error) {
           });
      })
}
