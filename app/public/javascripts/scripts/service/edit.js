var scripts = [null, "../assets/js/jquery.raty.js", "../assets/js/date-time/bootstrap-datepicker.js", "../assets/js/date-time/bootstrap-timepicker.js", "../assets/js/jquery.validate.js", "../assets/js/jquery.validate_msgzh.js", null]
$('.page-content-area').ace_ajax('loadScripts', scripts, function () {
  jQuery(function ($) {
    $('#serviceExpiredDate').datepicker({
      language: 'ch',
      autoclose: true,
      todayHighlight: true
    }).next().on(ace.click_event, function () {
      $(this).prev().focus();
    });

    getData()
    function getData() {
      var urlResourceCategory=$utils.ajax.setUrl("dicItem/getDicItemListByDicCode");
      var params={};
      params.dicCode ='D00000014';
      axios.post(urlResourceCategory, params)
        .then(function (response) {
          console.info(response)
          var array = response.body;
          for(var i=0;i<array.length;i++){
            $("#serviceType").append("<option value='"+array[i].ITEM_CODE+"'>"+array[i].ITEM_NAME+"</option>");
          }
        })
        .catch(function (error) {
        });

      var params1={};
      params1.dicCode ='D00000015';
      axios.post(urlResourceCategory, params1)
        .then(function (response) {
          console.info(response)
          var array = response.body;
          for(var i=0;i<array.length;i++){
            $("#serviceSource").append("<option value='"+array[i].ITEM_CODE+"'>"+array[i].ITEM_NAME+"</option>");
          }
        })
        .catch(function (error) {
        });

      var url=$utils.ajax.setUrl("userService/getServiceById");
      var params={};
      params.inId=$utils.string.getQueryParam("id");
      axios.post(url, params)
        .then(function (response) {
          renderSysService(response.body);
        })
        .catch(function (error) {
        });
    }

    function renderSysService(body) {
      $("#userPhone").val(body.USER_PHONE);
      $("#serviceType").val(body.SERVICETYPE);
      $("#serviceUsedTimes").val(body.SERVICE_AVAILABLE_TIMES);
      $("#serviceTimes").val(body.SERVICE_TIMES);
      $("#serviceExpiredDate").val(pickDate(body.SERVICE_EXPIRED_DATE,null,null));
      $("#serviceSource").val(body.SERVICE_SOURCE);
      $("#serviceDetail").val(body.SERVICE_DETAIL);
    }

    $("#edit").click(function () {
      updateSysService();
    })

    function updateSysService() {
      var url=$utils.ajax.setUrl("userService/updateServiceById");
      var params={};
      params.inId = $utils.string.getQueryParam("id");
      params.servicetype = $("#serviceType").val();
      params.serviceUsedTimes = $("#serviceUsedTimes").val();
      params.serviceAvailableTimes = $("#serviceAvailableTimes").val();
      params.serviceExpiredDate = $("#serviceExpiredDate").val();
      params.serviceSource=$("#serviceSource").val();
      params.serviceDetail = $("#serviceDetail").val();
      var flag = $("#editForm").valid();
      if(!flag){
        return false;
      }
      axios.post(url, params)
        .then(function (response) {
          window.location.href = "#page/service/list";
        })
        .catch(function (error) {
        });
    }
  });


});
