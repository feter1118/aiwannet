var scripts = [null, "../assets/js/jquery.raty.js", "../assets/js/date-time/bootstrap-datepicker.js", "../assets/js/date-time/bootstrap-timepicker.js", "../assets/js/jquery.validate.js", "../assets/js/jquery.validate_msgzh.js", null]
$('.page-content-area').ace_ajax('loadScripts', scripts, function () {
  jQuery(function ($) {
    $('#serviceExpiredDate').datepicker({
      language: 'ch',
      autoclose: true,
      todayHighlight: true
    }).next().on(ace.click_event, function () {
      $(this).prev().focus();
    });

    var url=$utils.ajax.setUrl("dicItem/getDicItemListByDicCode");
    var params={};
    params.dicCode ='D00000014';
    axios.post(url, params)
      .then(function (response) {
        console.info(response);
        var array = response.body;
        for(var i=0;i<array.length;i++){
          $("#serviceType").append("<option value='"+array[i].ITEM_CODE+"'>"+array[i].ITEM_NAME+"</option>");
        }
      })
      .catch(function (error) {
      });

    var params1={};
    params1.dicCode ='D00000015';
    axios.post(url, params1)
      .then(function (response) {
        console.info(response);
        var array = response.body;
        for(var i=0;i<array.length;i++){
          $("#serviceSource").append("<option value='"+array[i].ITEM_CODE+"'>"+array[i].ITEM_NAME+"</option>");
        }
      })
      .catch(function (error) {
      });

    $("#add").click(function () {
      addSysService();
    })
  });
  function addSysService() {
    var params={}
    params.userPhone=$("#userPhone").val();
    params.servicetype=$("#serviceType").val();
    params.serviceTimes=$("#serviceTimes").val();
    params.serviceSource=$("#serviceSource").val();
    params.serviceDetail = $("#serviceDetail").val();
    params.serviceExpiredDate=$("#serviceExpiredDate").val();
    var flag = $("#editForm").valid();
    if(!flag){
      return false
    }
    var url=$utils.ajax.setUrl("userService/addService");
    axios.post(url, params)
      .then(function (response) {
        if(response.code='10000'){
          window.location.href="#page/service/list"
        }
      })
      .catch(function (error) {
      });
  }
});
