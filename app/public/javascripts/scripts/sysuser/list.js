var scripts = [null, "", null];
var grid_sysUser = "#grid-table";
var pager_sysUser = "#grid-pager";
var url = $utils.ajax.setUrl("user/queryList")
$('.page-content-area').ace_ajax('loadScripts', scripts, function() {
  jQuery(function($) {
    $(window).on('resize.jqGrid', function () {
      $(grid_sysUser).jqGrid( 'setGridWidth', $(".page-content").width() );
    })
    var parent_column = $(grid_sysUser).closest('[class*="col-"]');
    $(document).on('settings.ace.jqGrid' , function(ev, event_name, collapsed) {
      if( event_name === 'sidebar_collapsed' || event_name === 'main_container_fixed' ) {
        //setTimeout is for webkit only to give time for DOM changes and then redraw!!!
        setTimeout(function() {
          $(grid_sysUser).jqGrid( 'setGridWidth', parent_column.width() );
        }, 0);
      }
    })
    var optinons=jqSetting(grid_sysUser,pager_sysUser,url);
    optinons.url=url;
    optinons.caption="系统用户列表"
    optinons.colNames=['操作 ','ID', '姓名','登陆帐户','手机号','邮箱', '性别', '更新时间'];
    optinons.colModel=[
      {name:'operation',index:'', width:80, fixed:true, sortable:false, resize:false,
        formatter:operation,
      },
      {display:"USER_ID",name:'USER_ID',index:'USER_ID', width:60, sorttype:"int", hidden:true,editable: true},
      {display:"姓名",name:'USER_NAME',index:'USER_NAME',width:90, editable:true, sorttype:"date",hidden:false},
      {display:"登陆帐户",name:'USER_ACCOUNT',index:'USER_ACCOUNT',width:90, editable:true, sorttype:"date",hidden:false},
      {display:"手机号",name:'MOBILE',index:'MOBILE', width:150,editable: true},
      {display:"邮箱",name:'EMAIL',index:'EMAIL', width:150,editable: true},
      {display:"性别",name:'SEX',index:'SEX', width:70, editable: true,formatter:function (v,o,r) {
          if(v==1){
            return "男"
          }else if(v==2){
            return "女"
          }else{
            return ""
          }
        }},
      {display:"更新时间",name:'UPDATE_TIME',index:'UPDATE_TIME', width:150, sortable:false,editable: false,formatter:pickDate}
    ];
    jQuery(grid_sysUser).jqGrid(optinons);
    $(window).triggerHandler('resize.jqGrid');//trigger window resize to make the grid get the correct size
    function operation(v,o,r) {
      var html=""
      html+='<span onclick="goEdit('+r.USER_ID+')" class="ace-icon fa fa-pencil blue bigger-150 cursor-p tt-cursor pld10 prd10"></span>'
      html+='<span onclick="del('+r.USER_ID+')" class="ace-icon fa fa-trash-o red bigger-150 cursor-p tt-cursor"></span>'
      return html
    }
    $(document).one('ajaxloadstart.page', function(e) {
      $(grid_sysUser).jqGrid('GridUnload');
      $('.ui-jqdialog').remove();
    });
  });
});
function goEdit(id) {
  window.location.href="#page/sysuser/edit?id="+id;
}
function del(id) {
  bootbox.confirm("确定删除吗",function (result) {
    if(result){
      var url=$utils.ajax.setUrl("user/deleteUserById");
      var params={}
      params.userId=id;
      axios.post(url, params)
        .then(function (response) {
          $(grid_sysUser).trigger("reloadGrid");
        })
        .catch(function (error) {
        });
    }
  });
}
