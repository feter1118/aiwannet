var scripts = [null, "../assets/js/jquery.validate.js", "../assets/js/jquery.validate_msgzh.js",null]
$('.page-content-area').ace_ajax('loadScripts', scripts, function () {

  jQuery(function ($) {
    getRoleAll()
    function getRoleAll() {
      var url=$utils.ajax.setUrl("role/getRoleAll");
      var params={}
      params.userId=$utils.string.getQueryParam("id")
      axios.post(url, params)
        .then(function (response) {
          renderRoleAll(response.body)
        })
        .catch(function (error) {
        });
    }
    function renderRoleAll(data) {
      var html=""
      $.each(data,function (key,val) {
        html+='<option value="'+val.roleId+'">'+val.roleName+'</option>'
      })
      $("#roleId").html(html)
    }
    $("#add").click(function () {
      addSysUser()
    })
  });

  function addSysUser() {

    var params={}
    params.userName=$("#userName").val()
    params.userAccount=$("#userAccount").val()
    params.roleId=$("#roleId").val()
    params.userPwd=$("#userPwd").val()
    params.mobile=$("#mobile").val()
    params.email=$("#email").val()
    params.sex=$("#sex input[name=sex]:checked").val()
    var flag = $("#editForm").valid();
    if(!flag){
      return false
    }
    console.log(flag)

    console.log($utils.validate.validatePhone(params.mobile))
    if (params.mobile.length > 0) {
      if (!$utils.validate.validatePhone(params.mobile)) {
        $(".error[for=mobile]").html("手机号不正确")
        return false
      }
    }
    if (params.email.length > 0) {
      if(!$utils.validate.validateEmail(params.email)){
        $(".error[for=email]").html("邮箱不正确")
        return false
      }
    }
    var url=$utils.ajax.setUrl("user/addUser");
    axios.post(url, params)
      .then(function (response) {
        window.location.href="#page/sysuser/list"
      })
  }
});
