var scripts = [null, "../assets/js/jquery.validate.js", "../assets/js/jquery.validate_msgzh.js",null]
$('.page-content-area').ace_ajax('loadScripts', scripts, function () {
  jQuery(function ($) {
    function getRoleAll() {
      var url=$utils.ajax.setUrl("role/getRoleAll");
      var params={}
      params.userId=$utils.string.getQueryParam("id")
      return axios.post(url, params)
        .then(function (response) {
          return response.body
        })
        .catch(function (error) {
        });
    }
    function renderRoleAll(data) {
      var html=""
      $.each(data,function (key,val) {
        html+='<option value="'+val.roleId+'">'+val.roleName+'</option>'
      })
      $("#roleId").html(html)
    }
    function getUserById() {
      var url=$utils.ajax.setUrl("user/getUserById");
      var params={}
      params.userId=$utils.string.getQueryParam("id")
     return axios.post(url, params)
        .then(function (response) {
          return response.body
        })
        .catch(function (error) {
        });
    }
    function renderSysUser(body) {
      $("#userName").val(body.userName)
      $("#userAccount").val(body.userAccount)
      $("#mobile").val(body.mobile)
      $("#email").val(body.email)
      $("#sex input[name=sex][value="+body.sex+"]").prop("checked",true)
      $("#userName").val(body.userName)
    }


    axios.all([getRoleAll(), getUserById()])
      .then(axios.spread(function (getRoleAll, getUserById) {
        renderRoleAll(getRoleAll)
        renderSysUser(getUserById)
        $("#roleId").val(getUserById.roleId)
      }));

    $("#update").click(function () {
      updateSysUser()
    })

    function updateSysUser() {
      var url=$utils.ajax.setUrl("user/updateUserById");
      var params={}
      params.userId=$utils.string.getQueryParam("id")
      params.userName=$("#userName").val()
      params.roleId=$("#roleId").val()
      params.userAccount=$("#userAccount").val()
      params.mobile=$("#mobile").val()
      params.email=$("#email").val()
      params.sex=$("#sex input[name=sex]:checked").val()
      var flag = $("#editForm").valid();
      if(!flag){
        return false
      }

      if (params.mobile.length > 0) {
        if (!$utils.validate.validatePhone(params.mobile)) {
          $("label.error[for='mobile']").html("手机号不正确")
          return false
        }
      }
      if (params.email.length > 0) {
        if(!$utils.validate.validateEmail(params.email)){
          $("label.error[for='email']").html("邮箱不正确")
          return false
        }
      }

      axios.post(url, params)
        .then(function (response) {
          var token=response.body.token;
          $.cookie('token', token, { expires: 7, path: '/' });
          return response
        })
        .catch(function (error) {
        });

    }
  });


});
