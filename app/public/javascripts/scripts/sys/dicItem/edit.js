var scripts = [null, '../assets/js/bootstrap-multiselect.js', "../assets/js/jquery.validate.js", "../assets/js/jquery.validate_msgzh.js", '../assets/js/bootstrap-tagsinput.js', null]

$('.page-content-area').ace_ajax('loadScripts', scripts, function () {

  getData();
  
  $("#update").click(function () {
      updateDicItem();
  })

  function updateDicItem() {
    var params = {}
    params.itemId = $utils.string.getQueryParam("id");
    params.itemName = $("#itemName").val();
    params.itemCode = $("#itemCode").val();
    
    if ($utils.string.isNull(params.dicCode)) {
        bootbox.alert("请选择数据字典")
        return false
      }
    
    if ($utils.string.isNull(params.itemCode)) {
        bootbox.alert("请输入数据项代码")
        return false
    }
    
    if (params.itemCode.length>100) {
        bootbox.alert("数据代码项不能超过100字")
        return false
    }
    
    if ($utils.string.isNull(params.itemName)) {
        bootbox.alert("请输入数据项名称")
        return false
      }
    
    if (params.itemName.length>200) {
        bootbox.alert("数据项名称不能超过200字")
        return false
    }

    var url = $utils.ajax.setUrl("dicItem/updateDicItemById");
    axios.post(url, params)
      .then(function (response) {
        bootbox.alert(response.msg);
          window.location.href = "#page/sys/dicItem/list"
      })
  }

});

function getData() {
    var url=$utils.ajax.setUrl("dicItem/getDicItemById");
    var params={}
    params.itemId=$utils.string.getQueryParam("id")
    axios.post(url, params)
      .then(function (response) {
        renderDic(response.body)
      })
      .catch(function (error) {
      });
}

function renderDic(body) {
    $("#itemCode").val(body.itemCode);
    $("#itemName").val(body.itemName);
    
    var url = $utils.ajax.setUrl("dic/getDicAll");
  	axios.post(url, {})
  		.then(function (response) {
  		var dic = response.body;
  		var html = '';
  		$.each(dic, function (key, val) {
  		    html += '<option value="' + val.DIC_CODE + '">' + val.DIC_NAME + '</option>'
  		});
  		$("#dicCode").html(html);
  		$("#dicCode").val(body.dicCode);
  	});
}




