var scripts = [null, "", null];
var grid_sysDicItem = "#grid-table";
var pager_sysDicItem = "#grid-pager";
var url = $utils.ajax.setUrl("dicItem/queryList")
$('.page-content-area').ace_ajax('loadScripts', scripts, function() {
	renderDicCode('dicCode');
  jQuery(function($) {
    $(window).on('resize.jqGrid', function () {
      $(grid_sysDicItem).jqGrid( 'setGridWidth', $(".page-content").width() );
    })
    var parent_column = $(grid_sysDicItem).closest('[class*="col-"]');
    $(document).on('settings.ace.jqGrid' , function(ev, event_name, collapsed) {
      if( event_name === 'sidebar_collapsed' || event_name === 'main_container_fixed' ) {
        //setTimeout is for webkit only to give time for DOM changes and then redraw!!!
        setTimeout(function() {
          $(grid_sysDicItem).jqGrid( 'setGridWidth', parent_column.width() );
        }, 0);
      }
    })
    var optinons=jqSetting(grid_sysDicItem,pager_sysDicItem,url);
    optinons.url=url;
    optinons.caption="系统数据字典项列表"
    optinons.colNames=['操作 ','ID', '数据项代码','数据项名称','数据字典代码','数据字典名称', '更新时间'];
    optinons.colModel=[
      {name:'operation',index:'', width:80, fixed:true, sortable:false, resize:false,
        formatter:operation,
      },
      {display:"ITEM_ID",name:'ITEM_ID',index:'ITEM_ID', width:60, sorttype:"int", hidden:true,editable: true},
      {display:"数据项代码",name:'ITEM_CODE',index:'ITEM_CODE',width:90, editable:true, sorttype:"date",hidden:false},
      {display:"数据项名称",name:'ITEM_NAME',index:'ITEM_NAME',width:90, editable:true, sorttype:"date",hidden:false},
      {display:"数据字典代码",name:'DIC_CODE',index:'DIC_CODE', width:150,editable: true},
      {display:"数据字典名称",name:'DIC_NAME',index:'DIC_NAME', width:150,editable: true},
      {display:"更新时间",name:'UPDATE_TIME',index:'UPDATE_TIME', width:150, sortable:false,editable: false,formatter:pickDate}
    ];
    jQuery(grid_sysDicItem).jqGrid(optinons);
    $(window).triggerHandler('resize.jqGrid');//trigger window resize to make the grid get the correct size
    function operation(v,o,r) {
      var html=""
      html+='<span onclick="goEdit('+r.ITEM_ID+')" class="ace-icon fa fa-pencil blue bigger-150 cursor-p tt-cursor pld10 prd10"></span>'
      html+='<span onclick="del('+r.ITEM_ID+')" class="ace-icon fa fa-trash-o red bigger-150 cursor-p tt-cursor"></span>'
      return html
    }
    $(document).one('ajaxloadstart.page', function(e) {
      $(grid_sysDicItem).jqGrid('GridUnload');
      $('.ui-jqdialog').remove();
    });
  });
  
  /*数据字典*/
	function renderDicCode(id) {
	  	var url = $utils.ajax.setUrl("dic/getDicAll");
	  	axios.post(url, {})
	  		.then(function (response) {
	  		var dic = response.body;
	  		var html = '<option value="">全部</option>'
	  		$.each(dic, function (key, val) {
	  		    html += '<option value="' + val.DIC_CODE + '">' + val.DIC_NAME + '</option>'
	  		});
	  		$("#" + id).html(html);
	  	});
	}
	
});
function goEdit(id) {
  window.location.href="#page/sys/dicItem/edit?id="+id;
}
function del(id) {
  bootbox.confirm("确定删除吗",function (result) {
    if(result){
      var url=$utils.ajax.setUrl("dicItem/deleteDicItemById");
      var params={}
      params.itemId=id;
      axios.post(url, params)
        .then(function (response) {
          $(grid_sysDicItem).trigger("reloadGrid");
        })
        .catch(function (error) {
        });
    }
  });
}

function search() {
	  var url = $utils.ajax.setUrl("dicItem/queryList");
	  var params = {}
	  params.dicCode = $("#dicCode").val();
	  params.itemCode = $("#itemCode").val();
	  params.itemName = $("#itemName").val();
	  $(grid_sysDicItem).jqGrid('setGridParam', {
	    url: url,
	    contentType: 'application/json;charset=utf-8',
	    page: 1,
	    postData: params
	    // 发送数据
	  }).trigger("reloadGrid");
	}

