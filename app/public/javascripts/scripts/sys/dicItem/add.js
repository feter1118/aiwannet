var scripts = [null, '../assets/js/bootstrap-multiselect.js', "../assets/js/jquery.validate.js", "../assets/js/jquery.validate_msgzh.js", '../assets/js/bootstrap-tagsinput.js', null]

$('.page-content-area').ace_ajax('loadScripts', scripts, function () {
  
	renderDicCode('dicCode')
	
  $("#add").click(function () {
    addDicItem()
  })

  function addDicItem() {
    var params = {}
    params.itemCode = $("#itemCode").val();
    params.itemName = $("#itemName").val();
    params.dicCode = $("#dicCode").val();

    if ($utils.string.isNull(params.dicCode)) {
      bootbox.alert("请选择数据字典")
      return false
    }
    
    if ($utils.string.isNull(params.itemCode)) {
        bootbox.alert("请输入数据项代码")
        return false
      }
      
      if (params.itemCode.length>100) {
          bootbox.alert("数据项代码不能超过100字")
          return false
      }
    
    if ($utils.string.isNull(params.itemName)) {
        bootbox.alert("请输入数据项名称")
        return false
      }
      
      if (params.itemName.length>100) {
          bootbox.alert("数据项名称不能超过100字")
          return false
      }

    var url = $utils.ajax.setUrl("dicItem/addDicItem");
    axios.post(url, params)
      .then(function (response) {
        bootbox.alert(response.msg);
          window.location.href = "#page/sys/dicItem/list"
      })
  }
	
	/*数据字典*/
	function renderDicCode(id) {
	  	var url = $utils.ajax.setUrl("dic/getDicAll");
	  	axios.post(url, {})
	  		.then(function (response) {
	  		var dic = response.body;
	  		var html = '';
	  		$.each(dic, function (key, val) {
	  		    html += '<option value="' + val.DIC_CODE + '">' + val.DIC_NAME + '</option>'
	  		});
	  		$("#" + id).html(html);
	  	});
	}

});

