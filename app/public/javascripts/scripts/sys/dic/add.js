var scripts = [null, '../assets/js/bootstrap-multiselect.js', "../assets/js/jquery.validate.js", "../assets/js/jquery.validate_msgzh.js", '../assets/js/bootstrap-tagsinput.js', null]

$('.page-content-area').ace_ajax('loadScripts', scripts, function () {
  
  $("#add").click(function () {
    addDic()
  })

  function addDic() {
    var params = {}
    params.dicCode = $("#dicCode").val();
    params.dicName = $("#dicName").val();

    if ($utils.string.isNull(params.dicCode)) {
      bootbox.alert("请输入数据字典代码")
      return false
    }
    
    if (params.dicCode.length>100) {
        bootbox.alert("数据字典代码不能超过100字")
        return false
    }
    
    if ($utils.string.isNull(params.dicName)) {
        bootbox.alert("请输入数据字典名称")
        return false
      }
      
      if (params.dicName.length>100) {
          bootbox.alert("数据字典名称不能超过100字")
          return false
      }

    var url = $utils.ajax.setUrl("dic/addDic");
    axios.post(url, params)
      .then(function (response) {
        bootbox.alert(response.msg);
          window.location.href = "#page/sys/dic/list"
      })
  }

});

