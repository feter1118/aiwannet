var scripts = [null, '../assets/js/bootstrap-multiselect.js', "../assets/js/jquery.validate.js", "../assets/js/jquery.validate_msgzh.js", '../assets/js/bootstrap-tagsinput.js', null]

$('.page-content-area').ace_ajax('loadScripts', scripts, function () {

  getData();
  
  $("#update").click(function () {
      updateDic();
  })

  function updateDic() {
    var params = {}
    params.dicId = $utils.string.getQueryParam("id");
    params.dicName = $("#dicName").val();
    params.dicCode = $("#dicCode").val();
    
    if ($utils.string.isNull(params.dicCode)) {
        bootbox.alert("请输入数据字典代码")
        return false
    }
    
    if (params.dicCode.length>100) {
        bootbox.alert("数据字典代码不能超过100字")
        return false
    }
    
    if ($utils.string.isNull(params.dicName)) {
        bootbox.alert("请输入数据字典名称")
        return false
      }
    
    if (params.dicName.length>200) {
        bootbox.alert("数据字典名称不能超过200字")
        return false
    }

    var url = $utils.ajax.setUrl("dic/updateDicById");
    axios.post(url, params)
      .then(function (response) {
        bootbox.alert(response.msg);
          window.location.href = "#page/sys/dic/list"
      })
  }

});

function getData() {
    var url=$utils.ajax.setUrl("dic/getDicById");
    var params={}
    params.dicId=$utils.string.getQueryParam("id")
    axios.post(url, params)
      .then(function (response) {
        renderDic(response.body)
      })
      .catch(function (error) {
      });
}

function renderDic(body) {
    $("#dicCode").val(body.dicCode);
    $("#dicName").val(body.dicName);
}


