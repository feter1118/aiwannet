var scripts = [null, "", null];
var grid_sysDic = "#grid-table";
var pager_sysDic = "#grid-pager";
var url = $utils.ajax.setUrl("dic/queryList")
$('.page-content-area').ace_ajax('loadScripts', scripts, function() {
  jQuery(function($) {
    $(window).on('resize.jqGrid', function () {
      $(grid_sysDic).jqGrid( 'setGridWidth', $(".page-content").width() );
    })
    var parent_column = $(grid_sysDic).closest('[class*="col-"]');
    $(document).on('settings.ace.jqGrid' , function(ev, event_name, collapsed) {
      if( event_name === 'sidebar_collapsed' || event_name === 'main_container_fixed' ) {
        //setTimeout is for webkit only to give time for DOM changes and then redraw!!!
        setTimeout(function() {
          $(grid_sysDic).jqGrid( 'setGridWidth', parent_column.width() );
        }, 0);
      }
    })
    var optinons=jqSetting(grid_sysDic,pager_sysDic,url);
    optinons.url=url;
    optinons.caption="系统数据字典列表"
    optinons.colNames=['操作 ','ID', '数据字典代码','数据字典名称','父级数据字典', '更新时间'];
    optinons.colModel=[
      {name:'operation',index:'', width:80, fixed:true, sortable:false, resize:false,
        formatter:operation,
      },
      {display:"DIC_ID",name:'DIC_ID',index:'DIC_ID', width:60, sorttype:"int", hidden:true,editable: true},
      {display:"数据字典代码",name:'DIC_CODE',index:'DIC_CODE',width:90, editable:true, sorttype:"date",hidden:false},
      {display:"数据字典名称",name:'DIC_NAME',index:'DIC_NAME',width:90, editable:true, sorttype:"date",hidden:false},
      {display:"父级数据字典",name:'DIC_PNAME',index:'DIC_PNAME', width:150,editable: true},
      {display:"更新时间",name:'UPDATE_TIME',index:'UPDATE_TIME', width:150, sortable:false,editable: false,formatter:pickDate}
    ];
    jQuery(grid_sysDic).jqGrid(optinons);
    $(window).triggerHandler('resize.jqGrid');//trigger window resize to make the grid get the correct size
    function operation(v,o,r) {
      var html=""
      html+='<span onclick="goEdit('+r.DIC_ID+')" class="ace-icon fa fa-pencil blue bigger-150 cursor-p tt-cursor pld10 prd10"></span>'
      html+='<span onclick="del('+r.DIC_ID+')" class="ace-icon fa fa-trash-o red bigger-150 cursor-p tt-cursor"></span>'
      return html
    }
    $(document).one('ajaxloadstart.page', function(e) {
      $(grid_sysDic).jqGrid('GridUnload');
      $('.ui-jqdialog').remove();
    });
  });
});
function goEdit(id) {
  window.location.href="#page/sys/dic/edit?id="+id;
}
function del(id) {
  bootbox.confirm("确定删除吗",function (result) {
    if(result){
      var url=$utils.ajax.setUrl("dic/deleteDicById");
      var params={}
      params.dicId=id;
      axios.post(url, params)
        .then(function (response) {
          $(grid_sysDic).trigger("reloadGrid");
        })
        .catch(function (error) {
        });
    }
  });
}

function search() {
	  var url = $utils.ajax.setUrl("dic/queryList");
	  var params = {}
	  params.dicCode = $("#dicCode").val();
	  params.dicName = $("#dicName").val();
	  $(grid_sysDic).jqGrid('setGridParam', {
	    url: url,
	    contentType: 'application/json;charset=utf-8',
	    page: 1,
	    postData: params
	    // 发送数据
	  }).trigger("reloadGrid");
	}

