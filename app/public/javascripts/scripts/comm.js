/*
* 产品预览
* */
function preview(id) {
  var PREVPATH = 'http://www.aiwanlx.com/destination/'
  if(window.location.hostname=='localhost'||window.location.hostname=='47.95.215.100'){
    PREVPATH='http://47.95.215.100/destination/'
  }
  var Suffix = '.html?preview=1'
  var path = PREVPATH + id + Suffix;
  return '<a class="pld10 prd10" target="_blank" href="' + path + '"><icon class="ace-icon fa fa-eye bigger-150"></icon></a>'
}

/*
* 获取树节点
* */
function getSelectTreetParam(TreeId) {
  var zTree = $.fn.zTree.getZTreeObj(TreeId);
  var nodes = zTree.getCheckedNodes(true);
  var SelectIdStrs = [];
  $.each(nodes, function (key, val) {
    SelectIdStrs.push(val.id)
  });
  if (SelectIdStrs.length > 0) {
    return SelectIdStrs.join(",");
  } else {
    return '';
  }
}

/*
* selectType 单选还是多选 1单选 2多选
* resourceType 资源类型
* resourceVal 资源应用范围的值
* cb 回调
* */
function resourceModel(selectType, resourceType, resourceVal, cb) {
  var html = '<div class="resourceModel" id="resourceModel">' +
    '  <div class="col-sm-2"></div>' +
    '  <div class="col-sm-8 middle">' +
    '    <div class="ModelSearch">' +
    '      <h3 class="header smaller lighter green"><span class="fl">资源管理</span><i class="ace-icon glyphicon glyphicon-remove fr green close"></i></h3>' +
    '      <div class="clear"></div>' +
    '      <div class="form-search form-horizontal">' +
    '<div class="form-group">' +
    ' <div class="col-sm-3">' +
    '      <span class="input-icon">' +
    '      <input type="text" class="nav-search-input" id="res-keyword"' +
    '                       autocomplete="off">' +
    '      <i class="ace-icon fa fa-search nav-search-icon"></i>' +
    '      </span>' +
    '</div>' +
    ' <div class="col-sm-4">' +
    '<label class="col-sm-4 control-label" for="resourceCategory"> 应用范围</label>' +
    '        <select id="resCategory" class="col-xs-12 col-sm-8">' +
    '          <option value="">全部</option>' +
    '        </select>' +
    '</div>' +
    ' <div class="col-sm-4">' +
    '        <span class="btn btn-success btn-sm search">' +
    '          <i class="ace-icon glyphicon glyphicon-search"></i>' +
    '          搜索' +
    '        </span>' +
    '        <span class="btn btn-success btn-sm ok">' +
    '          <i class="ace-icon fa fa-check"></i>' +
    '          确定选择' +
    '        </span>' +
    '</div>' +

    '</div>' +
    '      </div>' +
    '    </div>' +
    '    <div class="clear"></div>' +
    '    <div class="hr hr-double hr-dotted hr-6"></div>' +
    '    <ul class="ModelContent no-margin">' +
    // '      <li class="item">' +
    // '        <img class="thumb" data-id="1" src="../download/11287113923_57d37ed9d3_q.jpg"/>' +
    // '        <p class="tag">' +
    // '          <span class="label label-lg label-pink arrowed-right">1111</span>' +
    // '          <span class="label label-lg label-yellow arrowed-in">2222</span>' +
    // '        </p>' +
    // '      </li>' +
    '    </ul>' +
    '  </div>' +
    '  <div class="col-sm-2"></div>' +
    '  <div class="mask"></div>' +
    '</div>';
  var $html = $(html);
  var $body = $('body');


  var appResScope = JSON.parse(localStorage.getItem("appResScope"));
  for (var i = 0; i < appResScope.length; i++) {
    $html.find("#resCategory").append("<option value='" + appResScope[i].ITEM_CODE + "'>" + appResScope[i].ITEM_NAME + "</option>");
  }
  if (resourceVal != "") {
    $html.find("#resCategory").val(resourceVal);
    $html.find("#resCategory").prop("disabled", true)
  }
  $body.append($html);

  $html.on('click', '.search', function (e) {
    $html.find(".ModelContent").empty();
    renderResList(1)
  });
  $html.on('click', '.close', function (e) {
    offclick()
    $html.remove();
  });

  $html.on('click', '.ok', function (e) {
    var data = []
    $html.find(".ModelContent>.item.selected").each(function (index, element) {
      var item = {}
      item.id = $(element).find("img").attr("data-id")
      item.src = $(element).find("img").attr("src")
      data.push(item)
    })
    offclick()
    $html.remove();
    cb(data)
  });

  var page = 1;
  $html.find(".ModelContent").on('scroll', function (e) {
    var nDivHight = $(this).height() * 1;
    var nScrollHight = $(this)[0].scrollHeight * 1;
    var nScrollTop = $(this)[0].scrollTop * 1;
    if (nDivHight + nScrollTop >= nScrollHight * 0.9) {
      page++;
      renderResList(page)
    }
  })

  function offclick() {
    $html.off("click", ".close");
    $html.off("click", ".ok");
    $html.off("click", ".search");
    $html.find('.ModelContent .item').off("click");
    $html.find(".ModelContent").off("scroll")
  }

  renderResList(1)

  function renderResList(page) {
    var url = $utils.ajax.setUrl("resource/queryResourceList");
    var params = {}
    params.resourceType = "";
    params.resourceCategory = $html.find("#resCategory").val();
    params.resourceLabel = $html.find("#res-keyword").val();
    params.page = page;
    params.rows = 12;
    /*
        $.ajax({
          url: url,
          type: "post",
          async: true,
          data: params,
          success: function (data) {

          },
          error: function () {
            alert("添加图片失败");
          }
        });
    */
    axios.post(url, params)
      .then(function (response) {
        if (response.body.rows.length > 0) {
          renderList(response.body.rows)
        }
      })
      .catch(function (error) {
      });
  }

  function renderList(list) {
    var h = ""
    $.each(list, function (key, val) {
      var labelstyle = 'arrowed-in'
      if (key % 2 == 1) {
        labelstyle = 'arrowed-in'
      }
      h += '<li class="item">' +
        '<div class="thumb">' +
        '<img error=""  data-id="' + val.RESOURCE_ID + '" src="' + val.DOMAIN_NAME + val.RESOURCE_URL + val.RESOURCE_NAME + '"/>' +
        '</div>' +
        '<p class="tag">' +
        '<span class="label label-lg label-pink ' + labelstyle + '">' + val.RESOURCE_LABEL + '</span>' +
        '</p>' +
        '</li>';
    })
    $html.find(".ModelContent").append(h)

    $html.find('.ModelContent .item').off("click");
    $html.find('.ModelContent .item').on('click', function (e) {
      if (selectType == 2) {
        $(this).toggleClass('selected');
      }
      if (selectType == 1) {
        $(this).toggleClass('selected');
        $(this).siblings().removeClass("selected")
      }
    });
  }
}

/*
* 充值model
*
*
* */
function Recharge(cb) {
  var html = '<div class="RechargeModel">' +
    '  <div class="col-sm-4"></div>' +
    '  <div class="col-sm-4 middle">' +
    '    <h3 class="header smaller lighter green clearfix"><span class="fl">充值</span></h3>' +
    '    <div class="form-group clearfix">' +
    '      <label class="col-sm-3 control-label no-padding-right" for="scoreMoney"> 充值金额 </label>' +
    '      <div class="col-sm-9">' +
    '        <input type="text" id="scoreMoney" placeholder="" class="col-xs-10 col-sm-10">' +
    '      </div>' +
    '    </div>' +
    '    <div class="form-group clearfix">' +
    '      <label class="col-sm-3 control-label no-padding-right" for="scoreValue"> 积分 </label>' +
    '      <div class="col-sm-9">' +
    '        <input type="text" id="scoreValue" placeholder="" class="col-xs-10 col-sm-10">' +
    '      </div>' +
    '    </div>' +
    '    <div class="form-group clearfix">' +
    '      <label class="col-sm-3 control-label no-padding-right" for="scoreText"> 说明 </label>' +
    '      <div class="col-sm-9">' +
    '        <textarea type="text" id="scoreText" placeholder="" class="col-xs-10 col-sm-10"></textarea>' +
    '      </div>' +
    '    </div>' +
    '    <div class="hr dotted"></div>' +
    '    <div class="form-group  center">' +
    '      <span class="btn btn-success btn-sm ok">确定</span>' +
    '      <span class="btn btn-sm btn-warning cancel">放弃</span>' +
    '    </div>' +
    '  </div>' +
    '  <div class="col-sm-4"></div>' +
    '  <div class="mask"></div>' +
    '</div>'

  var $html = $(html);
  var $body = $('body');
  $body.append($html);

  $html.on('click', '.cancel', function (e) {
    offclick()
    $html.remove();
  });

  $html.on('click', '.ok', function (e) {
    offclick()
    var data = {}
    data.scoreMoney = $html.find("#scoreMoney").val()
    data.scoreValue = $html.find("#scoreValue").val()
    data.scoreText = $html.find("#scoreText").val()
    $html.remove();
    cb(data)
  });

  function offclick() {
    $html.off("click", ".cancel");
    $html.off("click", ".ok");
  }
}


/*
* 服务列表 消费服务
* */
function modelTemTwoInput(title, t1, t2, cb) {
  var html = '<div class="modelTemTwoInput">' +
    '  <div class="col-sm-4"></div>' +
    '  <div class="col-sm-4 middle">' +
    '    <h3 class="header smaller lighter green clearfix"><span class="fl">' + title + '</span></h3>' +
    '    <div class="form-group clearfix">' +
    '      <label class="col-sm-3 control-label no-padding-right" for="scoreMoney">' + t1 + '</label>' +
    '      <div class="col-sm-9">' +
    '        <input type="text" id="t1" placeholder="" class="col-xs-10 col-sm-10">' +
    '      </div>' +
    '    </div>' +
    '    <div class="form-group clearfix">' +
    '      <label class="col-sm-3 control-label no-padding-right" for="scoreValue">' + t2 + '</label>' +
    '      <div class="col-sm-9">' +
    '        <input type="text" id="t2" placeholder="" class="col-xs-10 col-sm-10">' +
    '      </div>' +
    '    </div>' +
    '    <div class="hr dotted"></div>' +
    '    <div class="form-group  center">' +
    '      <span class="btn btn-success btn-sm ok">确定</span>' +
    '      <span class="btn btn-sm btn-warning cancel">放弃</span>' +
    '    </div>' +
    '  </div>' +
    '  <div class="col-sm-4"></div>' +
    '  <div class="mask"></div>' +
    '</div>'
  var $html = $(html);
  var $body = $('body');
  $body.append($html);

  $html.on('click', '.cancel', function (e) {
    offclick()
    $html.remove();
  });

  $html.on('click', '.ok', function (e) {
    offclick()
    var data = {}
    data.t1 = $html.find("#t1").val()
    data.t2 = $html.find("#t2").val()
    $html.remove();
    cb(data)
  });

  function offclick() {
    $html.off("click", ".cancel");
    $html.off("click", ".ok");
  }
}


/*
* 添加旅客
* */
function modelAddPassenger(cb) {
  var html = '<div class="modelAddPassenger">' +
    '  <div class="col-sm-4"></div>' +
    '  <div class="col-sm-4 middle">' +
    '    <h3 class="header smaller lighter green clearfix"><span class="fl">添加旅客信息</span></h3>' +
    '    <div class="form-group clearfix">' +
    '      <label class="col-sm-3 control-label no-padding-right" for="scoreMoney">' + '姓名' + '</label>' +
    '      <div class="col-sm-9">' +
    '        <input type="text" id="t1" placeholder="" class="col-xs-10 col-sm-10">' +
    '      </div>' +
    '    </div>' +
    '    <div class="form-group clearfix">' +
    '      <label class="col-sm-3 control-label no-padding-right" for="scoreValue">' + '类型' + '</label>' +
    '      <div class="col-sm-9">' +
    '        <select id="t2" class="passengerType col-sm-10">' +
    '                  <option selected="" value="1">成人</option>' +
    '                  <option value="2">儿童</option>' +
    '                  <option value="3">老年人</option>' +
    '                </select>' +
    '      </div>' +
    '    </div>' +
    '    <div class="form-group clearfix">' +
    '      <label class="col-sm-3 control-label no-padding-right" for="scoreValue">' + '证件类型' + '</label>' +
    '      <div class="col-sm-9">' +
    '        <select id="t3" class="passengerType col-sm-10">' +
    '                  <option value="1">身份证</option>' +
    '                </select>' +
    '      </div>' +
    '    </div>' +
    '    <div class="form-group clearfix">' +
    '      <label class="col-sm-3 control-label no-padding-right" for="scoreValue">' + '证件号' + '</label>' +
    '      <div class="col-sm-9">' +
    '          <input type="text" id="t4" placeholder="" class="col-xs-10 col-sm-10">' +
    '      </div>' +
    '    </div>' +
    '    <div class="hr dotted"></div>' +
    '    <div class="form-group  center">' +
    '      <span class="btn btn-success btn-sm ok">确定</span>' +
    '      <span class="btn btn-sm btn-warning cancel">放弃</span>' +
    '    </div>' +
    '  </div>' +
    '  <div class="col-sm-4"></div>' +
    '  <div class="mask"></div>' +
    '</div>'
  var $html = $(html);
  var $body = $('body');
  $body.append($html);

  $html.on('click', '.cancel', function (e) {
    offclick()
    $html.remove();
  });

  $html.on('click', '.ok', function (e) {
    offclick()
    var data = {}
    data.t1 = $html.find("#t1").val()
    data.t2 = $html.find("#t2").val()
    data.t3 = $html.find("#t3").val()
    data.t4 = $html.find("#t4").val()
    $html.remove();
    cb(data)
  });

  function offclick() {
    $html.off("click", ".cancel");
    $html.off("click", ".ok");
  }
}


/*
* 添加旅客
* */
function modelAddFinerCost(cb) {
  var html = '<div class="modelAddFinerCost">' +
    '  <div class="col-sm-4"></div>' +
    '  <div class="col-sm-4 middle">' +
    '    <h3 class="header smaller lighter green clearfix"><span class="fl">添加费用细项</span></h3>' +
    '    <div class="form-group clearfix">' +
    '      <label class="col-sm-3 control-label no-padding-right" for="scoreMoney">' + '细项名称' + '</label>' +
    '      <div class="col-sm-9">' +
    '        <input type="text" id="t1" placeholder="" class="col-xs-10 col-sm-10">' +
    '      </div>' +
    '    </div>' +
    '    <div class="form-group clearfix">' +
    '      <label class="col-sm-3 control-label no-padding-right" for="scoreValue">' + '类型' + '</label>' +
    '      <div class="col-sm-9">' +
    '        <select id="t2" class="passengerType col-sm-10">' +
    '                  <option value="1">费用包含</option>' +
    '                  <option value="0">费用不含</option>' +
    '                </select>' +
    '      </div>' +
    '    </div>' +
    '    <div class="hr dotted"></div>' +
    '    <div class="form-group  center">' +
    '      <span class="btn btn-success btn-sm ok">确定</span>' +
    '      <span class="btn btn-sm btn-warning cancel">放弃</span>' +
    '    </div>' +
    '  </div>' +
    '  <div class="col-sm-4"></div>' +
    '  <div class="mask"></div>' +
    '</div>'
  var $html = $(html);
  var $body = $('body');
  $body.append($html);

  $html.on('click', '.cancel', function (e) {
    offclick()
    $html.remove();
  });

  $html.on('click', '.ok', function (e) {
    offclick()
    var data = {}
    data.t1 = $html.find("#t1").val()
    data.t2 = $html.find("#t2").val()
    $html.remove();
    cb(data)
  });

  function offclick() {
    $html.off("click", ".cancel");
    $html.off("click", ".ok");
  }
}

/*
* 温馨提示 添加
* */
function modeloneInput(title, t1, cb) {
  var html = '<div class="modelTemTwoInput">' +
    '  <div class="col-sm-4"></div>' +
    '  <div class="col-sm-4 middle">' +
    '    <h3 class="header smaller lighter green clearfix"><span class="fl">' + title + '</span></h3>' +
    '    <div class="form-group clearfix">' +
    '      <label class="col-sm-2 control-label no-padding-right" for="scoreValue">' + t1 + '</label>' +
    '      <div class="col-sm-10">' +
    '        <textarea type="text" id="t1" placeholder="" class="col-xs-10 col-sm-10"></textarea>' +
    '      </div>' +
    '    </div>' +
    '    <div class="hr dotted"></div>' +
    '    <div class="form-group  center">' +
    '      <span class="btn btn-success btn-sm ok">确定</span>' +
    '      <span class="btn btn-sm btn-warning cancel">放弃</span>' +
    '    </div>' +
    '  </div>' +
    '  <div class="col-sm-4"></div>' +
    '  <div class="mask"></div>' +
    '</div>'
  var $html = $(html);
  var $body = $('body');
  $body.append($html);

  $html.on('click', '.cancel', function (e) {
    offclick()
    $html.remove();
  });

  $html.on('click', '.ok', function (e) {
    offclick()
    var data = {}
    data.t1 = $html.find("#t1").val()
    $html.remove();
    cb(data)
  });

  function offclick() {
    $html.off("click", ".cancel");
    $html.off("click", ".ok");
  }
}

/*
* loading
* */
function modelLoading(v) {
  var html = '<div class="modelLoading">' +
    '  <div class="col-sm-4"></div>' +
    '  <div class="col-sm-4 middle">' +
    '<img src="../public/images/loading.gif" alt="">' +
    '  </div>' +
    '  <div class="col-sm-4"></div>' +
    '  <div class="mask"></div>' +
    '</div>'
  var $html = $(html);
  var $body = $('body');
  if (v) {
    $body.append($html);
  } else {
    $('.modelLoading').remove();
  }
}




/*
* jqgrid格式化
* */
function pickDate(v, o, r) {
  if (v != '') {
    return moment(v).format("YYYY-MM-DD");
  } else {
    return "";
  }
}

/*
* 填充父级菜单
* */
function renderPMenuList() {
  var html = '<option value="0">顶级菜单</option>'
  var pMenu = JSON.parse(localStorage.getItem("computedNav"));
  $.each(pMenu, function (key, val) {
    html += '<option value="' + val.menuId + '">' + val.menuName + '</option>'
  })
  $("#menuPid").html(html)
}

/*
*  获取 SessionStorage
* */

function getSessionStorage(key) {
  var result = ""
  if (sessionStorage.getItem(key) != null) {
    return JSON.parse(sessionStorage.getItem(key));
  } else {
    return result
  }
}

function getLocalStorage(key) {
  var result = ""
  if (localStorage.getItem(key) != null) {
    return JSON.parse(localStorage.getItem(key));
  } else {
    return result
  }
}

/*
*  set SessionStorage
* */

function setSessionStorage(key, obj) {
  sessionStorage.setItem(key, JSON.stringify(obj))
}

function setLocalStorage(key, obj) {
  localStorage.setItem(key, JSON.stringify(obj))
}

/*
* 重置目的地SessionStorage数据
* */
function resetDestination(cb) {
  var queryAllUrl = $utils.ajax.setUrl("destination/queryAllList");
  axios.post(queryAllUrl)
    .then(function (response) {
      setLocalStorage('destination', response.body)
      cb && cb()
    })
}



/*填充目的地*/
function renderDestination(id) {
  var destination = getLocalStorage('destination')
  var html = ''
  $.each(destination, function (key, val) {
    if(val.AREA_ID==1 || val.AREA_ID=="1"){
      html += '<option value="' + val.DESTINATION_ID + '">' + val.DESTINATION_NAME + '(区域)</option>'
    }else{
      html += '<option value="' + val.DESTINATION_ID + '">' + val.DESTINATION_NAME + '</option>'
    }
  })
  $("#" + id).append(html)
}

// 填充攻略列表
function renderStrategy(id, list) {
  var strategy = list || getLocalStorage('strategy')
  var html = ''
  $.each(strategy, function (key, val) {
    html += '<option value="' + val.STRATEGY_ID + '">' + val.STRATEGY_NAME + '</option>'
  })
  $("#" + id).html(html)
}


/*填充产品类型*/
function renderProductType(id) {
  var destination = getLocalStorage('productType')
  var html = ''
  $.each(destination, function (key, val) {
    html += '<option value="' + val.ITEM_CODE + '">' + val.ITEM_NAME + '</option>'
  })
  $("#" + id).html(html)
}


/*填充国家*/
function renderCountry(id) {
  var destination = getLocalStorage('country')
  var html = ''
  $.each(destination, function (key, val) {
    html += '<option value="' + val.ITEM_CODE + '">' + val.ITEM_NAME + '</option>'
  })
  $("#" + id).html(html)
}


/*填充目的地类型*/
function renderDestinationType(id) {
  var destination = getLocalStorage('destinationType')
  var html = ''
  $.each(destination, function (key, val) {
    html += '<option value="' + val.ITEM_CODE + '">' + val.ITEM_NAME + '</option>'
  })
  $("#" + id).append(html)
}


function renderActivityType(id) {
  var destination = getLocalStorage('activityType')
  var html = ''
  $.each(destination, function (key, val) {
    html += '<div class="checkbox col-sm-3">' +
      '                            <label>' +
      '                              <input name="form-field-checkbox" class="ace ace-checkbox-2" type="checkbox" value="'+val.ITEM_CODE +'">' +
      '                              <span class="lbl">'+val.ITEM_NAME+'</span>' +
      '                            </label>' +
      '                          </div>'
  })
  $("#" + id).html(html)
}


/*产品状态*/
function renderProductStatus(v) {
  var result=""
  switch (v) {
    case 1:
      result= "待付款"
      break
    case 2:
      result= "已付款"
      break
    case 3:
      result=  "处理中"
      break
    case 4:
      result=  "已完成"
      break
    case 5:
      result=  "已取消"
      break
  }
  return result
}

function removeArrItem(arr, val) {
  function indexOf(val) {
    for (var i = 0; i < arr.length; i++) {
      if (arr[i] == val) {
        return i;
      }
    }
    return -1;
  }

  var index = indexOf(val);
  if (index > -1) {
    arr.splice(index, 1);
  }
}

function hasNavRight(path) {
  var navList=getLocalStorage("nav");
  var result=false
  navList.forEach(function (val,index,arr) {
    if(val.menuUrl==path){
      result=true
    }
  })
  return result;
}

function MEMBER_GRADE(v) {
  if (v == 1) {
    return "银卡会员"
  } else if (v == 2) {
    return "金卡会员"
  } else if (v == 3) {
    return "黑卡会员"
  }else{
    return "不是会员"
  }
}

function MEMBER_GRADE_TYPE2(v) {
  if (v == 1) {
    return "<span class='pink'>【银】</span> "
  } else if (v == 2) {
    return "<span class='orange'>【金】</span> "
  } else if (v == 3) {
    return "<span class='brown'>【黑】</span> "
  } else {
    return "<span class='grey'>【普】</span> "
  }
}


function PASSENGERTYPE(v) {
  if (v == 1) {
    return "成年人"
  } else if (v == 2) {
    return "儿童"
  } else if (v == 3) {
    return "老年人"
  }else{
    return ""
  }
}

function CERTTYPE(v) {
  if (v == 1) {
    return "身份证"
  } else{
    return ""
  }
}

function getImageWidth(url,callback){
  var img = new Image();
  img.src = url;

  // 如果图片被缓存，则直接返回缓存数据
  if(img.complete){
    callback(img.width, img.height);
  }else{
    // 完全加载完毕的事件
    img.onload = function(){
      callback(img.width, img.height);
    }
  }
}

/*积分商城商品类别*/
function renderClassifyType(id) {
	var url = $utils.ajax.setUrl("/store/classify/getClassifyAll");
	axios.post(url, {})
		.then(function (response) {
		var classify = response.body;
		var html = ''
		$.each(classify, function (key, val) {
		    html += '<option value="' + val.CLASSIFY_ID + '">' + val.GOODS_CLASSIFY_NAME + '</option>'
		});
		$("#" + id).html(html);
	});

}

