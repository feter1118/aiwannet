var scripts = [null, "../assets/js/date-time/bootstrap-datepicker.js", "../assets/js/date-time/bootstrap-timepicker.js", "../assets/js/jquery.validate.js", "../assets/js/jquery.validate_msgzh.js", null]
$('.page-content-area').ace_ajax('loadScripts', scripts, function () {
  jQuery(function ($) {
    /*-----------------------------基本信息-----------------------------------*/
    getBuserById()

    function getBuserById() {
      var url = $utils.ajax.setUrl("buser/getBuserById");
      var params = {}
      params.buserId = $utils.string.getQueryParam("id")

      return axios.post(url, params)
        .then(function (response) {
          renderSysUser(response.body)
          renderPassengerList(response.body.passengerList)
        })
        .catch(function (error) {
        });
    }
    var accountId="";
    function renderSysUser(body) {
      $("#buserName").val(body.BUSER_NAME)
      $("#buserCertType").val(body.ACCOUNT_CERT_TYPE)
      $("#buserCertNo").val(body.ACCOUNT_CERT_NO)
      $("#buserMobile").val(body.BUSER_MOBILE)
      $("#buserEmail").val(body.BUSER_EMAIL)
      $("#buserAddress").val(body.BUSER_ADDRESS)
      $("#buserSex input[value='" + body.BUSER_SEX + "']").prop("checked", true)
      accountId =body.ACCOUNT_ID;
    }

    $("#update").click(function () {
      updateSysUser()

      function updateSysUser() {
        var url = $utils.ajax.setUrl("buser/updateBuserById");
        var params = {}
        params.accountId =accountId;
        params.bUserId = $utils.string.getQueryParam("id")
        params.buserName = $("#buserName").val()
        params.buserCertType = $("#buserCertType").val()
        params.buserCertNo = $("#buserCertNo").val()
        params.buserMobile = $("#buserMobile").val()
        params.buserEmail = $("#buserEmail").val()
        params.buserAddress = $("#buserAddress").val()
        params.buserSex = $("#buserSex input[name=sex]:checked").val()

        if (params.buserMobile.length == 0) {
          $(".error[for='buserMobile']").html("手机号不能为空")
          $("#buserMobile").focus()
          return false
        }
        
        if (params.buserMobile.length > 0) {
          if (!$utils.validate.validatePhone(params.buserMobile)) {
            $("label.error[for='buserMobile']").html("手机号不正确")
            return false
          }
        }
        
        if (params.buserEmail.length == 0) {
            $(".error[for='buserEmail']").html("邮箱不能为空")
            $("#buserEmail").focus()
            return false
        }
        if (params.buserEmail.length > 0) {
          if (!$utils.validate.validateEmail(params.buserEmail)) {
            $("label.error[for='buserEmail']").html("邮箱不正确")
            return false
          }
        }
        axios.post(url, params)
          .then(function (response) {
            bootbox.alert(response.msg)
          })
      }
    });
  })
});

/*-----------------------------旅客常用信息-----------------------------------*/
function renderPassengerList(list) {
  var html = '';
  $.each(list, function (key, val) {
    var select1 = val.PASSENGER_TYPE == 1 ? 'selected' : '';
    var select2 = val.PASSENGER_TYPE == 2 ? 'selected' : '';
    var select3 = val.PASSENGER_TYPE == 3 ? 'selected' : '';
    var select4 = val.PASSENGER_CERT_TYPE == 1 ? 'selected' : '';

    html += `<tr>
                <td class="hidden-480"><input type="text" class="passengerName" value="${val.PASSENGER_NAME}"></td>
                <td class="hidden-480"><select class="passengerType">
                  <option ${select1} value="1">成人</option>
                  <option ${select2} value="2">儿童</option>
                  <option ${select3} value="3">老年人</option>
                </select></td>
                <td class="hidden-480">
                  <select name="" class="passengerCertType">
                    <option ${select4}  value="1">身份证</option>
                  </select>
                </td>
                <td class="hidden-480"><input type="text" class="passengerCertNo" value="${val.PASSENGER_CERT_NO}"></td>
                <td class="hidden-480">
                <span class="btn btn-sm btn-danger" onclick="delPassenger(${val.PASSENGER_ID})">删除</span>
                <span class="btn btn-sm btn-primary" onclick="editPassenger(this,${val.PASSENGER_ID})">更新</span></td>
              </tr>`
  })
  $('#passengerListTable tbody').html(html)
}

function delPassenger(id) {
  bootbox.confirm("确认删除吗",function (v) {
    if(v){
      var url=$utils.ajax.setUrl("buser/delPassengerInfo");
      var params={}
      params.passengerId=id
      axios.post(url, params)
        .then(function (response) {
          bootbox.alert(response.msg)
          getPassengerList()
        })
    }
  })
}

function editPassenger(obj,id) {
  bootbox.confirm("确认修改吗",function (v) {
    if(v){
      var url=$utils.ajax.setUrl("buser/updatePassengerInfo");
      var params={}
      params.passengerId=id
      params.passengerName=$(obj).parents('tr').find(".passengerName").val()
      params.passengerType=$(obj).parents('tr').find(".passengerType").val()
      params.passengerCertType=$(obj).parents('tr').find(".passengerCertType").val()
      params.passengerCertNo=$(obj).parents('tr').find(".passengerCertNo").val()
      axios.post(url, params)
        .then(function (response) {
          bootbox.alert(response.msg)
        })
    }
  })
}

$("#addPassengerInfo").click(function () {
  AddPassenger()
})

function AddPassenger() {
  modelAddPassenger(function (data) {
    var url = $utils.ajax.setUrl("buser/addPassengerInfo");
    var params = {}
    params.buserId = $utils.string.getQueryParam("id")
    params.passengerName = data.t1
    params.passengerType = data.t2
    params.passengerCertType = data.t3
    params.passengerCertNo = data.t4
    axios.post(url, params)
      .then(function (response) {
        bootbox.alert(response.msg)
        getPassengerList()
      })
  })
}

function getPassengerList() {
  var url = $utils.ajax.setUrl("buser/getBuserById");
  var params = {}
  params.buserId = $utils.string.getQueryParam("id")
  axios.post(url, params)
    .then(function (response) {
      renderPassengerList(response.body.passengerList)
    })
}
