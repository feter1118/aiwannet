var scripts = [null, "", null];
var grid_biUser = "#grid-table";
var pager_biUser = "#grid-pager";
var url = $utils.ajax.setUrl("buser/queryList")
$('.page-content-area').ace_ajax('loadScripts', scripts, function() {
  jQuery(function($) {
    $(window).on('resize.jqGrid', function () {
      $(grid_biUser).jqGrid( 'setGridWidth', $(".page-content").width() );
    })
    var parent_column = $(grid_biUser).closest('[class*="col-"]');
    $(document).on('settings.ace.jqGrid' , function(ev, event_name, collapsed) {
      if( event_name === 'sidebar_collapsed' || event_name === 'main_container_fixed' ) {
        //setTimeout is for webkit only to give time for DOM changes and then redraw!!!
        setTimeout(function() {
          $(grid_biUser).jqGrid( 'setGridWidth', parent_column.width() );
        }, 0);
      }
    })
    var optinons=jqSetting(grid_biUser,pager_biUser,url);
    optinons.url=url;
    optinons.caption="业务用户列表"
    optinons.colNames=['操作 ','ID', '用户名', '手机号','登录账号','会员类型','剩余积分','消费积分','注册日期','会员到期日期','上次登录时间'];
    optinons.colModel=[
      {display:"操作",name:'operation',index:'', width:80, fixed:true, sortable:false, resize:false,
        formatter:operation
      },
      {display:"BUSER_ID",name:'BUSER_ID',index:'BUSER_ID', width:60, sorttype:"int", hidden:true,editable: true},
      {display:"用户名",name:'BUSER_NAME',index:'BUSER_NAME',width:120, editable:true, sorttype:"date",hidden:false,formatter:linkToDetail},
      {display:"手机号",name:'BUSER_MOBILE',index:'BUSER_MOBILE', width:150, editable: true},
      {display:"登陆帐户",name:'BUSER_ACCOUNT',index:'BUSER_ACCOUNT',width:90, editable:true, sorttype:"date",hidden:true},
      {display:"会员等级",name:'ACCOUNT_MEMBER_GRADE',index:'ACCOUNT_MEMBER_GRADE', width:70, editable: true,hidden:true},
      {display:"剩余积分",name:'ACCOUNT_AVAILABLE_SCORE',index:'ACCOUNT_AVAILABLE_SCORE', width:150,editable: true},
      {display:"消费积分",name:'ACCOUNT_USED_SCORE',index:'ACCOUNT_USED_SCORE', width:150,editable: true},
      {display:"注册日期",name:'CREATE_TIME',index:'CREATE_TIME', width:150, sortable:false,editable: false,formatter:pickDate},
      {display:"会员到期日期",name:'ACCOUNT_EXPIRE_DATE',index:'ACCOUNT_EXPIRE_DATE', width:150, sortable:false,editable: false,formatter:pickDate},
      {display:"用户上次登录时间",name:'BUSER_LAST_LOGIN_DATE',index:'BUSER_LAST_LOGIN_DATE', width:150, sortable:false,editable: false,formatter:pickDate}
    ];
    jQuery(grid_biUser).jqGrid(optinons);
    $(window).triggerHandler('resize.jqGrid');//trigger window resize to make the grid get the correct size

    function linkToDetail(v,o,r) {
      var GRADE=MEMBER_GRADE_TYPE2(r.ACCOUNT_MEMBER_GRADE)
      var html = ""
      if(v==null||v==""){
        v="匿名"
      }
      html += '<a href="#page/biuser/detail?id=' + r.BUSER_ID + '">'+ GRADE +v+ '</a>'
      return html
    }
    function operation(v,o,r) {
      var html=""
      html+='<span onclick="goEdit('+r.BUSER_ID+')" class="ace-icon fa fa-pencil blue bigger-150 cursor-p tt-cursor pld10 prd10"></span>'
      html+='<span onclick="del('+r.BUSER_ID+')" class="ace-icon fa fa-trash-o red bigger-150 cursor-p tt-cursor"></span>'
      return html
    }
    $(document).one('ajaxloadstart.page', function(e) {
      $(grid_biUser).jqGrid('GridUnload');
      $('.ui-jqdialog').remove();
    });
  });
});
function goEdit(id) {
  window.location.href="#page/biuser/edit?id="+id;
}
function del(id) {
  bootbox.confirm("确定删除吗",function (result) {
    if(result){
      var url=$utils.ajax.setUrl("buser/deleteBuserById");
      var params={}
      params.buserId =id;
      axios.post(url, params)
        .then(function (response) {
          $(grid_biUser).trigger("reloadGrid");
        })
        .catch(function (error) {
        });
    }
  });
}

function search() {
  var url = $utils.ajax.setUrl("buser/queryList");
  var params = {}
  params.userName = $("#userName").val()
  params.userPhone = $("#userPhone").val()
  $(grid_biUser).jqGrid('setGridParam', {
    url: url,
    contentType: 'application/json;charset=utf-8',
    page: 1,
    postData: params
    // 发送数据
  }).trigger("reloadGrid");
}

function exportBiUser() {
  var url = $utils.ajax.setUrl("buser/exportUserInfo")+'?userName='+$("#userName").val()+'&userPhone='+$("#userPhone").val();
  window.location.href=url;
}
