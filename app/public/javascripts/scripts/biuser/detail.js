var scripts = [null,"", null]
$('.page-content-area').ace_ajax('loadScripts', scripts, function() {
  jQuery(function($) {
    getBuserById()
    getHistoryById()
    renderBuserOrder()
    renderBuserInScore()
    renderBuserOutScore()
    renderBuserCollection()

    function renderBuserOrder() {
      var grid_buserOrder = "#grid-table-buserOrder";
      var pager_buserOrder = "#grid-pager-buserOrder";
      var url = $utils.ajax.setUrl("order/queryList")
      var params={}
      params.userId = $utils.string.getQueryParam("id")
      $(window).on('resize.jqGrid', function () {
        $(grid_buserOrder).jqGrid( 'setGridWidth', $(grid_buserOrder).parents(".tab-content").width()-15);
      })
      var parent_column = $(grid_buserOrder).closest('[class*="col-"]');
      $(document).on('settings.ace.jqGrid' , function(ev, event_name, collapsed) {
        if( event_name === 'sidebar_collapsed' || event_name === 'main_container_fixed' ) {
          //setTimeout is for webkit only to give time for DOM changes and then redraw!!!
          setTimeout(function() {
            $(grid_buserOrder).jqGrid( 'setGridWidth', parent_column.width() );
          }, 0);
        }
      })
      var optinons=jqSetting(grid_buserOrder,pager_buserOrder,url);
      optinons.url=url;
      optinons.postData=params
      optinons.caption=false
      optinons.colNames=['PRODUCT_ID', 'ORDER_ID','产品名称','原价','实收价格', '交易价格', '人数','产品单价','房间价格','是否分期','状态','下单时间'];
      optinons.colModel=[
        {display:"产品id",name:'PRODUCT_ID',index:'PRODUCT_ID', width:60, sorttype:"int", hidden:true,editable: true},
        {display:"订单id",name:'ORDER_ID',index:'ORDER_ID',width:90, editable:true, sorttype:"date",hidden:true},
        {display:"产品名称",name:'PRODUCT_NAME',index:'PRODUCT_NAME',width:150, editable:true,hidden:false},
        {display:"原价",name:'ORDER_ORIGINAL_PRICE',index:'ORDER_ORIGINAL_PRICE', width:90,editable: false},
        {display:"实收",name:'ORDER_ACTUAL_PRICE',index:'ORDER_ACTUAL_PRICE', width:90,editable: false},
        {display:"交易价格",name:'ORDER_TRANSACT_PRICE',index:'ORDER_TRANSACT_PRICE', width:70, editable: false},
        {display:"人数",name:'ORDER_PERSON_NUM',index:'ORDER_PERSON_NUM', width:50, editable: false},
        {display:"产品单价",name:'ORDER_PRODUCT_PRICE',index:'ORDER_PRODUCT_PRICE', width:90, editable: false},
        {display:"房间价格",name:'ORDER_ROOM_PRICE',index:'ORDER_ROOM_PRICE', width:90, editable: false},
        {display:"是否分期",name:'ORDER_IS_STAGE',index:'ORDER_IS_STAGE', width:90, editable: false,formatter:function (v,o,r) {
            if(v==0){
              return '否'
            }else{
              return '是'
            }
          }},
        {display:"状态",name:'ORDER_STATUS',index:'ORDER_STATUS', width:90, editable: false,formatter:function (v,o,r) {
            return renderProductStatus(v)
          }},
        {display:"下单时间",name:'CREATE_TIME',index:'CREATE_TIME', width:120, sortable:false,editable: false,formatter:pickDate}
      ];
      jQuery(grid_buserOrder).jqGrid(optinons);
      $(window).triggerHandler('resize.jqGrid');//trigger window resize to make the grid get the correct size

    }


    function renderBuserInScore() {
      var grid_buserInScore = "#grid-table-buserInScore";
      var pager_buserInScore = "#grid-pager-buserInScore";
      var url = $utils.ajax.setUrl("buserInScore/queryList")
      var params={}
      params.buserId = $utils.string.getQueryParam("id")
      $(window).on('resize.jqGrid', function () {
        $(grid_buserInScore).jqGrid( 'setGridWidth', $(grid_buserInScore).parents(".tab-content").width()-15);
      })
      var parent_column = $(grid_buserInScore).closest('[class*="col-"]');
      $(document).on('settings.ace.jqGrid' , function(ev, event_name, collapsed) {
        if( event_name === 'sidebar_collapsed' || event_name === 'main_container_fixed' ) {
          //setTimeout is for webkit only to give time for DOM changes and then redraw!!!
          setTimeout(function() {
            $(grid_buserInScore).jqGrid( 'setGridWidth', parent_column.width() );
          }, 0);
        }
      })
      var optinons=jqSetting(grid_buserInScore,pager_buserInScore,url);
      optinons.url=url;
      optinons.postData=params
      optinons.caption=false
      optinons.colNames=['ID','充值金额（元）','积分', '积分来源','说明','收入时间','操作'];
      optinons.colModel=[
        {display:"SCORE_ID",name:'SCORE_ID',index:'SCORE_ID', width:60, sorttype:"int", hidden:true,editable: true},
        {display:"充值金额",name:'SCORE_MONEY',index:'SCORE_MONEY',width:90, editable:true, sorttype:"date",hidden:false},
        {display:"积分",name:'SCORE_VALUE',index:'SCORE_VALUE',width:90, editable:true, sorttype:"date",hidden:false},
        {display:"积分来源",name:'SCORE_SOURCE_NAME',index:'SCORE_SOURCE_TYPE',width:90, editable:true, hidden:false},
        {display:"说明",name:'SCORE_TEXT',index:'SCORE_TEXT', width:150,editable: true},
        {display:"收入时间",name:'CREATE_TIME',index:'CREATE_TIME', width:150, sortable:false,editable: false,formatter:pickDate},
        {display:"操作",name:'oper',index:'oper', width:150,editable: true,formatter:operInScore},
      ];
      jQuery(grid_buserInScore).jqGrid(optinons);
      $(window).triggerHandler('resize.jqGrid');//trigger window resize to make the grid get the correct size

    }
    function renderBuserOutScore() {
      var grid_buserOutScore = "#grid-table-buserOutScore";
      var pager_buserOutScore = "#grid-pager-buserOutScore";
      var url = $utils.ajax.setUrl("buserOutScore/queryList")
      var params={}
      params.buserId = $utils.string.getQueryParam("id")
      $(window).on('resize.jqGrid', function () {
        $(grid_buserOutScore).jqGrid( 'setGridWidth', $(grid_buserOutScore).parents(".tab-content").width()-15);
      })
      var parent_column = $(grid_buserOutScore).closest('[class*="col-"]');
      $(document).on('settings.ace.jqGrid' , function(ev, event_name, collapsed) {
        if( event_name === 'sidebar_collapsed' || event_name === 'main_container_fixed' ) {
          //setTimeout is for webkit only to give time for DOM changes and then redraw!!!
          setTimeout(function() {
            $(grid_buserOutScore).jqGrid( 'setGridWidth', parent_column.width() );
          }, 0);
        }
      })
      var optinons=jqSetting(grid_buserOutScore,pager_buserOutScore,url);
      optinons.url=url;
      optinons.postData=params
      optinons.caption=false
      optinons.colNames=['ID', '消费积分','支出类型','说明','支出时间'];
      optinons.colModel=[
        {display:"SCORE_ID",name:'SCORE_ID',index:'SCORE_ID', width:60, sorttype:"int", hidden:true,editable: true},
        {display:"消费积分",name:'SCORE_VALUE',index:'SCORE_VALUE',width:90, editable:true, sorttype:"date",hidden:false},
        {display:"支出类型",name:'ITEM_NAME',index:'ITEM_NAME',width:90, editable:true, sorttype:"date",hidden:false},
        {display:"说明",name:'SCORE_TEXT',index:'SCORE_TEXT', width:150,editable: true},
        {display:"支出时间",name:'CREATE_TIME',index:'CREATE_TIME', width:150, sortable:false,editable: false,formatter:pickDate},
      ];
      jQuery(grid_buserOutScore).jqGrid(optinons);
      $(window).triggerHandler('resize.jqGrid');//trigger window resize to make the grid get the correct size
    }



    function renderBuserCollection() {
      var grid_buserCollection = "#grid-table-Collection";
      var pager_buserCollection = "#grid-pager-Collection";
      var url = $utils.ajax.setUrl("buserCollect/queryList")
      var params={}
      params.buserId = $utils.string.getQueryParam("id")
      $(window).on('resize.jqGrid', function () {
        $(grid_buserCollection).jqGrid( 'setGridWidth', $(grid_buserCollection).parents(".tab-content").width()-15);
      })
      var parent_column = $(grid_buserCollection).closest('[class*="col-"]');
      $(document).on('settings.ace.jqGrid' , function(ev, event_name, collapsed) {
        if( event_name === 'sidebar_collapsed' || event_name === 'main_container_fixed' ) {
          //setTimeout is for webkit only to give time for DOM changes and then redraw!!!
          setTimeout(function() {
            $(grid_buserCollection).jqGrid( 'setGridWidth', parent_column.width() );
          }, 0);
        }
      })
      var optinons=jqSetting(grid_buserCollection,pager_buserCollection,url);
      optinons.url=url;
      optinons.postData=params
      optinons.caption=false
      optinons.colNames=['ID', '产品名称','收藏时间','操作'];
      optinons.colModel=[
        {display:"COLLECT_ID",name:'COLLECT_ID',index:'COLLECT_ID', width:60, sorttype:"int", hidden:true,editable: true},
        {display:"PRODUCT_NAME",name:'PRODUCT_NAME',index:'PRODUCT_NAME',width:400, editable:true, sorttype:"date",hidden:false},
        {display:"收藏时间",name:'CREATE_TIME',index:'CREATE_TIME', width:100, sortable:false,editable: false,formatter:pickDate},
        {display:"操作",name:'oper',index:'oper', width:50,editable: true,formatter:operCollection}
      ];
      jQuery(grid_buserCollection).jqGrid(optinons);
      $(window).triggerHandler('resize.jqGrid');//trigger window resize to make the grid get the correct size
    }


    $("#Recharge").click(function () {
      Recharge(function (data) {
        var url=$utils.ajax.setUrl("buserInScore/addBuserInScore");
        var params={}
        params.buserId=parseInt($utils.string.getQueryParam("id"))
        params.scoreSourceType=4;
        params.scoreMoney=data.scoreMoney;
        params.scoreValue=data.scoreValue;
        params.scoreText=data.scoreText;
        axios.post(url, params)
          .then(function (response) {
            getBuserById()
           $("#grid-table-buserInScore").trigger("reloadGrid");
          })
          .catch(function (error) {
          });
      })
    })


  });
});

function operCollection(v,o,r) {
  var html=""
  html+='<span onclick="delCollection('+r.COLLECT_ID+')" class="ace-icon fa fa-trash-o red bigger-150 cursor-p tt-cursor"></span>'
  return html
}

function operInScore(v,o,r) {
  var html=""
  html+='<span onclick="delInScore('+r.SCORE_ID+')" class="ace-icon fa fa-trash-o red bigger-150 cursor-p tt-cursor"></span>'
  return html
}

function delInScore(id) {
  bootbox.confirm("确定删除吗",function (result) {
    if(result){
      var url=$utils.ajax.setUrl("buserInScore/deleteBuserInScoreById");
      var params={}
      params.scoreId =id;
      axios.post(url, params)
        .then(function (response) {
          getBuserById()
          $('#grid-table-buserInScore').trigger("reloadGrid");
        })
        .catch(function (error) {
        });
    }
  });
}

function delCollection(id) {
  bootbox.confirm("确定删除吗", function (result) {
    if (result) {
      var url = $utils.ajax.setUrl("buserCollect/deleteBuserCollectById");
      var params = {}
      params.collectId = id;
      axios.post(url, params)
        .then(function (response) {
          $('#grid-table-Collection').trigger("reloadGrid");
        })
        .catch(function (error) {
        });
    }
  });
}


function getBuserById() {
  var url = $utils.ajax.setUrl("buser/getBuserById");
  var params = {}
  params.buserId = $utils.string.getQueryParam("id")
  return axios.post(url, params)
    .then(function (response) {
      renderBuserBaseInfo(response.body)
    })
}
function getHistoryById() {
  var url = $utils.ajax.setUrl("buser/getBuserById");
  var params = {}
  params.buserId = $utils.string.getQueryParam("id")
  return axios.post(url, params)
    .then(function (response) {
      renderHistory(response.body.historyBrowsingList)
    })
}

function renderBuserBaseInfo(body) {
  $("#buserName").html(body.BUSER_NAME)
  $("#buserCertNo").html(body.ACCOUNT_CERT_NO)
  $("#buserMobile").html(body.BUSER_MOBILE)
  $("#buserEmail").html(body.BUSER_EMAIL)
  $("#buserAddress").html(body.BUSER_ADDRESS)
  $("#ACCOUNT_MEMBER_GRADE").html(MEMBER_GRADE(body.ACCOUNT_MEMBER_GRADE))
  $("#ACCOUNT_USED_SCORE").html(body.ACCOUNT_USED_SCORE)
  $("#ACCOUNT_AVAILABLE_SCORE").html(body.ACCOUNT_AVAILABLE_SCORE)
  if(body.IMAGE_URL.length>0){
    $("#avatar").attr("src",body.IMAGE_URL)
  }
}


function renderHistory(historyBrowsingList) {
  var grid_selector = "#grid-table-History";
  var options = jqSettingLocal(grid_selector,historyBrowsingList);
  options.colNames = ['产品名称','目的地','浏览时间'];
  options.colModel = [
    {name: 'PRODUCT_NAME', index: 'PRODUCT_NAME', width: 160, align: "center", editable: false, "sortable": false},
    {name: 'DESTINATION_NAME', index: 'DESTINATION_NAME', width: 160, align: "center", editable: false, "sortable": false},
    {name: 'CREATE_TIME', index: 'CREATE_TIME', width: 160, align: "center", editable: false, "sortable": false,formatter:pickDate}
  ];
  /*jqGrid 初始化*/
  jQuery(grid_selector).jqGrid(options);
  //初始化表格宽度
  var parent_column = $(grid_selector).parents('.tab-content').width()-15;
  initGridWidth(grid_selector, parent_column);
}


