var scripts = [null, "../assets/js/jquery.raty.js", "../assets/js/date-time/bootstrap-datepicker.js", "../assets/js/date-time/bootstrap-timepicker.js", "../assets/js/jquery.validate.js", "../assets/js/jquery.validate_msgzh.js", null]
$('.page-content-area').ace_ajax('loadScripts', scripts, function () {
  jQuery(function ($) {
    $('.rating').raty({'cancel': false, 'half': false, 'starType': 'i','number':10,score:0});
    $('.date-picker').datepicker({
      language: 'ch',
      autoclose: true,
      todayHighlight: true
    }).next().on(ace.click_event, function () {
      $(this).prev().focus();
    });

    $("#btn-ok").click(function () {
      addBUser()
    })
    $("#btn-reset").click(function () {
      reset()
    })
    $("#btn-cancel").click(function () {
      window.location.href="#page/bisuer/list"
    })

    $("#webuploader-pick").click(function () {
      resourceModel(1,1,"",function (data) {
        console.log(data)
        $(".mainImg").attr("src",data[0].src);
        $(".mainImg").attr("data-id",data[0].id);
      })
    })

    function addBUser() {
      var url = $utils.ajax.setUrl("buser/addBuser");
      var params = {}
      params.buserName = $("#buserName").val()
     // params.buserAccount = $("#buserAccount").val()
      params.buserPwd = $("#buserPwd").val()
      params.buserCertType = $("#buserCertType").val()
      params.buserCertNo = $("#buserCertNo").val()
      params.buserMobile = $("#buserMobile").val()
      params.buserEmail = $("#buserEmail").val()
      params.buserAddress = $("#buserAddress").val()
      params.buserSex = $("#buserSex input[name=sex]:checked").val()
      //params.buserIsMember = $("#buserIsMember input[name=buserIsMember]:checked").val()
     // params.buserMemberGrade = $("#buserMemberGrade input[name=score]").val()
     // params.buserScore = $("#buserScore").val()
     // params.buserExpireDate = $("#buserExpireDate").val()
      //params.buserHeaderUrl= $(".mainImg").attr("src")
      if (params.buserMobile.length == 0) {
          $(".error[for='buserMobile']").html("手机号不能为空")
          $("#buserMobile").focus()
          return false
      }
      if (params.buserMobile.length > 0) {
        if (!$utils.validate.validatePhone(params.buserMobile)) {
          $(".error[for='buserMobile']").html("手机号不正确")
          $("#buserMobile").focus()
          return false
        }
      }
      if (params.buserEmail.length == 0) {
          $(".error[for='buserEmail']").html("邮箱不能为空")
          $("#buserEmail").focus()
          return false
      }
      if (params.buserEmail.length > 0) {
        if (!$utils.validate.validateEmail(params.buserEmail)) {
          $(".error[for='buserEmail']").html("邮箱不正确")
          $("#buserEmail").focus()
          return false
        }
      }

      console.log(params)


      axios.post(url, params)
        .then(function (response) {
          bootbox.alert("添加成功")
          //window.location.href="#"
        })
    }

    function reset() {
      $("#buserName").val("")
      $("#buserAccount").val("")
      $("#buserCertType").val("0")
      $("#buserCertNo").val("")
      $("#buserMobile").val("")
      $("#buserEmail").val("")
      $("#buserAddress").val("")
      $("#sex input[value=0]").prop("checked", true)
      $("#buserIsMember input[value=0]").prop("checked", true)
      $("#buserAddress").val("")
      $('.rating').raty({'cancel': false, 'half': false, 'starType': 'i','number':10,score:0});
      $("#buserExpireDate").val("")
    }
  });
});
