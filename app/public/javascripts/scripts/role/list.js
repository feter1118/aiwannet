var scripts = [null, "", null];
var grid_sysRole = "#grid-table";
var pager_sysRole = "#grid-pager";
var url = $utils.ajax.setUrl("role/queryList")
$('.page-content-area').ace_ajax('loadScripts', scripts, function() {
  jQuery(function($) {
    $(window).on('resize.jqGrid', function () {
      $(grid_sysRole).jqGrid( 'setGridWidth', $(".page-content").width() );
    })
    var parent_column = $(grid_sysRole).closest('[class*="col-"]');
    $(document).on('settings.ace.jqGrid' , function(ev, event_name, collapsed) {
      if( event_name === 'sidebar_collapsed' || event_name === 'main_container_fixed' ) {
        //setTimeout is for webkit only to give time for DOM changes and then redraw!!!
        setTimeout(function() {
          $(grid_sysRole).jqGrid( 'setGridWidth', parent_column.width() );
        }, 0);
      }
    })
    var optinons=jqSetting(grid_sysRole,pager_sysRole,url);
    optinons.url=url;
    optinons.caption="系统角色列表"
    optinons.colNames=['操作 ','ID', '角色名', '', '更新时间'];
    optinons.colModel=[
      {name:'operation',index:'', width:80, fixed:true, sortable:false, resize:false,
        formatter:operation,
      },
      {display:"ROLE_ID",name:'ROLE_ID',index:'ROLE_ID', width:60, sorttype:"int", hidden:true,editable: true},
      {display:"姓名",name:'ROLE_NAME',index:'ROLE_NAME',width:90, editable:true, sorttype:"date",hidden:false},
      {display:"",name:'USER_ACCOUNT',index:'USER_ACCOUNT',width:90, editable:true, sorttype:"date",hidden:false},
      {display:"更新时间",name:'UPDATE_TIME',index:'UPDATE_TIME', width:150, sortable:false,editable: false,formatter:pickDate}
    ];
    jQuery(grid_sysRole).jqGrid(optinons);
    $(window).triggerHandler('resize.jqGrid');//trigger window resize to make the grid get the correct size

    function operation(v,o,r) {
      var html=""
      html+='<span onclick="goEdit('+r.ROLE_ID+')" class="ace-icon fa fa-pencil blue bigger-150 cursor-p tt-cursor pld10 prd10"></span>'
      html+='<span onclick="del('+r.ROLE_ID+')" class="ace-icon fa fa-trash-o red bigger-150 cursor-p tt-cursor"></span>'
      return html
    }
    $(document).one('ajaxloadstart.page', function(e) {
      $(grid_sysRole).jqGrid('GridUnload');
      $('.ui-jqdialog').remove();
    });
  });
});
function goEdit(id) {
  window.location.href="#page/role/edit?id="+id;
}
function del(id) {
  bootbox.confirm("确定删除吗",function (result) {
    if(result){
      var url=$utils.ajax.setUrl("role/deleteRoleById");
      var params={}
      params.roleId=id;
      axios.post(url, params)
        .then(function (response) {
         $(grid_sysRole).trigger("reloadGrid");
        })
        .catch(function (error) {
        });
    }
  });
}
