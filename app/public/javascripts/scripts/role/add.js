var scripts = [null, "../assets/js/jquery.validate.js", "../assets/js/jquery.validate_msgzh.js","../assets/ztree/jquery.ztree.core.min.js","../assets/ztree/jquery.ztree.excheck.min.js",null]
$('.page-content-area').ace_ajax('loadScripts', scripts, function () {
  jQuery(function ($) {
    getMenuList()
    function getMenuList() {
      var params={}
      var url=$utils.ajax.setUrl("menu/getMenuList");
      axios.post(url, params)
        .then(function (response) {
          computeMenuList(response.body)
        })
        .catch(function (error) {
        });
    }


  });

  function computeMenuList(data) {
    var MenuList=[]
    console.log(data)

    $.each(data.roleAllList,function (key,val) {
      var item={}
      item.id=val.menuId
      item.pId=val.menuPid
      item.checked=false
      item.name=val.menuName
      MenuList.push(item)
    })
    console.log(MenuList)


    renderRole(MenuList)
  }
  function renderRole(MenuList) {
    var setting = {
      check: {
        enable: true
      },
      data: {
        simpleData: {
          enable: true
        }
      }
    };
    var zNodes =MenuList;
    $.fn.zTree.init($("#ztree"), setting, zNodes);
  }


  $("#add").click(function () {
    addRole()
  })
  function addRole() {
    var params={}
    params.roleName=$("#roleName").val()
    params.roleDetail=$("#roleDetail").val()
    //bootbox.alert("更新成功")
    var url=$utils.ajax.setUrl("role/addRole");
    axios.post(url, params)
      .then(function (response) {
        allotNav(response.body.roleId)
      })
      .catch(function (error) {
      });
  }


  function allotNav(roleId) {
    var url=$utils.ajax.setUrl("role/updateRoleMenu");
    var params={}
    params.roleId=roleId
    params.menuIds=getSelectTreetParam("ztree")
    axios.post(url, params)
      .then(function (response) {
        bootbox.alert("更新成功")
        window.location.href="#page/role/list"
      })
      .catch(function (error) {
      });

  }



});
