var scripts = [null, "../assets/js/jquery.validate.js", "../assets/js/jquery.validate_msgzh.js","../assets/ztree/jquery.ztree.core.min.js","../assets/ztree/jquery.ztree.excheck.min.js",null]
$('.page-content-area').ace_ajax('loadScripts', scripts, function () {
  jQuery(function ($) {
    getRoleById()
    function getRoleById() {
      var url=$utils.ajax.setUrl("role/getRoleById");
      var params={}
      params.roleId=$utils.string.getQueryParam("id")
      axios.post(url, params)
        .then(function (response) {
          renderRole(response.body)
        })
        .catch(function (error) {
        });
    }



    function renderRole(data){
      $("#roleName").val(data.roleName)
      $("#roleDetail").val(data.roleDetail)
    }

    getMenuList()
    function getMenuList() {
      var params={}
      params.roleId=$utils.string.getQueryParam("id")
      var url=$utils.ajax.setUrl("menu/getMenuList");
      axios.post(url, params)
        .then(function (response) {
          computeMenuList(response.body)
        })
        .catch(function (error) {
        });
    }

    $("#add").click(function () {
      addSysUser()
    })
  });

  function computeMenuList(data) {
    var MenuList=[]
    var selectIds=[]
    $.each(data.selectMenuList,function (key,val) {
      selectIds.push(String(val.menuId))
    })

    $.each(data.roleAllList,function (key,val) {
      var item={}
      item.id=val.menuId
      item.pId=val.menuPid
      if($.inArray(String(item.id),selectIds)!=-1){
        item.checked=true
      }else{
        item.checked=false
      }
      item.name=val.menuName
      MenuList.push(item)
    })
    console.log(MenuList)


    renderRole(MenuList)
  }
  function renderRole(MenuList) {
    var setting = {
      check: {
        enable: true
      },
      data: {
        simpleData: {
          enable: true
        }
      }
    };
    var zNodes =MenuList;
    $.fn.zTree.init($("#ztree"), setting, zNodes);
  }

  $("#update").click(function () {
    updateRoleById()
  })

  function updateRoleById() {
    var url=$utils.ajax.setUrl("role/updateRoleById");
    var params={}
    params.roleId=$utils.string.getQueryParam("id")
    params.roleName=$("#roleName").val()
    params.roleDetail=$("#roleDetail").val()
    var flag = $("#editForm").valid();
    if(!flag){
      return false
    }
    axios.post(url, params)
      .then(function (response) {
        allotNav()
      })
      .catch(function (error) {
      });
  }


  function allotNav() {
    var url=$utils.ajax.setUrl("role/updateRoleMenu");
    var params={}
    params.roleId=$utils.string.getQueryParam("id")
    params.menuIds=getSelectTreetParam("ztree")
    axios.post(url, params)
      .then(function (response) {
        bootbox.alert("更新成功")
        window.location.href="#page/role/list"
      })
      .catch(function (error) {
      });

  }
});
