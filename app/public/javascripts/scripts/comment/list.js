var scripts = [null, "", null];
$('.page-content-area').ace_ajax('loadScripts', scripts, function() {
    function goEdit (id) {
      window.location.href = `#page/comment/edit?id=${id}`;
    }
    function goDelete (id) {
      $utils.modal.confirm({
        content: '确定删除吗？',
        yes () {
          $utils.ajax.post('comment/deleteProductCommentById', { id }).done(data => {
            if (data.code === '10000') {
              $grid.trigger('reloadGrid');
            }
          });
        },
        cancel () {}
      });
    }

    $utils.jqgrid.init({
      gridSelector: '#grid-table',
      pagerSelector: '#grid-pager',
      url: 'comment/deleteProductCommentById',
      caption: '产品评论列表',
      colNames: ['操作', '主键ID', '业务用户ID', '评论内容', '是否删除', '创建人', '创建时间', '修改时间'],
      colModel: [
        {
          name: 'operation',
          formatter: function (cellValue, options, rowObject) {
            var html = '';
            html += '<span onclick="goEdit({{rowObject.COMMENT_ID}})" class="ace-icon fa fa-pencil blue bigger-150 cursor-p tt-cursor pld10 prd10"></span>';
            html += '<span onclick="goDelete({{rowObject.COMMENT_ID}})" class="ace-icon fa fa-trash-o red bigger-150 cursor-p tt-cursor"></span>';
            return html.replace(/{{rowObject.COMMENT_ID}}/g, rowObject.PRODUCT_ID);
          },
        },
        { display: '主键ID', name: 'COMMENT_ID', hidden: true },
        { display: '业务用户ID', name: 'BUSER_ID' },
        { display: '评论内容', name: 'COMMENT_TEXT' },
        { display: '是否删除', name: 'DELETE_FLAG' },
        { display: '创建人', name: 'CREATER_ID' },
        {
          display: '创建时间',
          name: 'CREATE_TIME',
          width: 150,
          formatter: function (cellVal) {
            return moment(cellVal).format("YYYY-MM-DD");
          }
        },
        {
          display: '修改时间',
          name: 'UPDATE_TIME',
          width: 150,
          formatter: function (cellVal) {
            return moment(cellVal).format("YYYY-MM-DD");
          }
        }
      ]
    })
});
