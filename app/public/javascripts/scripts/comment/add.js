$utils.hook.ready([
  '../assets/js/jquery.validate.js',
  '../assets/js/jquery.validate_msgzh.js'
], () => {
  const $add = $('#add');
  const $editForm = $('#editForm');

  // 加载选项数据
  $utils.ajax.post('test/test', {}).done(data => {
    if (data.code === '10000') {
      //
    }
  });

  // 获取评论数据并渲染
  $utils.ajax.post('test/test', { id: $utils.string.getQueryParam('id') }).done(data => {
    if (data.code === '10000') {
      //
    }
  });

  // 点击添加按钮
  $add.click((e) => {
    if (!$editForm.valid()) {
      return;
    }
    $utils.ajax.post('test/test', {
      id: $utils.string.getQueryParam('id')
    }).done(data => {
      if (data.code === '10000') {
        window.location.href = '#page/comment/list';
      }
    });
  });
});
