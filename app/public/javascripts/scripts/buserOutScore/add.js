var scripts = [null, '../assets/js/bootstrap-multiselect.js', "../assets/js/jquery.validate.js", "../assets/js/jquery.validate_msgzh.js", '../assets/js/bootstrap-tagsinput.js', null]

$('.page-content-area').ace_ajax('loadScripts', scripts, function () {
  
	renderSourceType()
	
  $("#add").click(function () {
    addOutScore()
  })

  function addOutScore() {
    var params = {}
    params.buserMobile = $("#buserMobile").val();
    params.scoreOutType = $("#scoreOutType").val();
    params.scoreValue = $("#scoreValue").val();
    params.scoreText = $("#scoreText").val();

    if ($utils.string.isNull(params.scoreOutType)) {
      bootbox.alert("请选择积分来源")
      return false
    }
    
    if ($utils.string.isNull(params.scoreValue)) {
        bootbox.alert("请输入积分值")
        return false
      }
      
    if ($utils.string.isNull(params.scoreText)) {
        bootbox.alert("请输入积分说明")
        return false
      }
    
      if (params.scoreText.length>100) {
          bootbox.alert("积分说明不能超过100字")
          return false
      }
      
      if ($utils.string.isNull(params.buserMobile)) {
          bootbox.alert("请输入用户手机号")
          return false
        }
      
        if (params.buserMobile.length>20) {
            bootbox.alert("用户手机号不能超过20字")
            return false
        }

    var url = $utils.ajax.setUrl("buserOutScore/addBuserOutScore");
    axios.post(url, params)
      .then(function (response) {
        bootbox.alert(response.msg);
          window.location.href = "#page/buserOutScore/list"
      })
  }
	
	/*积分来源*/
	function renderSourceType() {
		var url=$utils.ajax.setUrl("dicItem/getDicItemListByDicCode");
		  var params={}
		  params.dicCode ='D00000009'
	  	axios.post(url, params)
	  		.then(function (response) {
	  		var item = response.body;
	  		var html = '';
	  		$.each(item, function (key, val) {
	  		    html += '<option value="' + val.ITEM_CODE + '">' + val.ITEM_NAME + '</option>'
	  		});
	  		$("#scoreOutType").html(html);
	  	});
	}

});

