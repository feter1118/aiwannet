var scripts = [null, "", null];
var grid_outScore = "#grid-table";
var pager_outScore = "#grid-pager";
var url = $utils.ajax.setUrl("buserOutScore/queryList")
$('.page-content-area').ace_ajax('loadScripts', scripts, function() {
  renderSourceType();
  jQuery(function($) {
    $(window).on('resize.jqGrid', function () {
      $(grid_outScore).jqGrid( 'setGridWidth', $(".page-content").width() );
    })
    var parent_column = $(grid_outScore).closest('[class*="col-"]');
    $(document).on('settings.ace.jqGrid' , function(ev, event_name, collapsed) {
      if( event_name === 'sidebar_collapsed' || event_name === 'main_container_fixed' ) {
        //setTimeout is for webkit only to give time for DOM changes and then redraw!!!
        setTimeout(function() {
          $(grid_outScore).jqGrid( 'setGridWidth', parent_column.width() );
        }, 0);
      }
    })
    var optinons=jqSetting(grid_outScore,pager_outScore,url);
    optinons.url=url;
    optinons.caption="系统用户列表"
    optinons.colNames=['ID', '用户手机号','积分支出类型','积分','内容', '更新时间'];
    optinons.colModel=[
      /*{name:'operation',index:'', width:80, fixed:true, sortable:false, resize:false,
        formatter:operation,
      },*/
      {display:"SCORE_ID",name:'SCORE_ID',index:'SCORE_ID', width:60, sorttype:"int", hidden:true,editable: true},
      {display:"用户手机号",name:'BUSER_MOBILE',index:'BUSER_MOBILE',width:90, editable:true, sorttype:"date",hidden:false},
      {display:"积分支出类型",name:'SCORE_OUT_NAME',index:'SCORE_OUT_NAME',width:90, editable:true, sorttype:"date",hidden:false},
      {display:"积分",name:'SCORE_VALUE',index:'SCORE_VALUE', width:150,editable: true},
      {display:"内容",name:'SCORE_TEXT',index:'SCORE_TEXT', width:150,editable: true},
      {display:"更新时间",name:'UPDATE_TIME',index:'UPDATE_TIME', width:150, sortable:false,editable: false,formatter:pickDate}
    ];
    jQuery(grid_outScore).jqGrid(optinons);
    $(window).triggerHandler('resize.jqGrid');//trigger window resize to make the grid get the correct size

    function operation(v,o,r) {
      var html=""
      html+='<span onclick="goEdit('+r.SCORE_ID+')" class="ace-icon fa fa-pencil blue bigger-150 cursor-p tt-cursor pld10 prd10"></span>'
      html+='<span onclick="del('+r.SCORE_ID+')" class="ace-icon fa fa-trash-o red bigger-150 cursor-p tt-cursor"></span>'
      return html
    }
    $(document).one('ajaxloadstart.page', function(e) {
      $(grid_outScore).jqGrid('GridUnload');
      $('.ui-jqdialog').remove();
    });
  });
  
  /*积分来源*/
  function renderSourceType() {
	  var url=$utils.ajax.setUrl("dicItem/getDicItemListByDicCode");
	  var params={}
	  params.dicCode ='D00000009'
		  axios.post(url, params)
	      .then(function (response) {
	        var array = response.body;
	        for(var i=0;i<array.length;i++){
	          $("#scoreOutType").append("<option value='"+array[i].ITEM_CODE+"'>"+array[i].ITEM_NAME+"</option>");
	        }
	      })
	      .catch(function (error) {
	      });
  }

});

function goEdit(id) {
  window.location.href="#page/buserOutScore/edit?id="+id;
}

function del(id) {
  bootbox.confirm("确定删除吗",function (result) {
    if(result){
      var url=$utils.ajax.setUrl("buserOutScore/deleteBuserOutScoreById");
      var params={}
      params.scoreId=id;
      axios.post(url, params)
        .then(function (response) {
          $(grid_outScore).trigger("reloadGrid");
        })
        .catch(function (error) {
        });
    }
  });
}

function search() {
	  var url = $utils.ajax.setUrl("buserOutScore/queryList");
	  var params = {}
	  params.buserMobile = $("#buserMobile").val();
	  params.scoreOutType = $("#scoreOutType").val()
	  $(grid_outScore).jqGrid('setGridParam', {
	    url: url,
	    contentType: 'application/json;charset=utf-8',
	    page: 1,
	    postData: params
	    // 发送数据
	  }).trigger("reloadGrid");
	}
