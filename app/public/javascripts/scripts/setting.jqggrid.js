
function jqSetting(grid_selector,pager_selector, url) {
  var options = {
    mtype:"POST",
    //subgrid options
    subGrid : false,
    // prmNames: {rows:null, sort: null,order: null, search:null, nd:null, npage:null},
    datatype: "json",
    height: "auto",
    viewrecords : true,
    rowNum:10,
    pagerpos: "right", //分页工具栏右侧
    // rowList:[10,20,30],
    pager : pager_selector,
    altRows: true,
    //toppager: true,
    multiselect: false,
    //multikey: "ctrlKey",
    multiboxonly: true,
    jsonReader: {
      root: "body.rows", page: "body.current", total: "body.total", records: "body.count", repeatitems: false
    },
    loadComplete: function(data) {
      $('#jqgh_'+pager_selector+'_cb').css({display: 'none'});
      $('#cb_'+pager_selector).attr({disabled: 'disabled'});
      $(grid_selector).closest(".ui-jqgrid-bdiv").css({ 'overflow-x' : 'hidden' });
      if (data.body.length==0||data.body.rows.length == 0) {
        var str = '<tr class="jqgridNoData"><td colspan="100" style="text-align: center;font-size: 18px;height: 36px;line-height: 36px;"><i class="ace-icon glyphicon glyphicon-repeat"></i>暂无数据</td></tr>';
        $(grid_selector +' tbody').append(str);
      }
      var table = this;
      setTimeout(function(){
        initPagerIcons(table);
      }, 0);
    }
  }
  return options;
}


function jqSettingLocal(grid_selector,grid_data) {
  var options = {
    data: grid_data,
    datatype: "local",
    height: 'auto',
    viewrecords: false, //右侧统计信息是否显示
    pagerpos: "right", //分页工具栏右侧
    altRows: true,
    loadMsg: '数据加载中',
    singleSelect: true, // 设置为单选行
    selectOnCheck: true, // 允许单行点击CheckBox取消选择(设置此属性为false，不能获得选择行信息)
    checkOnSelect: false, // 允许行选择复选框
    collapsible: true,
    viewsortcols: [true, 'vertical', true],
    border: false, // 是否显示边框
    fitColumns: false,
    showFooter: false, // 显示底部信息
    fit: true,
    rownumbers: false, // 行号显示
    remoteSort: true, // 设置为true会访问远程数据
    autowidth: true, //自适应宽度（铺满父级）
    autoScroll: false,
    multiselect: false, //左侧选择框（单选和复选与下面有关 需jqgrid事件控制）
    scroll:false,
    scrollOffset:0,
    loadComplete: function(data) {
      $(grid_selector).closest(".ui-jqgrid-bdiv").css({ 'overflow-x' : 'hidden' });
      if (data.rows.length == 0) {
        var str = '<tr class="jqgridNoData"><td colspan="100" style="text-align: center;font-size: 18px;height: 36px;line-height: 36px;"><i class="ace-icon glyphicon glyphicon-repeat"></i>暂无数据</td></tr>';
        $(grid_selector +' tbody').append(str);
      }
    }
  }
  return options;
}

function initPagerIcons(table) {
  var replacement = {
    'ui-icon-seek-first': 'ace-icon fa fa-angle-double-left bigger-140',
    'ui-icon-seek-prev': 'ace-icon fa fa-angle-left bigger-140',
    'ui-icon-seek-next': 'ace-icon fa fa-angle-right bigger-140',
    'ui-icon-seek-end': 'ace-icon fa fa-angle-double-right bigger-140'
  };
  $('.ui-pg-table:not(.navtable) > tbody > tr > .ui-pg-button > .ui-icon').each(function () {
    var icon = $(this);
    var $class = $.trim(icon.attr('class').replace('ui-icon', ''));
    if ($class in replacement) icon.attr('class', 'ui-icon ' + replacement[$class]);
  })
}
//初始化表格宽度
function initGridWidth(grid_selector,parent_column) {
  //resize to fit page size
  $(window).on('resize.jqGrid', function () {
    $(grid_selector).jqGrid('setGridWidth', parent_column);
  })
  $(window).triggerHandler('resize.jqGrid');
}
