var scripts = [null, "../assets/js/jquery.validate.js", "../assets/js/jquery.validate_msgzh.js",null]
$('.page-content-area').ace_ajax('loadScripts', scripts, function () {
  jQuery(function ($) {
    renderPMenuList()
    $("#add").click(function () {
      addNav()
    })
  });
  function addNav() {

    var params={}
    params.menuName=$("#menuName").val()
    params.menuType=$("#menuType").val()
    params.menuUrl=$("#menuUrl").val()
    params.menuPid=$("#menuPid").val()
    params.menuSort=$("#menuSort").val()

    var flag = $("#editForm").valid();
    if(!flag){
      return false
    }
    var url=$utils.ajax.setUrl("menu/addMenu");
    axios.post(url, params)
      .then(function (response) {
        window.location.href="#page/nav/list"
      })
      .catch(function (error) {
      });
  }
});
