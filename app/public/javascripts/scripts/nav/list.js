var scripts = [null, "", null];
var grid_sysMenu = "#grid-table";
var pager_sysRole = "#grid-pager";
var url = $utils.ajax.setUrl("menu/queryList")
$('.page-content-area').ace_ajax('loadScripts', scripts, function() {
  jQuery(function($) {
	renderMenuPid();
    $(window).on('resize.jqGrid', function () {
      $(grid_sysMenu).jqGrid( 'setGridWidth', $(".page-content").width() );
    })
    var parent_column = $(grid_sysMenu).closest('[class*="col-"]');
    $(document).on('settings.ace.jqGrid' , function(ev, event_name, collapsed) {
      if( event_name === 'sidebar_collapsed' || event_name === 'main_container_fixed' ) {
        //setTimeout is for webkit only to give time for DOM changes and then redraw!!!
        setTimeout(function() {
          $(grid_sysMenu).jqGrid( 'setGridWidth', parent_column.width() );
        }, 0);
      }
    })
    var optinons=jqSetting(grid_sysMenu,pager_sysRole,url);
    optinons.url=url;
    optinons.caption="系统菜单列表"
    optinons.colNames=['操作 ','ID', '菜单名','父级菜单', '路径'];
    optinons.colModel=[
      {name:'operation',index:'', width:80, fixed:true, sortable:false, resize:false,
        formatter:operation,
      },
      {display:"MENU_ID",name:'MENU_ID',index:'MENU_ID', width:60, sorttype:"int", hidden:true,editable: true},
      {display:"菜单名",name:'MENU_NAME',index:'MENU_NAME',width:90, editable:true, sorttype:"date",hidden:false},
      {display:"父级菜单",name:'PMENU_NAME',index:'PMENU_NAME',width:90, editable:true, sorttype:"date",hidden:false},
      {display:"路径",name:'MENU_URL',index:'MENU_URL',width:90, editable:true, sorttype:"date",hidden:false},
    ];
    jQuery(grid_sysMenu).jqGrid(optinons);
    $(window).triggerHandler('resize.jqGrid');//trigger window resize to make the grid get the correct size

    function operation(v,o,r) {
      var html=""
      html+='<span onclick="goEdit('+r.MENU_ID+')" class="ace-icon fa fa-pencil blue bigger-150 cursor-p tt-cursor pld10 prd10"></span>'
      html+='<span onclick="del('+r.MENU_ID+')" class="ace-icon fa fa-trash-o red bigger-150 cursor-p tt-cursor"></span>'
      return html
    }
    $(document).one('ajaxloadstart.page', function(e) {
      $(grid_sysMenu).jqGrid('GridUnload');
      $('.ui-jqdialog').remove();
    });
  });
});
function goEdit(id) {
  window.location.href="#page/nav/edit?id="+id;
}
function del(id) {
  bootbox.confirm("确定删除吗",function (result) {
    if(result){
      var url=$utils.ajax.setUrl("menu/deleteMenuById");
      var params={}
      params.menuId=id;
      axios.post(url, params)
        .then(function (response) {
         $(grid_sysMenu).trigger("reloadGrid");
        })
        .catch(function (error) {
        });
    }
  });
}

/*填充一级菜单*/
function renderMenuPid() {
	var html = '<option value="">全部</option>'
	var pMenu = JSON.parse(localStorage.getItem("computedNav"));
	$.each(pMenu, function (key, val) {
		html += '<option value="' + val.menuId + '">' + val.menuName + '</option>'
	})
	$("#menuPid").html(html)
}

function search() {
	  var url = $utils.ajax.setUrl("menu/queryList");
	  var params = {}
	  params.menuName = $("#menuName").val()
	  params.menuPid = $("#menuPid").val()
	  $(grid_sysMenu).jqGrid('setGridParam', {
	    url: url,
	    contentType: 'application/json;charset=utf-8',
	    page: 1,
	    postData: params
	    // 发送数据
	  }).trigger("reloadGrid");
	}

