var scripts = [null, "../assets/js/jquery.validate.js", "../assets/js/jquery.validate_msgzh.js", null]
$('.page-content-area').ace_ajax('loadScripts', scripts, function () {
  jQuery(function ($) {
    renderPMenuList()
    getData()

    function getData() {
      var url = $utils.ajax.setUrl("menu/getMenuById");
      var params = {}
      params.menuId = $utils.string.getQueryParam("id")
      axios.post(url, params)
        .then(function (response) {
          renderMeau(response.body)
        })
        .catch(function (error) {
        });
    }

    function renderMeau(body) {
      $("#menuName").val(body.menuName)
      $("#menuType").val(body.menuType)
      $("#menuUrl").val(body.menuUrl)
      $("#menuPid").val(body.menuPid)
      $("#menuSort").val(body.menuSort)
    }

    $("#update").click(function () {
      updateMeau()
    })

    function updateMeau() {
      var url = $utils.ajax.setUrl("menu/updateMenuById");
      var params = {}
      params.menuId = $utils.string.getQueryParam("id")
      params.menuName=$("#menuName").val()
      params.menuType=$("#menuType").val()
      params.menuUrl=$("#menuUrl").val()
      params.menuPid=$("#menuPid").val()
      params.menuSort=$("#menuSort").val()
      axios.post(url, params)
        .then(function (response) {
          window.location.href = "#page/nav/list"
        })
        .catch(function (error) {
        });

    }
  });


});
